<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', function(){
   return Redirect::to('/manage/login');
});
Route::group(array('prefix' => 'manage'), function() {
    //login bypass for the below listed controllers
    Route::controller('login', 'AdminloginController');
    Route::controller('forgotpassword', 'AdminforgotpasswordController');
});

Route::group(array('before' => 'managelogin', 'prefix' => ''), function()  //the before here is the  auth check if user isnt login then redirect him login
{	
    Route::controller('manage', 'AdminhomeController');   

});

Route::group(array('before' => 'manage_ajax', 'prefix' => ''), function()  //the before here is the  auth check if user isnt login then redirect him login
{
  Route::controller('dashboard', 'AdmindashboardController');
  Route::controller('adminprofile', 'AdminprofileController');
  Route::controller('users', 'AdminusersController');
  Route::controller('products', 'AdminproductsController');
  Route::controller('orders', 'AdminordersController');
  Route::controller('setting', 'AdminconfigsettingController');
  Route::controller('bank', 'BankController');
  Route::controller('usermanage', 'UsermanageController');

});


//logout.
Route::any("logout", array('as' => 'logout', 'uses' => 'AdminloginController@getLogout'));

###################### API ROUTES STARTS ########################


//route define for api with api prefix
Route::group(array('prefix' => 'api'), function() {
    Route::controller('products', 'ApiproductController');
});
//Route::controller('cron', 'CronController');
App::missing(function($exception) {
    
   return View::make('layout.managemaster'); 
});

//Route::controller('/', 'HomeController');
Route::controller('user', 'AdminUsersController');
