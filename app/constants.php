<?php

//define document root

define('SITE_TITLE', 'Mawashi');
define('BASEURL',url('/'));
define('BASEURL_MANAGE',url('/').'/manage/');
define('DIR_UPLOADS', url('/') . '/uploads/');

define('ASSETS_URL', url('/') . '/assets/');
define('MANAGE_TEXT', 'manage/');
define('ADMIN_TEXT', 'admin/');
define('ASSETS',url('/').'/assets/');
define('ASSETS_IMAGE',url('/').'/assets/images/');
define('ASSETS_IMAGE_FRONT',url('/').'/assets/images/');
define('DIR_PROFILE_PIC',public_path('') . '/uploads/profile/');
define('DIR_PROFILE_PIC_URL',url('/') . '/uploads/profile/');

define('ADMINPROFILE_IMAGE_URL',  url('/') . '/uploads/profile/');
define('ADMINPROFILE_IMAGE_PATH',  public_path('') . '/uploads/profile/');

define('PRODUCT_IMAGE_URL',  url('/') . '/uploads/product/');
define('PRODUCT_IMAGE_PATH',  public_path('') . '/uploads/product/');

define('DIR_EXPORTS',public_path('') . '/uploads/exports/');

define('MORE_FROM_LAST', 'More From Last Month');
define('LEASS_FROM_LAST', 'LESS From Last Month');

define('STATUS_FALSE', false);
define('STATUS_TRUE', true);

define('my_details', 'تفاصيلي');
?>
