<?php

class BankController extends BaseController {

    protected $layout = 'layout.managemaster';

    //###########################################################
    //Function : getIndex
    //purpose : To load dashboard
    //input : email,password
    //output : error/scuccess message
    //###########################################################
    public function getIndex() {

        $data = array();
        return View::make('layout.managemaster');
    }

    public function postAddnewbank() {

        $input = Input::all();
        $return_array = Configsetting::postAddnewbank($input);
      //  print_r($return_array);die;
        return $return_array;
        
    }
    
    public function getBankdetails(){
        
        $return_array['success'] = false;
        $return_array['data'] = array();
        
        $input = Input::all();
       
        if(isset($input['id'])){
            
            $Id = base64_decode($input['id']);
            $getbank = DB::table('bank_list')->select('*')->where('id', $Id)->first();
            $getbank = json_decode(json_encode($getbank), true);
            
            if($getbank){
                
                $return_array['success'] = true;
                $return_array['data'] = $getbank;
            }
        }
        
        return $return_array;
        
    }
    
    public function postAllbankslist(){
        
       $products = DB::table('bank_list')
            ->select('id','imag_logo', 'name', 'number')->where('status', 1);
            return Datatables::of($products)
            ->edit_column('imag_logo', '<a href="manage/product_detail/{{base64_encode($id)}}"><img src="{{PRODUCT_IMAGE_URL.$imag_logo}}" height="100" width="100"/></a>')
           ->edit_column('edit_product', '<a href="manage/edit_bank/{{base64_encode($id)}}" class="btn btn-green">Edit</a><a href="javascript:;" data-id="{{$id}}" class="btn btn-danger sheep_size_delete"><i class="fa fa-times" aria-hidden="true"></i></a>')
            ->make(true);
        
    }
    
    public function postEditbank(){
         $input = Input::all();
         $return_array = Configsetting::postEditbank($input);
     
        return $return_array;
        
    }
    
     public function postDeletebank(){
         
          $in = Input::all();
        
          if(isset($in['bank_id']) && $in['bank_id'] !=''){
                 $update  = DB::table('bank_list')
                                ->where(array('id' => $in['bank_id']))
                                ->update(array('status' => 0));
                 if($update){
                      $returndata['success'] = true;
                 }
             }
             return $returndata;
        
    }

}