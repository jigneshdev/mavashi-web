<?php

class AdminusersController extends BaseController {

    protected $layout = 'layout.managemaster';
	public function anyAllusersdata()
	{
		$users = DB::table('front_users')
			->select('id', 'name','from_social_network',
			DB::raw('CASE WHEN from_social_network = "1" THEN "Facebook" WHEN from_social_network = "2" THEN "Google" ELSE "Email" END as from_social_network'),
			'email_address', 'last_login', 
			'my_referral_code', 'phone_number');
        
        if(Input::get('roles_ids') !=''){ 
           $users->where('app_user_status','=',Input::get('roles_ids'));
        }
		return Datatables::of($users)
		->edit_column('name', '<a href="manage/usersdetail/{{base64_encode($id)}} ">{{$name}}</a>')
		->make(true);
	}
    
   
}