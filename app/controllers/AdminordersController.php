<?php

class AdminordersController extends BaseController {

    protected $layout = 'layout.managemaster';
    public function anyAllnewordersdata()
    {
      $neworders = DB::table('main_orders as o')
        ->select('o.order_id', 'o.name','o.address','o.city', 
               DB::raw('CASE o.payment_type when 1 then "Bank details" when 2 then "Cash" END as payment_type'), 
               DB::raw('(select sum(quantity) from main_orders_options where order_id = o.order_id) as total_qty'),
               'o.grand_total')
      ->where('o.status', 1)
      ->where('o.is_paid', 0)
      ->whereRaw('DATE(o.created_at) = date(NOW())')
      ->where('o.is_complete', 0);

      return Datatables::of($neworders)
      ->edit_column('view_order', '<a href="manage/order_detail/{{base64_encode($order_id)}}" class="btn btn-green"><i class="fa fa-eye" aria-hidden="true"></i></a><a href="javascript:;" data-bind="{{base64_encode($order_id)}}" class="btn btn-default complete_order"><i class="fa fa-check" aria-hidden="true"></i></a><a href="javascript:;" data-bind="{{base64_encode($order_id)}}" class="btn btn-danger cancel_order"><i class="fa fa-times" aria-hidden="true"></i></a><a href="manage/edit_order/{{base64_encode($order_id)}}" class="btn btn-green"><i class="fa fa-pencil" aria-hidden="true"></i></a>')
      ->make(true);
  }

   public function anyAllorderlist()
   {
      $params = Input::all();
      
      $neworders = DB::table('main_orders as o')
       ->select('o.order_id', 'o.name','o.address','o.city', 
                DB::raw('CASE o.payment_type when 1 then "Bank details" when 2 then "Cash" END as payment_type'),
               DB::raw('(select sum(quantity) from main_orders_options where order_id = o.order_id) as total_qty'),
               'o.grand_total')
      ->where(function($query) use ($params)
      {
            if(isset($params['start_date']) && $params['start_date']):
                 $query->whereRaw('date(o.created_at) >= "'.$params['start_date'].'"');
            endif;
           if(isset($params['end_date']) && $params['end_date']):
                 $query->whereRaw('date(o.created_at) <= "'.$params['end_date'].'"');
            endif;
      })
      ->where('o.status', 1)
      
      ->orderby('o.order_id', 'desc')
      ->where('o.is_paid', 0)
      ->where('o.is_complete', 0);

      return Datatables::of($neworders)
       ->edit_column('view_order', '<a href="manage/order_detail/{{base64_encode($order_id)}}" class="btn btn-green"><i class="fa fa-eye" aria-hidden="true"></i></a><a href="javascript:;" data-bind="{{base64_encode($order_id)}}" class="btn btn-default complete_order"><i class="fa fa-check" aria-hidden="true"></i></a><a href="javascript:;" data-bind="{{base64_encode($order_id)}}" class="btn btn-danger cancel_order"><i class="fa fa-times" aria-hidden="true"></i></a><a href="manage/edit_order/{{base64_encode($order_id)}}" class="btn btn-green"><i class="fa fa-pencil" aria-hidden="true"></i></a>')
      ->make(true);
  }
    
   public function anyAllordersdata()
   { 
      $neworders = DB::table('main_orders as o')
      ->select('o.order_id', 'o.name','o.address','o.city', 
                DB::raw('CASE o.payment_type when 1 then "Bank details" when 2 then "Cash" END as payment_type'),
               DB::raw('(select sum(quantity) from main_orders_options where order_id = o.order_id) as total_qty'),
               'o.grand_total')
      ->where('o.status', 1)
      ->where('o.is_paid', 1)
      ->where('o.is_complete', 1);

      return Datatables::of($neworders)
      ->edit_column('view_order', '<a href="manage/order_detail/{{base64_encode($order_id)}}" class="btn btn-green">View Order</a>')
      ->make(true);
  }

   public function getOrderdetails(){
      
    $returndata = array();
    $returndata['success'] = false;
    $Id = Input::get('id');
    $OrderDetails = Orders::OrderDetail($Id);

    if($OrderDetails['success'] == 1){
        $returndata['success'] = true;
        $returndata['details'] = $OrderDetails['data'];
    }
    return Response::json($returndata);
}

   public function postCompleteorder(){
    $returndata = array();
    $returndata['success'] = false;
    $returndata['message'] = ERROR_COMPLETE_ORDER;
    $Id = Input::get('order_id');
    $completeorder = Orders::completeorder(base64_decode($Id));
    if($completeorder == 1){
        $returndata['success'] = true;
        $returndata['message'] = SUCCESS_COMPLETE_ORDER;
    }
    return Response::json($returndata);
}

   public function postCancelorder(){
    $returndata = array();
    $returndata['success'] = false;
    $returndata['message'] = ERROR_CANCEL_ORDER;
    $Id = Input::get('order_id');
    $completeorder = Orders::cancelorder(base64_decode($Id));
    if($completeorder == 1){
        $returndata['success'] = true;
        $returndata['message'] = SUCCESS_CANCEL_ORDER;
    }
    return Response::json($returndata);
 }

  public function postUpdateorder(){
    $returndata = array();
    $returndata['success'] = false;
    $returndata['message'] = ERROR_UPDATE_ORDER;
    $completeorder = Orders::updateorder(Input::all());
    if($completeorder == 1){
        $returndata['success'] = true;
        $returndata['message'] = SUCCESS_UPDATE_ORDER;
    }
    return Response::json($returndata);
}

  public function getAllcomments(){

    $getAllComments = Orders::getAllcomments(Input::all());
    return Response::json($getAllComments);
  
  }

  ## insert into the database.
  public function getInsertcomments(){

    $getinserted = Orders::getInsertcomments(Input::all());
    return Response::json($getinserted);

  }

  public function getGeneratecsv() {

    //global declaration
    $ResponseData = array();
    $UpdateArray = array();
    //get post data
    $PostData = Input::all();

    //call function to get user data
    $ResultUserList = Orders::OrderslistingCSV();

    //define array.
    $OutPutHeaderArray = array('Order Id', 'Product Name', 'Sheep type', 'Product size', 'Quantity', 'Grand total', 'Address');
    //call excel library for file generation.
    Excel::create('Orders list-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
        $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {

            //append header
            $sheet->rows(array($OutPutHeaderArray));
            //check for data availability
            if (isset($ResultUserList) && !empty($ResultUserList)) {
                //loop through data and create csv
                foreach ($ResultUserList as $UserKey => $UserVal) {
                    $sheet->appendRow(array($UserVal['order_id'], $UserVal['product_name'], $UserVal['sheep_type'], $UserVal['product_size'], $UserVal['quantity'], $UserVal['grand_total'], $UserVal['address']));
                }
            }
        });
    })->export('xlsx');
    exit;

} 

  public function getGeneratepdf() {
    //global declaration
    $ResponseData = array();
    $UpdateArray = array();
    //get post data
    $PostData = Input::all();

    //call function to get user data
    $ResultUserList = Orders::OrderslistingCSV();

            //define array.
    $OutPutHeaderArray = array('Order Id', 'Product Name', 'Sheep type', 'Product size', 'Quantity', 'Grand total', 'Address');
            //call excel library for file generation.
    Excel::create('Order List-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
        $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {
            //append header
            $sheet->rows(array($OutPutHeaderArray));
                    //check for data availability
            if (isset($ResultUserList) && !empty($ResultUserList)) {
                        //loop through data and create csv
                foreach ($ResultUserList as $UserKey => $UserVal) {
                    $sheet->appendRow(array($UserVal['order_id'], $UserVal['product_name'], $UserVal['sheep_type'], $UserVal['product_size'], $UserVal['quantity'], $UserVal['grand_total'], $UserVal['address']));
                }
            }
        });
    })->export('pdf');
    exit;
}

  public function getGeneratecompleteordercsv() {

    //global declaration
    $ResponseData = array();
    $UpdateArray = array();
    //get post data
    $PostData = Input::all();

    //call function to get user data
    $ResultUserList = Orders::CompleteOrderslistingCSV();

    //define array.
    $OutPutHeaderArray = array('Order Id', 'Product Name', 'Sheep type', 'Product size', 'Quantity', 'Grand total', 'Address');
    //call excel library for file generation.
    Excel::create('Orders list-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
        $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {

            //append header
            $sheet->rows(array($OutPutHeaderArray));
            //check for data availability
            if (isset($ResultUserList) && !empty($ResultUserList)) {
                //loop through data and create csv
                foreach ($ResultUserList as $UserKey => $UserVal) {
                    $sheet->appendRow(array($UserVal['order_id'], $UserVal['product_name'], $UserVal['sheep_type'], $UserVal['product_size'], $UserVal['quantity'], $UserVal['grand_total'], $UserVal['address']));
                }
            }
        });
    })->export('xlsx');
    exit;

} 

  public function getGeneratecompleteorderpdf() {
    
    //global declaration
    $ResponseData = array();
    $UpdateArray = array();
    //get post data
    $PostData = Input::all();

    //call function to get user data
    $ResultUserList = Orders::CompleteOrderslistingCSV();

    //define array.
    $OutPutHeaderArray = array('Order Id', 'Product Name', 'Sheep type', 'Product size', 'Quantity', 'Grand total', 'Address');
    //call excel library for file generation.
    Excel::create('Order List-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
        $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {
            //append header
            $sheet->rows(array($OutPutHeaderArray));
                    //check for data availability
            if (isset($ResultUserList) && !empty($ResultUserList)) {
                        //loop through data and create csv
                foreach ($ResultUserList as $UserKey => $UserVal) {
                    $sheet->appendRow(array($UserVal['order_id'], $UserVal['product_name'], $UserVal['sheep_type'], $UserVal['product_size'], $UserVal['quantity'], $UserVal['grand_total'], $UserVal['address']));
                }
            }
        });
    })->export('pdf');
    exit;
    
 }



    
}