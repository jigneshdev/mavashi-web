<?php

class AdminconfigsettingController extends BaseController {

    protected $layout = 'layout.managemaster';
	
    public function postAddsheepsize() {
		$result_forgot = Configsetting::Addsheepsize(Input::all());
		if ($result_forgot) {
			$returndata['success'] = true;
			$returndata['message'] = SUCCESS_ADD_SHEEP_SIZE;
		} else {
			$returndata['success'] = false;
			$returndata['message'] = ERROR_ADD_SHEEP_SIZE;
		}
		return Response::json($returndata);
    }

    public function postAddsheeptype() {
        $result_forgot = Configsetting::Addsheeptype(Input::all());
        if ($result_forgot) {
            $returndata['success'] = true;
            $returndata['message'] = SUCCESS_ADD_SHEEP_TYPE;
        } else {
            $returndata['success'] = false;
            $returndata['message'] = ERROR_ADD_SHEEP_TYPE;
        }
        return Response::json($returndata);
    }

    public function getContent() {
        $returndata = array();
        $result = Configsetting::content();
        if ($result) {
            $returndata['success'] = true;
            $returndata['data'] = $result['data'];
        } else {
            $returndata['success'] = false;
        }
        return Response::json($returndata);
    }
    
    public function getContact() {
        $returndata = array();
        $result = DB::table('admin_config')->select('setting_value')->first();
        $result = json_decode(json_encode($result), true);
        if ($result) {
            $returndata['success'] = true;
            $returndata['data'] = $result['setting_value'];
        } else {
            $returndata['success'] = false;
        }
        return Response::json($returndata);
    }
    
    public function postUpdatecontact(){
        
        $Params = Input::all();
        $returndata = array();
        $result = DB::table('admin_config')->where(array('id' => 1))->update(array('setting_value' => $Params['phone_num']));
        
        if ($result) {
            $returndata['message'] = 'Contact Number updated.';
            $returndata['success'] = true;
        } else {
            $returndata['message'] = 'Something went wrong. Please try again!';
            $returndata['success'] = false;
        }
        return Response::json($returndata);
        
    }

    public function postUpdatetext() {

        $result_forgot = Configsetting::Updatetext(Input::all());
        if ($result_forgot) {
            $returndata['success'] = true;
            $returndata['message'] = SUCCESS_UPDATE_CONTENT;
        } else {
            $returndata['success'] = false;
            $returndata['message'] = ERROR_UPDATE_CONTENT;
        }
        return Response::json($returndata);
    }
    
     ## all inqueries listing.
     public function anySheeptypelist()
        {
          $neworders = DB::table('sheep_types as o')
          ->select('o.sheep_type_id', 'o.sheep_name')
          ->where('o.status', 1)
          ->orderby('sheep_type_id', 'desc');

          return Datatables::of($neworders)
          ->edit_column('action', '<a href="javascript:;" data-id="{{$sheep_type_id}}" data-name="{{$sheep_name}}" class="btn btn-green edit_sheep_type"><i class="fa fa-pencil" aria-hidden="true"></i></a><a href="javascript:;" data-id="{{$sheep_type_id}}" class="btn btn-danger sheep_type_delete"><i class="fa fa-times" aria-hidden="true"></i></a>')
          ->make(true);
      }
    
     
     public function anySheepsizelist()
        {
          $neworders = DB::table('sheep_size as o')
          ->select('o.size_id', 'o.size_name')
         ->where('o.status', 1)
          ->orderby('size_id', 'desc');

          return Datatables::of($neworders)
          ->edit_column('action', '<a href="javascript:;"  data-id="{{$size_id}}" data-name="{{$size_name}}" class="btn btn-green sheep_size_edit"><i class="fa fa-pencil" aria-hidden="true"></i></a><a href="javascript:;" data-id="{{$size_id}}" class="btn btn-danger sheep_size_delete"><i class="fa fa-times" aria-hidden="true"></i></a>')
          ->make(true);
      }
    
     public function postUpdatesize(){

         $in = Input::all();
         $returndata['success'] = false;
            
         if(isset($in['size_id']) && isset($in['size_name']) && $in['size_name'] !=''){
             $update  = DB::table('sheep_size')
                            ->where(array('size_id' => $in['size_id']))
                            ->update(array('size_name'=> $in['size_name']));
             if($update){
                  $returndata['success'] = true;
             }
         }
         return $returndata;
     }
    
    ## to update the sheep type
    public function postUpdatetype(){
        
         $in = Input::all();
         $returndata['success'] = false;
            
         if(isset($in['size_id']) && isset($in['type_name']) && $in['type_name'] !=''){
             $update  = DB::table('sheep_types')
                            ->where(array('sheep_type_id' => $in['size_id']))
                            ->update(array('sheep_name'=> $in['type_name']));
             if($update){
                  $returndata['success'] = true;
             }
         }
         return $returndata;
        
        
    }
    
    public function postDeletetype(){
        
      $in = Input::all();
        
      if(isset($in['delete_id']) && $in['delete_id'] !=''){
             $update  = DB::table('sheep_types')
                            ->where(array('sheep_type_id' => $in['delete_id']))
                            ->update(array('status' => 0));
             if($update){
                  $returndata['success'] = true;
             }
         }
         return $returndata;
    }
    
    public function postDeletesize(){
        
      $in = Input::all();
        
      if(isset($in['delete_id']) && $in['delete_id'] !=''){
             $update  = DB::table('sheep_size')
                            ->where(array('size_id' => $in['delete_id']))
                            ->update(array('status' => 0));
             if($update){
                  $returndata['success'] = true;
             }
         }
         return $returndata;
    }
}