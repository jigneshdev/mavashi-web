<?php

class AdminproductsController extends BaseController {

    protected $layout = 'layout.managemaster';

    ## To get the product listings.
    public function anyAllproductsdata()
    {
        $products = DB::table('products as pp')
            ->select('pp.product_id', 
                     'pp.product_name',
                     'pp.price', 
                     'pp.product_image',
                     'pp.created_on',
                     DB::raw('(SELECT count(moo.product_id) FROM main_orders as mo LEFT JOIN main_orders_options as moo on mo.order_id = moo.order_id WHERE moo.product_id = pp.product_id AND mo.is_paid = 1 AND mo.is_complete = 1 ) AS orders'))
            ->where('pp.status', 1)
            ->orderby('pp.product_id', 'desc');
            
          return Datatables::of($products)
            ->edit_column('product_image', '<a href="manage/product_detail/{{base64_encode($product_id)}}"><img src="{{PRODUCT_IMAGE_URL.$product_image}}" height="100" width="100"/></a>')
            ->edit_column('edit_product', '<a href="manage/edit_product/{{base64_encode($product_id)}}" class="btn btn-green">Edit</a><a href="javascript:;" data-id="{{$product_id}}" class="btn btn-danger product_delete"><i class="fa fa-times" aria-hidden="true"></i></a>')
            ->make(true);
        
    }

    // To add new product.
    public function postAddproduct()
    {
        $returndata = array();
        
        $result = Products::Addproductinfo(Input::all());
            if ($result) {
                $returndata['success'] = true;
                $returndata['message'] = SUCCESS_PRODUCT_ADD;
            }
            else{
                $returndata['success'] = false;
                $returndata['message'] = ERROR_PRODUCT_ADD;
            }
        
        return Response::json($returndata);
    }

    // To edit product.
    public function postEditproduct()
    {
        $returndata = array();
        
        $result = Products::Editproductinfo(Input::all());
            if ($result) {
                $returndata['success'] = true;
                $returndata['message'] = SUCCESS_PRODUCT_EDIT;
            }
            else{
                $returndata['success'] = false;
                $returndata['message'] = ERROR_PRODUCT_EDIT;
            }
        
        return Response::json($returndata);
    }

    public function getProductetails(){
        
        $returndata = array();
        $returndata['success'] = false;
        $Id = Input::get('id');
        $ProductDetails = Products::ProductDetail($Id);
        
        if($ProductDetails['success'] == 1){
            $returndata['success'] = true;
            $returndata['details'] = $ProductDetails['data'];
        }
        return Response::json($returndata);
    }

    public function getSheeptypes(){
        $returndata = array();
        $returndata['success'] = false;
        $SheepDetails = Products::Sheeptypes();
        
        if($SheepDetails['success'] == 1){
            $returndata['success'] = true;
            $returndata['details'] = $SheepDetails['data'];
        }
        return Response::json($returndata);
    }

    public function getSheepsizes(){
        $returndata = array();
        $returndata['success'] = false;
        $SizeDetails = Products::Sheepsizes();
        
        if($SizeDetails['success'] == 1){
            $returndata['success'] = true;
            $returndata['details'] = $SizeDetails['data'];
        }
        return Response::json($returndata);
    }

    public function getGeneratecsv() {

    //global declaration
    $ResponseData = array();
    $UpdateArray = array();
    //get post data
    $PostData = Input::all();

    //call function to get user data
    $ResultUserList = Products::ProductslistingCSV();

    //define array.
    $OutPutHeaderArray = array('Product Id', 'Product Name', 'Price');
    //call excel library for file generation.
    Excel::create('Orders list-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
        $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {

            //append header
            $sheet->rows(array($OutPutHeaderArray));
            //check for data availability
            if (isset($ResultUserList) && !empty($ResultUserList)) {
                //loop through data and create csv
                foreach ($ResultUserList as $UserKey => $UserVal) {
                    $sheet->appendRow(array($UserVal['product_id'], $UserVal['product_name'], $UserVal['price']));
                }
            }
        });
    })->export('xlsx');
    exit;

} 

    public function getGeneratepdf() {
        //global declaration
        $ResponseData = array();
        $UpdateArray = array();
        //get post data
        $PostData = Input::all();

        //call function to get user data
        $ResultUserList = Products::ProductslistingCSV();

                //define array.
        $OutPutHeaderArray = array('Product Id', 'Product Name', 'Price');
                //call excel library for file generation.
        Excel::create('Order List-' . date('dSMY'), function($excel) use($OutPutHeaderArray, $ResultUserList) {
            $excel->sheet('Sheetname', function($sheet) use($OutPutHeaderArray, $ResultUserList) {
                //append header
                $sheet->rows(array($OutPutHeaderArray));
                        //check for data availability
                if (isset($ResultUserList) && !empty($ResultUserList)) {
                            //loop through data and create csv
                    foreach ($ResultUserList as $UserKey => $UserVal) {
                        $sheet->appendRow(array($UserVal['product_id'], $UserVal['product_name'], $UserVal['price']));
                    }
                }
            });
        })->export('pdf');
        exit;
    }
    
    public function postDeleteproduct(){
        
        $returndata = array();
        $returndata['success'] = false;
        $product_id = Input::all();
        
        if(isset($product_id) && count($product_id) > 0){
            $update = DB::table('products')
                        ->where(array('product_id' => $product_id['product_id']))
                        ->update(array('status' => 0));
            if($update){ $returndata['success'] = true; }
        }
        
        return Response::json($returndata);
    }
}
