<?php

class AdmindashboardController extends BaseController { 
    ##Jigs Virani 21 Oct 2016.
    ## To get latest stastics from system.
  public function anyLateststats() {

    $data = array();
    $data['success'] = false;
    $userdata = Dashboard::Totaldatacount();
    
    if ($userdata) 
    {
        $total_products = empty($userdata['total_products']['total_count']) ? '0' : $userdata['total_products']['total_count'];
        $total_order = empty($userdata['total_order']['total_count']) ? '0' : $userdata['total_order']['total_count'];
        $today_order = empty($userdata['today_order']['total_count']) ? '0' : $userdata['today_order']['total_count'];
        $complete_orders = empty($userdata['complete_orders']['total_count']) ? '0' : $userdata['complete_orders']['total_count'];
        $pending_orders = empty($userdata['pending_orders']['total_count']) ? '0' : $userdata['pending_orders']['total_count'];
        $orders_by_week = empty($userdata['orders_by_week']['total_count']) ? '0' : $userdata['orders_by_week']['total_count'];
        $orders_by_month = empty($userdata['orders_by_month']['total_count']) ? '0' : $userdata['orders_by_month']['total_count'];
        $orders_by_year = empty($userdata['orders_by_year']['total_count']) ? '0' : $userdata['orders_by_year']['total_count'];
        $total_sales = empty($userdata['total_sales']['total_sales']) ? '0' : $userdata['total_sales']['total_sales'];
        $total_profit = empty($userdata['total_profit']) ? '0' : $userdata['total_profit'];
        
        
       $result = array(
          'total_products' => $total_products,
          'total_order' => $total_order,
          'today_order' => $today_order,
          'complete_orders' => $complete_orders,
          'pending_orders' => $pending_orders,
          'orders_by_week' => $orders_by_week,
          'orders_by_month' => $orders_by_month,
          'orders_by_year' => $orders_by_year,
          'total_sales' => $total_sales,
          'total_profit'=> $total_profit
       );

        $data['success'] = true;
        $data['data'] = $result;            
    }
   return Response::json($data);
 }
    
    ## all inqueries listing.
     public function anyInquerylist()
        {
          $neworders = DB::table('contact_us as o')
          ->select('o.id', 'o.name','o.email','o.contact_num','o.message','o.contact_date')
          ->orderby('id', 'desc');

          return Datatables::of($neworders)
          //->edit_column('view_order', '<a href="manage/order_detail/{{base64_encode($order_id)}}" class="btn btn-green"><i class="fa fa-eye" aria-hidden="true"></i></a><a href="javascript:;" data-bind="{{base64_encode($order_id)}}" class="btn btn-default complete_order"><i class="fa fa-check" aria-hidden="true"></i></a><a href="javascript:;" data-bind="{{base64_encode($order_id)}}" class="btn btn-danger cancel_order"><i class="fa fa-times" aria-hidden="true"></i></a><a href="manage/edit_order/{{base64_encode($order_id)}}" class="btn btn-green"><i class="fa fa-pencil" aria-hidden="true"></i></a>')
          ->make(true);
      }

}
