<?php

class UsermanageController  extends BaseController {

    protected $layout = 'layout.managemaster';
	
    public function anyAlluserlist() {
        
         $neworders = DB::table('manage_users as o')
            ->select('o.id', 'o.name', 'o.email_address', DB::raw('CASE o.admin_type WHEN 1 THEN "Super Admin" WHEN 2 THEN "Section Admin" END AS admin_type'))
            ->where('o.status', 1)
            ->orderby('o.id', 'desc');

          return Datatables::of($neworders)
          ->edit_column('action', '<a href="manage/userdetail/{{base64_encode($id)}}" data-id="{{$id}}" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a><a href="javascript:;" data-id="{{$id}}" class="btn btn-danger sheep_type_delete"><i class="fa fa-times" aria-hidden="true"></i></a>')
          ->make(true);
        
        
    }
    
    ## To insert the new user.
    public function anyAddnewuser(){
        
        $PostData = Input::all();
        $returndata = Users::anyAddnewuser($PostData);
        return Response::json($returndata);
        
    }
    
    ## To check wether email is exist or not.
    public function anyCheckemailexist(){
        
        $PostData = Input::all();
        $returndata = true;
        if(isset($PostData['email_address']) && $PostData['email_address']){
            
            $checkEmail = DB::table('manage_users')->select('email_address')->where('email_address', $PostData['email_address'])->first();
            if(isset($checkEmail->email_address) && $checkEmail->email_address != ''){
               $returndata = false;
            }else{
               $returndata = true;
            }
        }else{
            $returndata = true;
        }
        
        return Response::json($returndata);
    }
    
    // To delete the user.
    public function anyDeleteuser(){
         
        $returndata['success'] = true;
        
        $params = Input::all();
        if(isset($params['user_id']) && $params['user_id'] != ''){
            //$deleteuser = DB::table('manage_users')->where('id', $params['user_id'])->delete();
            $returndata['message'] = 'User deleted successfully.';
        }else{
            $returndata['message'] = 'Something went wrong.';
            $returndata = false;
        }
        
         return Response::json($returndata);
    }
    
    public function getUserinfo(){
        
        $returndata['success'] = true;
        $params = Input::all();
        
        if(isset($params['user_id']) && $params['user_id'] != ''){
            
            $users_array = DB::table('manage_users')->select('*')->where('id', base64_decode($params['user_id']))->first();
            $returndata['details'] =  json_decode(json_encode($users_array), true);
            
        } else{
            
            $returndata['message'] = 'Something went wrong.';
            $returndata = false;
        }
        return Response::json($returndata);
    }
    
    // To Edit the user
    public function postEdituser(){
        
        $returndata['success'] = true;
        $returndata = Users::edituser(Input::all());
        return Response::json($returndata);    
    
    }
}