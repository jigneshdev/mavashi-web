<?php

class ApiproductController extends BaseController { 
   
    ##Jigs Virani 21 Oct 2016.
    ## To get latest stastics from system.
  public function getProductlist() {
      
        $ReturnData = array();
        $PostData = Input::all();
        
        $getproduct = Apiproducts::productlist();
        
        //proceed admin insertion
        if (isset($getproduct) && !empty($getproduct)) {
                $ReturnData['status'] = '1';
                $ReturnData['message'] = 'done';
                $ReturnData['data'] = $getproduct;
           
        } else {
                $ReturnData['status'] = '0';
                $ReturnData['message'] = 'empty';
                $ReturnData['data'] = array();
        }
   
   return Response::json($ReturnData);
 }
    
     ##Jigs Virani 21 Oct 2016.
    ## To get latest stastics from system.
      public function getBanklist() {

            $ReturnData = array();
            $PostData = Input::all();

            $getproduct = Apiproducts::getBanklist();

            //proceed admin insertion
            if (isset($getproduct) && !empty($getproduct)) {
                    $ReturnData['status'] = '1';
                    $ReturnData['message'] = 'done';
                    $ReturnData['data'] = $getproduct;

            } else {
                    $ReturnData['status'] = '0';
                    $ReturnData['message'] = 'empty';
                    $ReturnData['data'] = array();
            }

       return Response::json($ReturnData);
     }
    
    public function postProductinfo(){
        
         //global declaration
        $ResponseData['success'] =  STATUS_FALSE;
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
             //make validator for facebook
            $ValidateFacebook = Validator::make(array(
                        'product_id' => Input::get('product_id'),
                        ), array(
                        'product_id' => 'required',
                 ));
            if ($ValidateFacebook->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['message'] = $ValidateFacebook->messages()->first();
                $ResponseData['data'] = array();
            } else {
                
                $getList = Apiproducts::postProductinfo($PostData);
                
                if(isset($getList['success']) && $getList['success'] == true){
                    
                     $ResponseData['success'] =  STATUS_TRUE;
                     $ResponseData['message'] = 'Success';
                     $ResponseData['data'] = $getList['data'];
                                    
                }else {
                     $ResponseData['success'] =  STATUS_FALSE;
                     $ResponseData['message'] = 'empty';
                     $ResponseData['data'] = array();
                }  
            }
        } else{
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['message'] = INVALID_PARAMS;
            $ResponseData['data'] = array();
            
        }
        
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    ##
    public function postOrderinfo(){
        
        //global declaration
        $ResponseData['success'] =  STATUS_FALSE;
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
             //make validator for facebook
            $ValidateFacebook = Validator::make(array(
                        'product_id' => Input::get('product_id'),
                        'address' => Input::get('address'),
                        'name' => Input::get('name'),
                        'name' => Input::get('email'),
                        'number' => Input::get('number'),
                        'city' => Input::get('city'),
                        'grand_total' => Input::get('grand_total'),
                        //'bank_detail' => Input::get('bank_detail'),
                        'sheep_type' => Input::get('sheep_type'),
                        'product_size' => Input::get('product_size'),
                        ), array(
                        'product_id' => 'required',
                        'address' => 'required',
                         'name' => 'required',
                         'name' => 'required',
                         'city' => 'required',
                         'number' => 'required',
                        'grand_total' => 'required',
                        //'bank_detail' => 'required',
                        'sheep_type' => 'required',
                        'product_size' => 'required'
                 ));
            if ($ValidateFacebook->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['message'] = $ValidateFacebook->messages()->first();
                $ResponseData['data'] = array();
            } else {
                
                $getList = Apiproducts::postOrderinfo($PostData);
                
                if(isset($getList['success']) && $getList['success'] == true){
                    
                     $ResponseData['success'] =  STATUS_TRUE;
                     $ResponseData['message'] = 'Success';
                     $ResponseData['data'] = $getList['data'];
                                    
                }else {
                     $ResponseData['success'] =  STATUS_FALSE;
                     $ResponseData['message'] = 'empty';
                     $ResponseData['data'] = array();
                }  
            }
        } else{
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['message'] = INVALID_PARAMS;
            $ResponseData['data'] = array();
            
        }
        
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
        
        
        
    }
    
    public function postInquery(){
        //global declaration
        $ResponseData['success'] =  STATUS_FALSE;
        $ResponseData = array();

        //get data from request and process
        $PostData = Input::all();

        if (isset($PostData) && !empty($PostData)) {
             //make validator for facebook
            $ValidateFacebook = Validator::make(array(
                        'name' => Input::get('name'),
                        'email' => Input::get('email'),
                        'contact_num' => Input::get('contact_num'),
                        'message' => Input::get('message'),
                        ), array(
                        'name' => 'required',
                        'email' => 'required',
                        'contact_num' => 'required',
                        'message' => 'required',
                     
                 ));
            if ($ValidateFacebook->fails()) {
                $ResponseData['success'] =  STATUS_FALSE;
                $ResponseData['message'] = $ValidateFacebook->messages()->first();
                $ResponseData['data'] = array();
            } else {
                
                Apiproducts::postInqueryinfo($PostData);
                
                $email_address = Input::get('email');
                $name = Input::get('name');
                $contact_num = Input::get('contact_num');
                $message = Input::get('message');
                
                 $html = '';
                 $html .= '<strong>Email Address :</strong> '. $email_address.'<br>';
                 $html .= '<strong>Name :</strong> '. $name.'</br>';
                 $html .= '<strong>Feedback :</strong> '. $message.'</br>';
                 $html .= '<strong>Contact Number :</strong> '. $contact_num.'</br>';
                    
                    Mail::send(array(), array(), function ($message) use ($html) {
                           // $message->to('viranijignesh91@gmail.com')
                             $message->to('ahmedalamin@zoho.com')->cc('viranijignesh91@gmail.com')
                            ->subject('MaWashi Team :  Feedback')
                            ->from('no-replay@mawashi.com')
                            ->setBody($html, 'text/html');
                        });
                
                 $getList['success'] = true;
                 $getList['data']=  array();
                
                if(isset($getList['success']) && $getList['success'] == true){
                    
                     $ResponseData['success'] =  STATUS_TRUE;
                     $ResponseData['message'] = 'Success';
                     $ResponseData['data'] = $getList['data'];
                                    
                }else {
                     $ResponseData['success'] =  STATUS_FALSE;
                     $ResponseData['message'] = 'empty';
                     $ResponseData['data'] = array();
                }  
            }
        } else{
            $ResponseData['success'] =  STATUS_FALSE;
            $ResponseData['message'] = INVALID_PARAMS;
            $ResponseData['data'] = array();
        }
        return Response::json($ResponseData, 200, [], JSON_NUMERIC_CHECK);
    }
    
    public function getContents(){
        
        
        $ReturnData = array();
        $PostData = Input::all();
        
        $getproduct = DB::table('content')->select('about_us', 'our_vision', 'privacy_policy')->first();
        $getproduct = json_decode(json_encode($getproduct), true);
        
        //proceed admin insertion
        if (isset($getproduct) && !empty($getproduct)) {
                $ReturnData['status'] = '1';
                $ReturnData['message'] = 'done';
                $ReturnData['data'] = $getproduct;
           
        } else {
                $ReturnData['status'] = '0';
                $ReturnData['message'] = 'empty';
                $ReturnData['data'] = array();
        }
   
        return Response::json($ReturnData, 200, [], JSON_NUMERIC_CHECK);
        
    }
    
     ## To get the contact number.
    public function getContact(){
        
         $ReturnData = array();
        $getcontact = DB::table('admin_config')->select('setting_value')->where('id', 1)->first();
        $getcontact = json_decode(json_encode($getcontact), true);
        
        if($getcontact){
                $ReturnData['status'] = true;
                //$ReturnData['message'] = 'done';
                $ReturnData['data'] = $getcontact['setting_value'];
            
        } else{
                $ReturnData['status'] = false;
                //$ReturnData['message'] = 'done';
                $ReturnData['data'] = "";
            
        }
        return Response::json($ReturnData, 200, [], JSON_NUMERIC_CHECK);
    }
}
