<?php 

class Dashboard extends Eloquent {
    
    ##Jigs Virani 20 Oct 2015
    ## To get total numbers of new users.
	public static function Totaldatacount()
	{
		$result = array();
		//select  as total_count from front_users.
		$result['total_products'] = DB::table('products')
                ->select(DB::raw('count(product_id) as total_count'))->where('status',1)
				->first();

		$result['total_order'] = DB::table('main_orders')
                ->select(DB::raw('count(order_id) as total_count'))
                  ->where('status', 1)
				->first();

		$result['today_order'] = DB::table('main_orders')
                ->select(DB::raw('count(order_id) as total_count'))
                ->whereRaw('DATE(created_at) = date(NOW())')
                ->where('status', 1)
                ->where('is_complete', '0')
                ->where('is_paid', '0')
				->first();

		$result['complete_orders'] = DB::table('main_orders')
                ->select(DB::raw('count(order_id) as total_count'))
				->where('is_complete', '1')
              ->where('status', 1)
				->first();

		$result['pending_orders'] = DB::table('main_orders')
                ->select(DB::raw('count(order_id) as total_count'))
				->where('is_complete', '0')
                ->where('is_paid', '0')
                ->where('status', 1)
				->first();

		$result['orders_by_week'] = DB::table('main_orders')
                ->select(DB::raw('count(order_id) as total_count'))
				->whereRaw('yearweek(DATE(created_at), 1) = yearweek(curdate(), 1)')
                  ->where('status', 1)
				->first();

		$result['orders_by_month'] = DB::table('main_orders')
                ->select(DB::raw('count(order_id) as total_count'))
				->whereRaw('MONTH (created_at) = MONTH(NOW())')
                ->where('status', 1)
				->first();

		$result['orders_by_year'] = DB::table('main_orders')
                ->select(DB::raw('count(order_id) as total_count'))
				->whereRaw('YEAR (created_at) = YEAR(NOW())')
                  ->where('status', 1)
				->first();

		$result['total_sales'] = DB::table('main_orders')
                ->select(DB::raw('sum(grand_total) as total_sales'))
				->where('is_complete', '1')
                ->first();
        
        $total_cost = DB::table('main_orders')
                                  ->select(DB::raw('sum(cost) as total_cost'))
                                  ->where('is_complete', '1')
                                  ->first();
        
        $app_price = DB::table('main_orders')
                                  ->select(DB::raw('sum(app_price) as app_price'))
                                  ->where('is_complete', '1')
                                  ->first();
        
        $result['total_profit'] = $app_price->app_price - $total_cost->total_cost;       
		
        $returndata = json_decode(json_encode($result), true);
        return $returndata;
	}
    
    
}

?>