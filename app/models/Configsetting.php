<?php

class Configsetting extends Eloquent {

	// check image exist or not and return image 
	public static function ImageExist($image) {
        $filename = PRODUCT_IMAGE_URL . $image;
        $physicalfilename = PRODUCT_IMAGE_PATH.$image;
        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = PRODUCT_IMAGE_URL . 'product_placeholder.png';
        }
        return $filename;
    }
	
    public static function UserlistingCSV() {
        
        $users_obj = DB::table('front_users')
                ->select('id', 'name', 'email_address', DB::raw('CASE WHEN app_user_status = "1" THEN "Questioner" WHEN app_user_status = "2" THEN "Agent" ELSE "Both" END as roles'),
                         'phone_number', 'my_referral_code', 'last_login')
                ->where('user_delete', '0')
                ->get();
        $users_array = json_decode(json_encode($users_obj), true);
        return $users_array;
    }
	
	public static function Addsheepsize($data) {
		$data_temp = array(
			'size_name' => $data['size_name'],
			'status' => 1,
			'created_on' => date('Y-m-d h:i:s')
		);
		$update_status = DB::table('sheep_size')->insert($data_temp);
        return true;
	}

	public static function Addsheeptype($data) {
		$data_temp = array(
			'sheep_name' => $data['sheep_type'],
			'status' => 1,
			'created_on' => date('Y-m-d h:i:s')
		);
		$update_status = DB::table('sheep_types')->insert($data_temp);
        return true;
	}
    
    public static function postAddnewbank($data){
        
        $return_array['success'] = false; 
        
        if(isset($data) && count($data) > 0){
            
            
                $created_on  = date('Y-m-d H:i:s');	
            
    	       $destinationPath = PRODUCT_IMAGE_PATH;
        
                if (Input::hasFile('bank_logo')) {

                    $filename = rand() . time() . str_replace(' ', '_', Input::file('bank_logo')->getClientOriginalName());

                    Image::make(Input::file('bank_logo'))->save($destinationPath . $filename);

                    $data_temp = array(
                        'name' => $data['bank_name'],
                        'number' => $data['acc_num'],
                        'imag_logo' => $filename,
                        'status' => 1,
                        'created_on' => $created_on
                        );
                }

    	       $result = DB::table('bank_list')->insertGetId($data_temp);
                if($result){
                        $return_array['success'] = true; 
                }
            
        }
        return $return_array;
    }
    
    
//     public static function postAddnewbank($data){
//        
//        $return_array['success'] = false; 
//        
//        if(isset($data) && count($data) > 0){
//            
//            
//                $created_on  = date('Y-m-d H:i:s');	
//            
//    	       $destinationPath = PRODUCT_IMAGE_PATH;
//        
//                if (Input::hasFile('bank_logo')) {
//
//                    $filename = rand() . time() . str_replace(' ', '_', Input::file('bank_logo')->getClientOriginalName());
//
//                    Image::make(Input::file('bank_logo'))->save($destinationPath . $filename);
//
//                    $data_temp = array(
//                        'name' => $data['bank_name'],
//                        'number' => $data['acc_num'],
//                        'imag_logo' => $filename,
//                        'status' => 1,
//                        'created_on' => $created_on
//                        );
//                }
//
//    	       $result = DB::table('bank_list')->insertGetId($data_temp);
//                if($result){
//                        $return_array['success'] = true; 
//                }
//            
//        }
//        return $return_array;
//    }
    
    
    public static function postEditbank($data){
        
        $return_array['success'] = false; 
        
        if(isset($data) && count($data) > 0){
            
            
                $created_on  = date('Y-m-d H:i:s');	
            
    	       $destinationPath = PRODUCT_IMAGE_PATH;
        
                if (Input::hasFile('bank_logo')) {

                    $filename = rand() . time() . str_replace(' ', '_', Input::file('bank_logo')->getClientOriginalName());

                    Image::make(Input::file('bank_logo'))->save($destinationPath . $filename);

                    $data_temp = array(
                        'name' => $data['bank_name'],
                        'number' => $data['acc_num'],
                        'imag_logo' => $filename,
                        'status' => 1,
                        'created_on' => $created_on
                        );
                }else{
                    
                       $data_temp = array(
                        'name' => $data['bank_name'],
                        'number' => $data['acc_num'],
                       // 'imag_logo' => $filename,
                        //'status' => 1,
                        //'created_on' => $created_on
                        );
                }

    	       $result = DB::table('bank_list')
                            ->where(array('id' => $data['bank_id']))
                            ->update($data_temp);
            
                if($result){
                    //data.message
                        $return_array['message'] = 'Bank Detail updated successfully'; 
                        $return_array['success'] = true; 
                }else{
                     $return_array['message'] = 'You did changed any filed, Please update the fields value';
                }
            
        }
        return $return_array;
    }

	public static function content() {
		$returndata = array();
		$Data = DB::table('content')
			->select('content_id','about_us','our_vision','privacy_policy')
			->first();
		
		if($Data)
			{
				$returndata['success'] = true; 
				$returndata['data'] = json_decode(json_encode($Data), true);
			}
		return $returndata;
	}

	public static function Updatetext($data) {
		$id = $data['content_id'];
		$data_temp = array(
			'about_us' => $data['about_us'],
			'our_vision' => $data['our_vision'],
            'privacy_policy' => $data['privacy_policy'],
			'created_on' => date('Y-m-d h:i:s')
		);
		$update_status = DB::table('content')->where('content_id', $id)->update($data_temp);
        return true;
	}
}