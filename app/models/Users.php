<?php

class Users extends Eloquent {

	// check image exist or not and return image 
	public static function ImageExist($image) {
        $filename = DIR_PROFILE_PIC_URL . $image;
        $physicalfilename = DIR_PROFILE_PIC.$image;
        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = DIR_PROFILE_PIC_URL . 'no_image_user.png';
        }
        return $filename;
    }
	
    public static function UserlistingCSV() {
        
        $users_obj = DB::table('front_users')
                ->select('id', 'name', 'email_address', DB::raw('CASE WHEN app_user_status = "1" THEN "Questioner" WHEN app_user_status = "2" THEN "Agent" ELSE "Both" END as roles'),
                         'phone_number', 'my_referral_code', 'last_login')
                ->where('user_delete', '0')
                ->get();
        $users_array = json_decode(json_encode($users_obj), true);
        return $users_array;
    }
	
	//###########################################################
    //Function : UserDetail
    //purpose : Fetch user detail
    //Author: Jitendra Parmar
    //###########################################################
	public static function UserDetail($Id) {
		$returndata = array();
        $returndata['success'] = false; 
        $Id = base64_decode($Id);
        
        if(isset($Id) && $Id !='') {
			$UserData = DB::table('front_users')
			->select('id','name','email_address','profile_pic','phone_number','questioner_ratings','agent_ratings','phone_number','my_referral_code',
				DB::raw('DATE_FORMAT(created_on,"%d %b %y") as joined'),
				DB::raw('DATE_FORMAT(last_login,"%d %b %y") as last_active'))
			->where('id',$Id)
			->first();
		   if($UserData)
			{
				$returndata['success'] = true; 
				$UserData->profile_pic = Self::ImageExist($UserData->profile_pic);
				$returndata['data'] = json_decode(json_encode($UserData), true);
			}
		}
		return $returndata;
	}
	
	public static function admindetails($admin_id){
		$returndata = array();
        $returndata['success'] = false; 
        if(isset($admin_id) && $admin_id !='') {
			$UserData = DB::table('manage_users')
			->select('id','name','email_address','profile_image')
			->where('id',$admin_id)
			->first();
		   if($UserData)
			{
				$returndata['success'] = true; 
				$UserData->profile_image = Self::ImageExist($UserData->profile_image);
				$returndata['data'] = json_decode(json_encode($UserData), true);
			}
		}
		return $returndata;	
	}
	
	public static function postUpdatepersonalinfo($data)
	{
		$created_on  = date('Y-m-d H:i:s');	
		$destinationPath = ADMINPROFILE_IMAGE_PATH;
		if (Input::hasFile('profile_image')) {
				$filename = rand() . time() . str_replace(' ', '_', Input::file('profile_image')->getClientOriginalName());
				$x = (int) $data['x1'];
				$y = (int) $data['y1'];
				$w = (int) $data['w'];
				$h = (int) $data['h'];
				Image::make(Input::file('profile_image'))->crop($w, $h, $x, $y)->resize(400, 400, function ($constraint) {
					$constraint->aspectRatio();
					$constraint->upsize();
				})->save($destinationPath . $filename);
			}
			else {
				$filename = '';
			}
			if($filename == '')
			{
				$data_temp = array(
					'name' => $data['name'],	
					'email_address' => $data['email_address'],
				);
			}
			else
			{
				$data_temp = array(
					'name' => $data['name'],	
					'email_address' => $data['email_address'],
					'profile_image' => $filename,
				);
			}
			$result = DB::table('manage_users')->where('id', $data['id'])->update($data_temp);
			return $result;
	}
	
	public static function postSetnewpassword($data ,$id) {
		$data_temp = array(
			'password' => Hash::make($data['new_password'])
		);
		$result = DB::table('manage_users')->where(array('id'=>$id))->update($data_temp);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
    }
    
    
    ## In order to insert new user.
    public static function anyAddnewuser($Params){
        
        $returndata = array();
        $returndata['success'] = false;
        $returndata['message'] = 'Oops! Something just went wrong. Try again.';
        
        if(isset($Params) && count($Params)> 0){
            
            $insertArray = array();
            
            $insertArray = array('name' => $Params['user_name'],
                                 'email_address' => $Params['email_address'],
                                 'password' => Hash::make($Params['password']),
                                 'status' => 1,
                                 'is_delete' => 0,
                                 'created_at' =>  date('Y-m-d H:i:s'));
                                 
            if(isset($Params['is_super_admin']) && $Params['is_super_admin'] !=''){
                
                $temp_array = array('admin_type' => 1,
                                    'is_completed_admin' => 1,
                                    'is_all_order_admin' => 1,
                                    'is_today_admin' => 1,
                                    'is_bank_admin' => 1,
                                    'is_users_admin' => 1,
                                    'is_inquiry_admin' => 1,
                                    'is_product_admin' => 1);    
                    
            }else {
                
                $temp_array = array('admin_type' => 2,
                                    'is_completed_admin' => isset($Params['is_completed_admin']) && $Params['is_completed_admin'] !='' ? 1 : 0 ,
                                    'is_all_order_admin' => isset($Params['is_all_order_admin']) && $Params['is_all_order_admin'] !='' ? 1 : 0 ,
                                    'is_today_admin' => isset($Params['is_today_admin']) && $Params['is_today_admin'] !='' ? 1 : 0 ,
                                    'is_bank_admin' => isset($Params['is_bank_admin']) && $Params['is_bank_admin'] !='' ? 1 : 0 ,
                                    'is_users_admin' => isset($Params['is_users_admin']) && $Params['is_users_admin'] !='' ? 1 : 0 ,
                                    'is_inquiry_admin' => isset($Params['is_inquiry_admin']) && $Params['is_inquiry_admin'] !='' ? 1 : 0 ,
                                    'is_product_admin' => isset($Params['is_product_admin']) && $Params['is_product_admin'] !='' ? 1 : 0);
                
            }
            
            $insertArrayMain  = array_merge($insertArray, $temp_array);
            $insertArray = DB::table('manage_users')->insert($insertArrayMain);
            
            if($insertArray){
                $returndata['success'] = true;
                $returndata['message'] = 'Inserted successfully'; 
            }
        }
        
        return $returndata;
    }
    
    public static function edituser($Params){
        
        $returndata = array();
        $returndata['success'] = false;
        $returndata['message'] = 'Oops! Something just went wrong. Try again.';
        
        if(isset($Params) && count($Params)> 0){
            
            $insertArray = array();
            
            $insertArray = array('name' => $Params['user_name'],
                                 'email_address' => $Params['email_address'],
                                // 'password' => Hash::make($Params['password']),
                                 //'status' => 1,
                                // 'is_delete' => 0,
                                // 'created_at' =>  date('Y-m-d H:i:s')
                                );
                                 
            if(isset($Params['is_super_admin']) && $Params['is_super_admin'] !=''){
                
                $temp_array = array('admin_type' => 1,
                                    'is_completed_admin' => 1,
                                    'is_all_order_admin' => 1,
                                    'is_today_admin' => 1,
                                    'is_bank_admin' => 1,
                                    'is_users_admin' => 1,
                                    'is_inquiry_admin' => 1,
                                    'is_product_admin' => 1);    
                    
            }else {
                
                $temp_array = array('admin_type' => 2,
                                    'is_completed_admin' => isset($Params['is_completed_admin']) && $Params['is_completed_admin'] !='' ? 1 : 0 ,
                                    'is_all_order_admin' => isset($Params['is_all_order_admin']) && $Params['is_all_order_admin'] !='' ? 1 : 0 ,
                                    'is_today_admin' => isset($Params['is_today_admin']) && $Params['is_today_admin'] !='' ? 1 : 0 ,
                                    'is_bank_admin' => isset($Params['is_bank_admin']) && $Params['is_bank_admin'] !='' ? 1 : 0 ,
                                    'is_users_admin' => isset($Params['is_users_admin']) && $Params['is_users_admin'] !='' ? 1 : 0 ,
                                    'is_inquiry_admin' => isset($Params['is_inquiry_admin']) && $Params['is_inquiry_admin'] !='' ? 1 : 0 ,
                                    'is_product_admin' => isset($Params['is_product_admin']) && $Params['is_product_admin'] !='' ? 1 : 0);
            }
            
            $insertArrayMain  = array_merge($insertArray, $temp_array);
            $insertArray = DB::table('manage_users')->where(array('id' => $Params['user_id']))->update($insertArrayMain);
            
            if($insertArray){
                $returndata['success'] = true;
                $returndata['message'] = 'Updated successfully'; 
            }else{
                $returndata['success'] = true;
                $returndata['message'] = 'No changes has been made'; 
            }
        }
        
        return $returndata;
    }
}

