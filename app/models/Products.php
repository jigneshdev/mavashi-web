<?php

class Products extends Eloquent {

	// check image exist or not and return image 
	public static function ImageExist($image) {
        $filename = PRODUCT_IMAGE_URL . $image;
        $physicalfilename = PRODUCT_IMAGE_PATH.$image;
        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = PRODUCT_IMAGE_URL . 'product_placeholder.png';
        }
        return $filename;
    }
	
    public static function ProductslistingCSV() {
        $users_obj = DB::table('products as p')
                ->select('product_id', 'product_name','price')
                ->get();
        $users_array = json_decode(json_encode($users_obj), true);
        return $users_array;
    }

    public static function Addproductinfo($data)
    {
    	$created_on  = date('Y-m-d H:i:s');	
    	$destinationPath = PRODUCT_IMAGE_PATH;
        
    	if (Input::hasFile('product_image')) {
            
            
    		$filename = rand() . time() .'.'.Input::file('product_image')->getClientOriginalExtension();
    		$x = (int) $data['x1'] == 0 ? 100 : (int) $data['x1'];
    		$y = (int) $data['y1']== 0 ? 100 : (int) $data['y1'];
    		$w = (int) $data['w'] == 0 ? 100 : (int) $data['w'];
    		$h = (int) $data['h'] == 0 ? 100 : (int) $data['h'];
           
    		Image::make(Input::file('product_image'))->crop($w, $h, $x, $y)->resize(400, 400, function ($constraint) {
    			$constraint->aspectRatio();
    			$constraint->upsize();
    		})->save($destinationPath . $filename);
            
    		$data_temp = array(
    			'product_name' => $data['product_name'],
                'sheep_type_id' => $data['sheep_type_id'],
    			'price' => $data['price'],
                 'status' => 1,
				'product_image' => $filename,
    			'created_on' => date('Y-m-d h:i:s')
    			);
    	}

    	$result = DB::table('products')->insertGetId($data_temp);
    	if($result)
    	{
			//Add lable
    		for($i=0; $i<count($data['lable']); $i++)
    		{
    			$label_data[] = array(
    				'lable'=>$data['lable'][$i],
    				'product_id'=>$result,
    				'type' => 0
    				);
    		}
    		$label_data = DB::table('product_lable')->insert($label_data);
			//Add lable checkbox
    		if(!empty($data['lablechk']))
            {
                for($i=0; $i<count($data['lablechk']); $i++)
                {
                    $label_datachk[] = array(
                        'lable'=>$data['lablechk'][$i],
                        'product_id'=>$result,
                        'type' => 1
                        );
                }
                $label_datachk = DB::table('product_lable')->insert($label_datachk);
            }

            //Add product sizes
            if(!empty($data['sheep_size']))
            {
                for($i=0; $i<count($data['sheep_size']); $i++)
                {
                    $sheep_size[] = array(
                        'product_size_id'=>$data['sheep_size'][$i],
                        'product_id'=>$result,
                        'status' =>1,
                        'price' => isset($data['sheep_size_price'][$i]) && $data['sheep_size_price'][$i] !='' ? $data['sheep_size_price'][$i] : 0,
                        'created_on' => date('Y-m-d h:i:s')
                        );
                }
                $sheep_size = DB::table('products_sizes')->insert($sheep_size);
            }

    		$product_lable_data = DB::table('product_lable')->where('product_id', $result)->where('type', 0)->get();
    		$lable = array();
    		foreach($product_lable_data as $product_lable_row)
    		{
    			array_push($lable, $product_lable_row->product_lable_id);
    		}

    		$product_lable_data_chk = DB::table('product_lable')->where('product_id', $result)->where('type', 1)->get();
    		$lablechk = array();
    		foreach($product_lable_data_chk as $product_lable_row)
    		{
    			array_push($lablechk, $product_lable_row->product_lable_id);
    		}

    		$search = array_map(function($value){
    			return $value='/\b'.$value."\b/";
    		},array_unique($data['option_value_index']));
    		$lable_id = preg_replace($search, $lable, $data['option_value_index']);

            if(!empty($data['option_value_index_chk']))
            {
        		$search_chk = array_map(function($value_chk){
        			return $value_chk = '/\b'.$value_chk."\b/";
        		},array_unique($data['option_value_index_chk']));
        		$lable_id_chk = preg_replace($search_chk, $lablechk, $data['option_value_index_chk']);
            }
    		for($j=0; $j<count($data['option_value_index']); $j++)
    		{
    			$price_option_data[] = array(
    				'lable_id'=>$lable_id[$j],
    				'lable_price'=>$data['lable_price'][$j],
    				'lable_option'=>$data['lable_option'][$j],
    				'type' => 0
    				);
    		}
    		$product_price_option = DB::table('product_price_option')->insert($price_option_data);

            if(!empty($data['option_value_index_chk']))
            {
                for($l=0; $l<count($data['option_value_index_chk']); $l++)
                {
                    $price_option_data_chk[] = array(
                        'lable_id'=>$lable_id_chk[$l],
                        'lable_price'=>$data['lable_price_chk'][$l],
                        'lable_option'=>$data['lable_option_chk'][$l],
                        'type' => 1
                        );
                }
                $product_price_option_chk = DB::table('product_price_option')->insert($price_option_data_chk);
            }
			//Add lable end 
    	}
    	return true;
    }

    public static function Sheeptypes()
    {
        $returndata = array();
        $returndata['success'] = false; 
            $SheepData = DB::table('sheep_types')
            ->select('sheep_type_id','sheep_name')
            ->where('status',1)
            ->get();
            
           if($SheepData)
            {
                $returndata['success'] = true; 
                $returndata['data'] = json_decode(json_encode($SheepData), true);
            }
        return $returndata;
    }

    public static function Sheepsizes()
    {
        $returndata = array();
        $returndata['success'] = false; 
            $SizeData = DB::table('sheep_size')
            ->select('size_id','size_name')
            ->where('status',1)
            ->get();
            
           if($SizeData)
            {
                $returndata['success'] = true; 
                $returndata['data'] = json_decode(json_encode($SizeData), true);
            }
        return $returndata;
    }

	public static function productdetail($id)
	{
		$returndata = array();
        $returndata['success'] = false; 
        $Id = base64_decode($id);
        
        if(isset($Id) && $Id !='') {
            
			$ProductData = DB::table('products as p')
			->select('product_id','product_image','product_name','price','sheep_type_id')
			->where('product_id',$Id)
			->first();
			
		   if($ProductData)
			{
                $product['product_id'] = $ProductData->product_id;
	            $product['product_image'] = Self::ImageExist($ProductData->product_image);
	            $product['product_name'] = $ProductData->product_name;
                $product['sheep_type_id'] = $ProductData->sheep_type_id;
                $product['sheep_size'] = Self::sheepsizedetail($ProductData->product_id);
	            $product['price'] = $ProductData->price;
	            $product['lable_data'] = Self::labledetail($ProductData->product_id);
                $product['checkbox_data'] = Self::checkboxdetail($ProductData->product_id);
				$returndata['success'] = true; 
				$returndata['data'] = json_decode(json_encode($product), true);
            
           }
		}
        
        return $returndata;
	}
    
    ## To select the product size.
    public static function sheepsizedetail($Id)
    {
            // To select the Product sizes
            $sheepsize = DB::table('products_sizes')
                ->select('product_size_id', 'price')
                ->where('product_id',$Id)
                ->get();
        
            $sheepsize = json_decode(json_encode($sheepsize), true);
            
            $sheepMainsize = DB::table('sheep_size')
                ->select('size_id', 'size_name')
                ->where('status', 1)
                ->get();
        
            $sheepMainsize = json_decode(json_encode($sheepMainsize), true);
        
            $tempArray = array();
        
            foreach($sheepMainsize as $key => $val){
              
                $tempArray[] = array('size_id' => $val['size_id'],
                                     'size_name' => $val['size_name'],
                                     'price' => isset($sheepsize[$key]['price']) && $val['size_id'] == $sheepsize[$key]['product_size_id'] ? $sheepsize[$key]['price'] : 0);
             }
        
        if($tempArray){
            return $tempArray;
        } else {
            return false;
        }
    }

	public static function labledetail($Id)
	{
		$LableData = DB::table('product_lable')
		->select('product_lable_id','lable','type')
		->where('product_id',$Id)
        ->where('type',0)
		->get();

		if($LableData)
		{
			foreach($LableData as $lablerow)
			{
				$lableproduct['product_lable_id'] = $lablerow->product_lable_id;
	            $lableproduct['lable'] = $lablerow->lable;
	            $lableproduct['type'] = $lablerow->type;
	            $lableproduct['option_data'] = Self::optiondetail($lablerow->product_lable_id);
	            $lbl[] = $lableproduct;
			}
			return $lbl;
		}
	}

    public static function checkboxdetail($Id)
    {
        $LableData = DB::table('product_lable')
        ->select('product_lable_id','lable','type')
        ->where('product_id',$Id)
        ->where('type',1)
        ->get();

        if($LableData)
        {
            foreach($LableData as $lablerow)
            {
                $lableproduct['product_lable_id'] = $lablerow->product_lable_id;
                $lableproduct['lable'] = $lablerow->lable;
                $lableproduct['type'] = $lablerow->type;
                $lableproduct['option_data'] = Self::optiondetail($lablerow->product_lable_id);
                $lbl[] = $lableproduct;
            }
            return $lbl;
        }
    }

	public static function optiondetail($Id)
	{
		$optionData = DB::table('product_price_option')
		->select('price_option_id','lable_option','lable_price')
		->where('lable_id',$Id)
		->get();

		if($optionData)
		{
			foreach($optionData as $optionrow)
			{
				$optionproduct['price_option_id'] = $optionrow->price_option_id;
				$optionproduct['lable_option'] = $optionrow->lable_option;
	            $optionproduct['lable_price'] = $optionrow->lable_price;
	            $opt[] = $optionproduct;
			}
			return $opt;
		}
	}

	public static function editproductinfo($data)
    {
    	
    	$created_on  = date('Y-m-d H:i:s');
    	$product_id = $data['product_id'];
    	$destinationPath = PRODUCT_IMAGE_PATH;
    	$data_temp = array();
        $sheep_size = isset($data['sheep_size']) ? implode(',', $data['sheep_size']) : '';
        
    	if (Input::hasFile('product_image')) {
    		$filename = rand() . time() .'.'.Input::file('product_image')->getClientOriginalExtension();
    		$x = (int) $data['x1'];
    		$y = (int) $data['y1'];
    		$w = (int) $data['w'];
    		$h = (int) $data['h'];
    		Image::make(Input::file('product_image'))->crop($w, $h, $x, $y)->resize(400, 400, function ($constraint) {
    			$constraint->aspectRatio();
    			$constraint->upsize();
    		})->save($destinationPath . $filename);
    		
            $data_temp = array(
                'product_name' => $data['product_name'],
                'sheep_type_id' => $data['sheep_type_id'],
                'price' => $data['price'],
                //'sheep_size' => $sheep_size,
                'product_image' => $filename,
                'created_on' => date('Y-m-d h:i:s')
            );
    	}
    	else
    	{
    		$data_temp = array(
                'product_name' => $data['product_name'],
                'sheep_type_id' => $data['sheep_type_id'],
                'price' => $data['price'],
                //'sheep_size' => $sheep_size,
                'created_on' => date('Y-m-d h:i:s')
            );
    	}
    	
    	$result = DB::table('products')->where('product_id', $product_id)->update($data_temp);
        
        if(isset($data['sheep_size']) && count($data['sheep_size']) > 0){
            
            $delete = DB::table('products_sizes')
                        ->where(array('product_id' => $product_id))
                        ->delete();
            
            $tempSize = array();
            $sheep_price =  $data['sheep_price'];
            
            foreach($data['sheep_size'] as $sk => $v){
                
                $tempSize[] = array('product_id' => $product_id, 
                                    'price' => $sheep_price[$sk],
                                    'status' => 1,
                                    'product_size_id' => $v,
                                    'created_on' => date('Y-m-d h:i:s'));
            }
            //print_r($tempSize);die;
            $result = DB::table('products_sizes')->insert($tempSize);
            
        }
        
    	//Delete all record first
        self::deletedata($product_id);
        
            //Add product sizes
            if(!empty($data['sheep_size']))
            {
                for($i=0; $i<count($data['sheep_size']); $i++)
                {
                    $sheep_sizedata[] = array(
                        'product_size_id'=>$data['sheep_size'][$i],
                        'product_id'=>$product_id,
                        'updated_on' => date('Y-m-d h:i:s')
                        );
                }
                $sheep_size = DB::table('products_sizes')->insert($sheep_sizedata);
            }

            $product_lable_data = DB::table('product_lable')->where('product_id', $product_id)->where('type', 0)->get();
            $lable = array();
            foreach($product_lable_data as $product_lable_row)
            {
                array_push($lable, $product_lable_row->product_lable_id);
            }

            $product_lable_data_chk = DB::table('product_lable')->where('product_id', $product_id)->where('type', 1)->get();
            $lablechk = array();
            
            foreach($product_lable_data_chk as $product_lable_row)
            {
                array_push($lablechk, $product_lable_row->product_lable_id);
            }
            
            
            ## To insert values in the database.
            ## Type 0 dropdown.
              if(isset($data['lable'])){
                  
                    for($i=0; $i<count($data['lable']); $i++)
                    {
                        $main_insert_array = array(
                            'lable'=> $data['lable'][$i],
                            'product_id'=> $product_id,
                            'type' => 0
                            );
                        ##insert the label and get inserted id.
                        $main_insert = DB::table('product_lable')->insertGetId($main_insert_array);

                        $mainPrice = $data['lable_'.$i.'_price'];
                        $mainOption = $data['lable_'.$i.'_option'];

                        ## insert the label data.
                        foreach($mainPrice as $in2 => $val2){
                              $main_insertdata[] = array(
                                        'lable_id'=> $main_insert,
                                        'lable_price'=> $mainPrice[$in2],
                                        'lable_option'=> $mainOption[$in2],
                                        'type' => 0
                                 );
                         }
                        
                        $insertArray = DB::table('product_price_option')->insert($main_insertdata);
                        $main_insertdata = array();
                    }
               }
            
            ## To add edited values in the database.
            ## Type 0 dropdown.
              if(isset($data['lable_2'])){
                  
                    for($i=0; $i<count($data['lable_2']); $i++)
                    {
                        $label_data2 = array(
                            'lable'=> $data['lable_2'][$i],
                            'product_id'=> $product_id,
                            'type' => 0
                            );
                        ##insert the label and get inserted id.
                        $result_lable_data = DB::table('product_lable')->insertGetId($label_data2);

                        $tempPrice = $data['lable_'.$i.'_price2'];
                        $tempOption = $data['lable_'.$i.'_option2'];

                        ## insert the label data.
                        foreach($tempPrice as $in2 => $val2){
                              $price_option_data2[] = array(
                                        'lable_id'=> $result_lable_data,
                                        'lable_price'=> $tempPrice[$in2],
                                        'lable_option'=> $tempOption[$in2],
                                        'type' => 0
                                 );
                         }
                       
                        $insert = DB::table('product_price_option')->insert($price_option_data2);
                         $price_option_data2 = array();
                    }
               }
            
            ## To insert values for type 1
            ## Type 1 dropdown.
           //print_r($data);die;
            if(isset($data['lablechk']))
            {   
                    for($i=0; $i<count($data['lablechk']); $i++)
                    {
                     
                        $tempCheckPrice4 = isset($data['lable_'.$i.'_price_chk']) ? $data['lable_'.$i.'_price_chk'] : '';
                        $tempCheckOption4 = isset($data['lable_'.$i.'_option_chk']) ? $data['lable_'.$i.'_option_chk'] : '';
                        
                        if(isset($tempCheckPrice4) && $tempCheckPrice4 !=''){
                            
                          $label_datacheck = array(
                            'lable'=> $data['lablechk'][$i],
                            'product_id'=> $product_id,
                            'type' => 1
                            );
                          ##insert the label and get inserted id.
                          $result4 = DB::table('product_lable')->insertGetId($label_datacheck);
                            
                        }
                        
                        ## insert the label data.
                        if($tempCheckPrice4 !=''){
                            
                            foreach($tempCheckPrice4 as $cin2 => $cval2){
                              $data4[] = array(
                                        'lable_id'=> $result4,
                                        'lable_price'=> $tempCheckPrice4[$cin2],
                                        'lable_option'=> $tempCheckOption4[$cin2],
                                        'type' => 1
                                     );
                             }
                          
                            $insertCheckarray = DB::table('product_price_option')->insert($data4);
                            $data4 = array();
                            $label_datacheck  = array();
                            $tempCheckPrice4 = array();
                            $tempCheckOption4 = array();
                            $result4 = array();
                        }
                    }
            }
            
            ## To add edited values for type 1
            ## Type 1 dropdown.
           
            if(isset($data['lable_chk2']))
            { 
                    for($i=0; $i<count($data['lable_chk2']); $i++)
                    {
                        $label_datachech = array(
                            'lable'=> $data['lable_chk2'][$i],
                            'product_id'=> $product_id,
                            'type' => 1
                            );
                        
                        ##insert the label and get inserted id.
                        $result_lable_check_data = DB::table('product_lable')->insertGetId($label_datachech);

                        $tempCheckPrice5 = $data['lable_'.$i.'_price_chk2'];
                        $tempCheckOption5 = $data['lable_'.$i.'_option_chk2'];
                        
                        ## insert the label data.
                        foreach($tempCheckPrice5 as $cin2 => $cval2){
                              $price_option_check_data2[] = array(
                                        'lable_id'=> $result_lable_check_data,
                                        'lable_price'=> $cval2,
                                        'lable_option'=> $tempCheckOption5[$cin2],
                                        'type' => 1
                                 );
                         }
                       // print_r($price_option_check_data2);
                        $insertCheck = DB::table('product_price_option')->insert($price_option_check_data2);
                        $insertCheck = array();
                        $tempCheckPrice5 = array();
                        $tempCheckOption5 = array();
                        $label_datachech = array();
                        $price_option_check_data2 = array();
                    
                    }
            }
        
            //Add lable end 
    	return true;
    }

    public static function deletedata($product_id)
    {
        if(!empty($product_id))
        {
            $Data = DB::table('product_lable')
            ->select('product_lable_id')
            ->where('product_id',$product_id)
            ->get();
            $ids = array();
            foreach($Data as $row)
            {
                array_push($ids, $row->product_lable_id);
            }
            if(!empty($ids))
            {
                DB::table('product_price_option')->where('lable_id', $ids)->delete();
                DB::table('products_sizes')->where('product_id', $product_id)->delete();
            }
            
            DB::table('product_lable')->where('product_id', $product_id)->delete();
        }
    }

}

