<?php

class Apiproducts extends Eloquent {

    ## to get product listing.
    public static function productlist(){
        
        $getproducts = DB::table('products as pp')
                    ->select('pp.product_name', 'pp.product_id','st.sheep_name as type',
                             DB::raw('CONCAT("'.PRODUCT_IMAGE_URL.'", pp.product_image) as product_image'),
                             'pp.price')
                        ->leftjoin('sheep_types as st', 'st.sheep_type_id', '=', 'pp.sheep_type_id')
                        ->where('pp.status', 1)
                        ->orderby('pp.price', 'asc')
                        ->get();
        
        $getproducts = json_decode(json_encode($getproducts), true);
        
        if($getproducts)
        {
            foreach($getproducts as $keys => $val){
                
                 $product_id = $val['product_id'];
                 $getproducts1 = DB::table('products_sizes as ps')
                                ->select('sz.size_id','sz.size_name', 'ps.price')
                                ->leftjoin('sheep_size as sz', 'sz.size_id', '=', 'ps.product_size_id')
                                ->where('ps.product_id', $product_id)
                               // ->where('ps.status', 1)
                                ->get();
                
                 $getproducts1 = json_decode(json_encode($getproducts1), true);
                 $getproducts[$keys]['size'] = $getproducts1;
                
            }
            
            return $getproducts;
        } else{
            return false;   
        }
        
    }
    
    public static function getBanklist(){
        
         $getproducts = DB::table('bank_list as pp')
                    ->select('pp.name', 'pp.id','pp.number',
                             DB::raw('CONCAT("'.PRODUCT_IMAGE_URL.'", pp.imag_logo) as bank_logo'))
                        ->orderby('pp.id', 'asc')
                        ->get();
        
         $getproducts = json_decode(json_encode($getproducts), true);
        
           if($getproducts)
           {
                 return $getproducts;
           }else{
               
               return false;
           }
    }
    
    public static function postProductinfo($params){
        
       $product_id = $params['product_id'];
        
       $getproducts = DB::table('product_lable AS pl')
                    ->select('pl.lable', 'pl.type','pl.product_lable_id')
                    ->where('pl.product_id', $product_id)
                        //->leftjoin('sheep_types as st', 'st.sheep_type_id', '=', 'pp.sheep_type_id')
                    ->get();
        
        $getproductsinfo = json_decode(json_encode($getproducts), true);
        
       //print_r($getproductsinfo);die;
        
        if($getproductsinfo)
        {
            foreach($getproductsinfo as $keys => $val){
                
                 $product_lable_id = $val['product_lable_id'];
                
                 $getproducts1 = DB::table('product_price_option as ppo')
                                ->select('ppo.lable_option', 'lable_price')
                                //->leftjoin('sheep_size as sz', 'sz.size_id', '=', 'ps.product_size_id')
                                ->where('ppo.lable_id', $product_lable_id)
                                ->get();
                
                 $getproducts1 = json_decode(json_encode($getproducts1), true);
                 
                 $getproductsinfo[$keys]['sub'] = $getproducts1;
                
            }
            $returnarray['success'] = true;
            $returnarray['data'] = $getproductsinfo;
            return $returnarray;
        } else{
            return false;   
        }
    }
    
    ## order information
    // @ table main_orders, order_bank_details, order_options.
    
    
public static function postOrderinfo($params){
        
        $insert = json_encode($params);
        $mainy = DB::table('order_temp')->insertGetId(array('fields' =>$insert));
        $created_at = date('Y-m-d');
        
        ## To select the new fields.
        $product_id = $params['product_id'];
        $address = $params['address'];
        $sheep_type = $params['sheep_type'];
        $product_size = $params['product_size'];
        $quantity = $params['quantity'];
        $grand_total = $params['grand_total'];
        $bank_detail = isset($params['bank_detail']) ? $params['bank_detail'] : '';
        $json_string = $params['options'];
        
        $main_table_array = array('address' => $address,
                                   'location' => $params['location'],
                                   'name' => $params['name'],
                                   'email' => $params['email'],
                                   'number' => $params['number'],
                                   'city' => $params['city'],
                                   'bank_id' => $bank_detail,
                                   'grand_total'=> $grand_total,
                                   'status'=> 1,
                                   'payment_type' => $params['payment_type'],
                                   'is_paid' => 0,
                                   'is_complete' => 0,
                                   'created_at' => $created_at);
        
        $main_array = DB::table('main_orders')->insertGetId($main_table_array);
        
        if($main_array){
            
            ## To insert in table option.
            $products_array = explode(",", $product_id);
            $sheep_type_array = explode(",", $sheep_type);
            $sheep_size_array = explode(",", $product_size);
            $sheep_quantity = explode(",", $quantity);
            $option_array = explode('#', $json_string);
            
            foreach($products_array as $ppk => $ppv){
                
                $prepare_array[] = array('order_id' => $main_array,
                                         'product_id'=> $ppv,
                                         'sheep_type' => $sheep_type_array[$ppk],
                                         'sheep_size' => $sheep_size_array[$ppk],
                                         'quantity'=> $sheep_quantity[$ppk],
                                         'option' => $option_array[$ppk],
                                         'created_on' => $created_at);
            }
            
            if($prepare_array){
                
                 $sub_array_options = DB::table('main_orders_options')->insert($prepare_array);
                    $returnarray['success'] = true;
                    $returnarray['data'] = $main_array;
            }else{
                   $returnarray['success'] = false;
                   $returnarray['data'] = array();
            }
        } else {
                    $returnarray['success'] = false;
                    $returnarray['data'] = array();
        }
        
        return $returnarray;
    
    }
    
    public static function postInqueryinfo($params){
        
        $data_tmp = array(
            'contact_num' => $params['contact_num'],
            'email' => $params['email'],
            'name' => $params['name'],
            'message' => $params['message']
            );

        DB::table('contact_us')->insert($data_tmp);
        return true;
    }

}