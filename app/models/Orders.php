<?php

class Orders extends Eloquent {

	// check image exist or not and return image 
	public static function ImageExist($image) {
        $filename = PRODUCT_IMAGE_URL . $image;
        $physicalfilename = PRODUCT_IMAGE_PATH.$image;
        if (file_exists($physicalfilename) && $image != '') {
            $filename = $filename;
        } else {
            $filename = PRODUCT_IMAGE_URL . 'product_placeholder.png';
        }
        return $filename;
    }

    // check image exist or not and return image 
    public static function UserImageExist($image) {
          $filename = DIR_PROFILE_PIC_URL . $image;
          $physicalfilename = DIR_PROFILE_PIC.$image;
          if (file_exists($physicalfilename) && $image != '') {
              $filename = $filename;
          } else {
              $filename = DIR_PROFILE_PIC_URL . 'no_image_user.png';
          }
          return $filename;
      }
	
    public static function OrderslistingCSV() {
        
        $users_obj = DB::table('main_orders as o')
                ->select('order_id', 'product_name','address','sheep_type','product_size','quantity','grand_total')
                ->leftJoin('products as p', 'o.product_id', '=', 'p.product_id')
                ->get();
        $users_array = json_decode(json_encode($users_obj), true);
        return $users_array;
    }

    public static function CompleteOrderslistingCSV() {
        
        $users_obj = DB::table('main_orders as o')
                ->select('order_id', 'product_name','address','sheep_type','product_size','quantity','grand_total')
                ->leftJoin('products as p', 'o.product_id', '=', 'p.product_id')
                ->where('is_complete',1)
                ->get();
        $users_array = json_decode(json_encode($users_obj), true);
        return $users_array;
    }
	
	public static function OrderDetail($Id)
	{   
		$returndata = array();
        $returndata['success'] = false; 
        $Id = base64_decode($Id);
        
        if(isset($Id) && $Id !='') {
            
			$OrderData = DB::table('main_orders as o')
			                 ->select('o.order_id', 'o.email', 'o.name', 'o.number', 'o.city',  'o.address', 'o.location', 'o.app_price', 'o.cost',
                             'o.grand_total', 'o.payment_type', 'o.is_paid', 'o.is_complete')
			                 ->where('o.order_id',$Id)
			                 ->first();
			
            $OrderData = json_decode(json_encode($OrderData), true);
            
		    if($OrderData)
			{
               // in oder to calculate the net profit.
               if($OrderData['app_price'] != '' && $OrderData['cost'] !=''){
                     $OrderData['profit'] = $OrderData['app_price'] - $OrderData['cost'];     
               }else{
                    $OrderData['profit'] = 0;
               }
               
               $getOptions = DB::table('main_orders_options as moo')
                            ->select('moo.id as oreder_option_id', 'moo.product_id', 'moo.quantity', 'moo.sheep_type', 'moo.sheep_size',
                                     'pp.product_name', DB::raw('concat("'.PRODUCT_IMAGE_URL.'", pp.product_image) as  product_image'),'moo.option')
                            ->leftjoin('products as pp', 'pp.product_id', '=', 'moo.product_id')
                            ->where('order_id', $OrderData['order_id'])
                            ->get();
               
               $getOptions = json_decode(json_encode($getOptions), true);
               $arrays = [];
               
               foreach($getOptions as $kk => $vv){
                  
                   $ss = explode( "},", $vv['option']);
                   
                    foreach($ss as $val){

                       $one = explode(",", $val);

                       if(isset($one[0]) && isset($one[1]) && isset($one[2])){
                           $lable = explode(":", $one[0]);
                           $option = explode(":", $one[1]);
                           $price = explode(":", $one[2]);
                           $price1 = str_replace( array( ']', '"',',' , '}' ), ' ', $price[1]);
                           if(isset($lable[1])){

                            $arrays[] = array('lable' => substr(trim($lable[1]), 1, -1), 
                                             'options' => substr(trim($option[1]), 1, -1),
                                             'price' => trim($price1));
                            
                           }
                       }
                    }
                   
                   unset($getOptions[$kk]['option']);
                   $getOptions[$kk]['option'] = $arrays;
                   $arrays = array();
               
               }
               
                $resultData['order_info'] = $OrderData;
                $resultData['order_list'] = $getOptions;
               
				$returndata['success'] = true;
				$returndata['data'] = json_decode(json_encode($resultData), true);
			
            }
		}
        
        return $returndata;
	}

	public static function orderinfo($Id)
	{
		$Data = DB::table('order_detail')
		->select('order_lable_id','order_option_id')
		->where('order_id',$Id)
		->get();

		if($Data)
		{
			foreach($Data as $row)
			{
				$orderinfo['lable_data'] = Self::lable_detail($row->order_lable_id,$row->order_option_id);
	            $odr[] = $orderinfo;
			}
			return $odr;
		}
	}

	public static function lable_detail($lable_id,$option_id)
	{
		$LableData = DB::table('product_lable')
		->select('lable')
		->where('product_lable_id',$lable_id)
		->first();

		if($LableData)
		{
			$lableproduct['lable'] = $LableData->lable;
            $lableproduct['option_data'] = Self::optiondetail($option_id);
            $lbl[] = $lableproduct;
		
			return $lableproduct;
		}
	}

	public static function optiondetail($option_id)
	{
		$optionData = DB::table('product_price_option')
		->select('lable_option','lable_price')
		->where('price_option_id',$option_id)
		->first();

		if($optionData)
		{
			$optionproduct['lable_option'] = $optionData->lable_option;
	        $optionproduct['lable_price'] = $optionData->lable_price;
	           
			return $optionproduct;
		}
	}
	
	public static function completeorder($order_id)
	{
		$data_temp = array(
			'status' => 1, 'is_paid' =>  1, 'is_complete' => 1
        );
        $update_status = DB::table('main_orders')
            ->where('order_id', $order_id)
            ->update($data_temp);
        return true;
	}

	public static function cancelorder($order_id)
	{
        $update_status = DB::table('main_orders')
            ->where('order_id', $order_id)
            ->delete();
        return true;
	}
    
    ## TO UPDATE THE ORDER.
	public static function updateorder($data)
	{
        $order_id = $data['order_id'];
            
        foreach($data['oreder_option_id'] as $ppk => $ppv)
        {
             $arrayOption = array('quantity' => $data['quantity'][$ppk],
                                 'sheep_size' => $data['product_size'][$ppk]);
            
             for($i=0; $i<count($data['lable_'.$ppk.'']); $i++)
             {
                        $lable_data[] = array(
                            'lable' => $data['lable_'.$ppk.''][$i],
                            'option'=> $data['option_'.$ppk.''][$i],
                            'price'=>$data['price_'.$ppk.''][$i]
                        );
             }
            
            $order_options = json_encode($lable_data,JSON_UNESCAPED_UNICODE);
            $dataOptionUpdate = array_merge($arrayOption, array('option' => $order_options));
            
            $update_status = DB::table('main_orders_options')
                            ->where('id', $ppv)
                            ->update($dataOptionUpdate);
            $arrayOption = $lable_data = $dataOptionUpdate =  array();
            
        }
        
       
//        
//        foreach($data['total_lables'] as $key => $val)
//        {
//            for($i=0; $i<count($data['lable_'.$key.'']); $i++)
//            {
//                        $lable_data[] = array(
//                            'lable' => $data['lable_'.$key.''][$i],
//                            'option'=> $data['option_'.$key.''][$i],
//                            'price'=>$data['price_'.$key.''][$i]
//                        );
//            }
//        }
//        
//        $order_options = json_encode($lable_data,JSON_UNESCAPED_UNICODE);
       
        $data_temp = array(
			         'email' => $data['email_address'],
			         'name' => $data['name'],
			         'number' => $data['number'],
			         'address' => $data['address'],
               'cost' => isset($data['cost']) ? $data['cost'] : '', 
               'app_price' => isset($data['app_price']) ? $data['app_price'] : '',
               'city' => $data['city'],
              'payment_type' => $data['payment_type'],
		);

		  $update_status = DB::table('main_orders')
            ->where('order_id', $order_id)
            ->update($data_temp);
       return true;
	}


  // To get all comments.
  public static function getAllcomments($Params){

      $returndata = array();
      $order_id =  $Id = base64_decode($Params['id']);
     
      $get_comments = DB::table('orders_comments as oc')
                        ->select('oc.*', 'mu.name', 'mu.profile_image')
                        ->leftjoin('manage_users as mu', 'mu.id','=', 'oc.user_id')
                        ->where(array('order_id' => $order_id))
                        ->get();

      $get_comments = json_decode(json_encode($get_comments), true);
            
      if($get_comments){
         
          foreach ($get_comments as $key => $value) {
              $get_comments[$key]['profile_image']  = self::UserImageExist($value['profile_image']);
              $get_comments[$key]['main_user'] = Auth::manage_user()->id();
          }
          
        $returndata['data'] = $get_comments;
        $returndata['success'] = true; 
      } else{
        $returndata['success'] = false;
      }

      return $returndata;
  }

  public static function getInsertcomments($Params){

      $returndata = array();
      $user_id = Auth::manage_user()->id();
      
      if(isset($Params['order_id']) && $Params['order_id'] != ''){

          ## insert into the database.
          $insertArray = array('order_id' => $Params['order_id'],
                               'comment' => $Params['comment_box'],
                               'user_id' => $user_id,
                                'created_on' => date('Y-m-d H:i:s'));

          $insert = DB::table('orders_comments')->insertGetId($insertArray);

          if($insert){


              $get_comments = DB::table('orders_comments as oc')
                        ->select('oc.*', 'mu.name', 'mu.profile_image')
                        ->leftjoin('manage_users as mu', 'mu.id','=', 'oc.user_id')
                        ->where(array('oc.id' => $insert))
                        ->first();

              $get_comments = json_decode(json_encode($get_comments), true);
              $get_comments['profile_image']  = self::UserImageExist($get_comments['profile_image']);
              $get_comments['main_user'] = Auth::manage_user()->id();

              $returndata['data'] = $get_comments;
              $returndata['success'] = true;

          }
      }else{
              $returndata['success'] = false;
      }

      return $returndata;
  }

}

