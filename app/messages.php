<?php

//################## General message ##############################/
define('INVALID_PARAMS', 'Invalid parameters.');
define('INVALID_IMAGE', 'Image is not valid.');
define('WELCOME_MESSAGE', 'Welcome to Mawashi!!');
define('GENERAL_ERROR', 'Oops! Something just went wrong. Please try again later.');
define('GENERAL_SUCCESS', 'Done!');
define('GENERAL_NO_CHANGES', 'You have made no changes.');
define('GENERAL_NO_DATA', 'No data available.');
//################## Admin panel messages start ##############################/
define('LOGIN_ERROR', 'Invalid email address or password.');
define('LOGIN_SUCCESS', 'Authentication completed successfully.');
define('EMAIL_NOT_EXIST', 'Email is not matched with account.');
define('FORGOT_PASSWORD_SUCCESS', 'An email has been sent to you. Please check your inbox.');
define('INVALID_OLD_PASSWORD', 'Your old password is incorrect.');
define('SUCCESS_PASSWORD_CHANGE', 'Password updated successfully. Redirecting to Login...');
define('ADMIN_TOKEN_EXPIRED', 'Link has been expired.');
define('SUCCESS_PROFILE_UPDATE', 'Profile has been updated.');
define('OLD_PASSWORD_NOT_OK', 'Old Password is not correct.');
define('PASSWORD_CHANGED', 'Password has been changed.');
define('SAME_PASSWORD', 'You cannot set the current password as the new password.');


//####################### Admin profile section ##########################################
define('SUCCESS_ADMIN_PROFILE_UPDATE', 'Profile updated successfully.');
define('ERROR_ADMIN_PROFILE_UPDATE', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_ADMIN_CHANGE_PASSWORD', 'Your have changed your password successfully');
define('ERROR_ADMIN_CHANGE_PASSWORD', 'Oops! Something just went wrong. Please try again later.');
define('ERROR_INVALID_CURRENT_PASSWORD', 'Invalid current password!'); 
define('SUCCESS_CHANGE_LANGUAGE', 'Language successfully'); 
//####################### Mange section ##########################################
define('SUCCESS_PRODUCT_ADD','Product added successfully ');
define('ERROR_PRODUCT_ADD', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_PRODUCT_EDIT','Product updated successfully ');
define('ERROR_PRODUCT_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_COMPLETE_ORDER','Order completed successfully ');
define('ERROR_COMPLETE_ORDER', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_CANCEL_ORDER','Order canceled successfully ');
define('ERROR_CANCEL_ORDER', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_UPDATE_ORDER','Order updated successfully ');
define('ERROR_UPDATE_ORDER', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_ADD_SHEEP_SIZE','Sheep size added successfully');
define('ERROR_ADD_SHEEP_SIZE', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_ADD_SHEEP_TYPE','Sheep type added successfully');
define('ERROR_ADD_SHEEP_TYPE', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_UPDATE_CONTENT','Content updated successfully');
define('ERROR_UPDATE_CONTENT', 'Oops! Something just went wrong. Please try again later.');

define('SUCCESS_CATEGORY_EDIT','Category updated successfully ');
define('SUCCESS_PAYMENT_EDIT', 'Updated successfully.');
define('ERROR_PAYMENT_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_COMMISSION_EDIT', 'Commission updated successfully.');
define('ERROR_COMMISSION_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_TERMSCONDITION_EDIT', 'Terms & condition updated successfully.');
define('ERROR_TERMSCONDITION_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_PROMOCODE_ADD','Promo code added successfully ');
define('ERROR_PROMOCODE_ADD', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_REFERRAL_AMOUNT_EDIT','Referral amount updated successfully ');
define('ERROR_REFERRAL_AMOUNT_EDIT', 'Oops! Something just went wrong. Please try again later.');
define('SUCCESS_ADDITIONAL_CATEGORY_ADD','Additional category saved successfully ');
define('ERROR_ADDITIONAL_CATEGORY_ADD', 'Oops! Something just went wrong. Please try again later.');
