<!DOCTYPE html>
<html lang="en">
    <head>
        <title ng-bind="title"> UNIDeal | Change Password </title>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}  
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::style('assets/lib/select2/css/select2.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/select2/css/select2-bootstrap.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        {{ HTML::style('assets/css/components.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/plugins.css', array('rel' => 'stylesheet')) }}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        {{ HTML::style('assets/css/login3.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/bootstrap-toastr/toastr.min.css',array('rel'=>'stylesheet')) }}
        <!-- END PAGE LEVEL STYLES -->

        <!--{{ HTML::style('assets/admin/css/custom.css',array('rel'=>'stylesheet')) }}-->  

        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.png"/>
    </head>
    <body class="login">
        <div class="logo">
            <img src="<?php echo ASSETS_IMAGE_FRONT . 'logo.png' ?>" alt="UNIDealUNIDeal" class="" style="width: 250px;"/>
        </div>
        <p>
        <h4 class="h4-verify"  style="color:#FFFFFF;text-align: center">Your password has been changed! You can <a href="<?php echo url() . '/manage'; ?>" style="color: #68C743;font-weight: 600">Login</a> with your new password now.</h4>
        </p>        
        <script>var BASEURL = '<?php echo url() . '/manage/'; ?>'</script>
        {{ HTML::script('assets/lib/jquery.min.js') }}
        {{ HTML::script('assets/lib/bootstrap/js/bootstrap.min.js') }}
        {{ HTML::script('assets/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
        {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}
        {{ HTML::script('assets/lib/jquery.cokie.min.js') }}
        {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}        

        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        {{ HTML::script('assets/lib/jquery-validation/js/jquery.validate.min.js') }}
        {{ HTML::script('assets/lib/jquery-validation/js/additional-methods.min.js') }}
        {{ HTML::script('assets/lib/select2/js/select2.full.min.js') }}
        {{ HTML::script('assets/lib/bootstrap-toastr/toastr.min.js') }}
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        {{ HTML::script('assets/js/app.js') }} 
        <script>
            $(document).ready(function()
            {
                //Metronic.init();

            });
        </script>
    </body>
</html>
