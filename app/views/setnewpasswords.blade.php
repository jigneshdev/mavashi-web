<!DOCTYPE html>
<html lang="en">
<head>
<title ng-bind="title"> UNIDeal | Set New Password </title>

<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

{{ HTML::style('assets/admin/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}
{{ HTML::style('assets/admin/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}
{{ HTML::style('assets/admin/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}
{{ HTML::style('assets/admin/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}

{{ HTML::style('assets/admin/css/login3.css',array('rel'=>'stylesheet')) }}


{{ HTML::style('assets/admin/css/components.css',array('rel'=>'stylesheet','id'=>'style_components')) }}
{{ HTML::style('assets/admin/css/plugins.css',array('rel'=>'stylesheet')) }}
{{ HTML::style('assets/admin/css/layout.css',array('rel'=>'stylesheet')) }}
{{ HTML::style('assets/admin/css/themes/grey.css',array('rel'=>'stylesheet')) }}
{{ HTML::style('assets/admin/css/custom.css',array('rel'=>'stylesheet')) }}
<style>
	.error{
		border-color: red;
	}
	label.error{
		color:red;
	}
</style>
<link rel="shortcut icon" href="<?=url('/')."/"?>favicon.ico"/>
</head>
<body class="login page-header-fixed page-sidebar-fixed page-container-bg-solid page-sidebar-closed-hide-logo " >
			<div class="copyright">
                 <h2 style="color:#FFFFFF">PortDrop</h2>
				{{HTML::image('http://166.62.84.81/~portdropxyz/assets/admin/images/logo-100.png','alt',array('class'=>''))}}

				<div class="">
					<div class="col-md-4 col-sm-6 col-xs-12 text-center" >
						<form class="form-signin form-group" action="<?php echo URL::to('/').'/login/resetpassword'?>" method="POST" id="resetpasswordform">
							<input type="hidden" name="user" value="{{$email}}">
							<h2 class="form-signin-heading" style="color:#FFFFFF">Set your new password</h2>
							<div class="form-group">
								<input type="password" name="password" id="newPassword" class="form-control" placeholder="New password" autofocus>
							</div>
							<div class="form-group">
								<input type="password" name="password_confirmation" id="confirmPassword" class="form-control" placeholder="ReType Password">
							</div>
							<div class="checkbox"></div>
							<input type="submit" class="btn btn-lg btn-primary btn-block" value="Reset">
						</form>
					</div>
				</div>
            </div>
            
            <div class="whiter text-center">
                <h1>&nbsp;</h1>
            </div>
            <div class="menu-toggler sidebar-toggler">
            </div>
			
            
    {{ HTML::script('assets/admin/lib/jquery.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery-migrate.min.js') }}
    {{ HTML::script('assets/admin/lib/bootstrap/js/bootstrap.min.js') }}
    {{ HTML::script('assets/admin/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery-slimscroll/jquery.slimscroll.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery.blockui.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery.cokie.min.js') }}
    {{ HTML::script('assets/admin/lib/uniform/jquery.uniform.min.js') }}
	{{ HTML::script('assets/admin/lib/jquery-validation/js/jquery.validate.min.js') }}
    {{ HTML::script('assets/admin/js/metronic.js') }}

<script>
$(document).ready(function()
{
	Metronic.init();
	$("#resetpasswordform").validate({
			rules: {
				
				password: {
					required: true,
					minlength: 6
				},
				password_confirmation: {
					required: true,
					minlength: 6,
					equalTo: "#newPassword"
				}
			},
			messages: {
				password: {
					required: "Please enter a password",
					minlength: "Your password must be at least 6 characters long"
				},
				password_confirmation: {
					required: "Please enter a password",
					minlength: "Your password must be at least 6 characters long",
					equalTo: "Please enter the same password as above"
				}
			}
		});
	
});
</script>
</body>
</html>
