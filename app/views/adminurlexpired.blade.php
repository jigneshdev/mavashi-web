<!DOCTYPE html>
<html lang="en">
<head>
<title ng-bind="title"> UNIDeal | URL Expired </title>

<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

{{ HTML::style('assets/admin/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}  
{{ HTML::style('assets/admin/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}  
{{ HTML::style('assets/admin/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}  
{{ HTML::style('assets/admin/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}  

{{ HTML::style('assets/admin/css/login3.css',array('rel'=>'stylesheet')) }}   


{{ HTML::style('assets/admin/css/components.css',array('rel'=>'stylesheet','id'=>'style_components')) }}  
{{ HTML::style('assets/admin/css/plugins.css',array('rel'=>'stylesheet')) }}
{{ HTML::style('assets/admin/css/layout.css',array('rel'=>'stylesheet')) }}  
{{ HTML::style('assets/admin/css/themes/grey.css',array('rel'=>'stylesheet')) }}  
{{ HTML::style('assets/admin/css/custom.css',array('rel'=>'stylesheet')) }}  

<link rel="shortcut icon" href="<?=url('/')."/"?>favicon.ico"/>
</head>
<body class="login page-header-fixed page-sidebar-fixed page-container-bg-solid page-sidebar-closed-hide-logo " >
			<div class="copyright">
                 <h2 style="color:#FFFFFF">PortDrop</h2>
				{{HTML::image('http://166.62.84.81/~portdropxyz/assets/admin/images/logo-100.png','alt',array('class'=>''))}}
				<div class="droidios">
					<a class="androidios" href="" style="color:#FFFFFF"><i class="fa fa-3x fa-android"></i></a>
					<a class="androidios" href="" style="color:#FFFFFF"><i class="fa fa-3x fa-apple"></i></a>
				</div>
				<div class="">
					<div class="col-md-4 col-sm-6 col-xs-12 text-center" ><h4 class="h4-verify"  style="COLOR:#FFFFFF;">Sorry!  Your {{$url_for}} link has been expired.</h4></div>
				</div>
            </div>
            
            <div class="whiter text-center">
                <h1>&nbsp;</h1>
            </div>
            <div class="menu-toggler sidebar-toggler">
            </div>
			
            
    {{ HTML::script('assets/admin/lib/jquery.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery-migrate.min.js') }}
    {{ HTML::script('assets/admin/lib/bootstrap/js/bootstrap.min.js') }}
    {{ HTML::script('assets/admin/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery-slimscroll/jquery.slimscroll.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery.blockui.min.js') }}
    {{ HTML::script('assets/admin/lib/jquery.cokie.min.js') }}
    {{ HTML::script('assets/admin/lib/uniform/jquery.uniform.min.js') }}
	{{ HTML::script('assets/admin/lib/jquery-validation/js/jquery.validate.min.js') }}
    {{ HTML::script('assets/admin/js/metronic.js') }}

<script>
$(document).ready(function()
{
	Metronic.init();
	
});
</script>
</body>
</html>
