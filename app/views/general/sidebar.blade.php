<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed text-capitalize" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px" ng-controller="MyCtrl">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler"> </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                <form class="sidebar-search" action="page_general_search_3.html" method="POST" style="display: none;">
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit">
                                <i class="icon-magnifier"></i>
                            </a>
                        </span>
                    </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
			
            <?php if(Session::has('admin_access')){ $value = Session::get('admin_access'); }else{ $value = ''; } ?>
          
            <?php if(isset($value['admin_type']) && $value['admin_type'] == 1) { ?>
            
                    <li ng-class="{active:isActive('/manage/dashboard')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'dashboard')}}" class="nav-link nav-toggle">
                            <i class="fa fa-dashboard"></i>
                            <span class="title" translate="dashboard"></span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/orders')}" ng-click="hidesub()" >
                        <a href="{{ URL::to(MANAGE_TEXT.'orders')}}" class="nav-link nav-toggle">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="title" translate="orders"></span>
                            <span class="{'selected': isActive('/manage/orders')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/neworders')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'neworders')}}" class="nav-link nav-toggle">
                            <i class="fa fa-shopping-basket"></i>
                            <span class="title" translate="new_orders"></span>
                            <span class="{'selected': isActive('/manage/neworders')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/allorderslist')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'allorderslist')}}" class="nav-link nav-toggle">
                            <i class="fa fa-cart-arrow-down"></i>
                            <span class="title" translate="allorders"></span>
                            <span class="{'selected': isActive('/manage/allorderslist')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/products')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'products')}}" class="nav-link nav-toggle">
                            <i class="fa fa-archive "></i>
                            <span class="title" translate="products"></span>
                            <span class="{'selected': isActive('/manage/products')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/banklisting')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'banklisting')}}" class="nav-link nav-toggle">
                            <i class="fa fa-university"></i>
                            <span class="title" translate="banklist"></span>
                            <span class="{'selected': isActive('/manage/banklisting')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/inquery')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'inquery')}}" class="nav-link nav-toggle">
                            <i class="fa fa-envelope-o"></i>
                            <span class="title" translate="inquery"></span>
                            <span class="{'selected': isActive('/manage/inquery')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/usermanage')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'usermanage')}}" class="nav-link nav-toggle">
                            <i class="fa fa-users"></i>
                            <span class="title" translate="usermanage"></span>
                            <span class="{'selected': isActive('/manage/usermanage')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/configsetting')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'configsetting')}}" class="nav-link nav-toggle">
                            <i class="fa fa-cogs"></i>
                            <span class="title" translate="setting"></span>
                            <span class="{'selected': isActive('/manage/configsetting')}"></span>
                        </a>
                    </li>
            <?php }else{ ?>
            
                    <li ng-class="{active:isActive('/manage/dashboard')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'dashboard')}}" class="nav-link nav-toggle">
                            <i class="fa fa-dashboard"></i>
                            <span class="title" translate="dashboard"></span>
                            <span class="selected"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/orders')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'orders')}}" class="nav-link nav-toggle <?=isset($value['is_completed_admin']) && $value['is_completed_admin'] == 1 ? '': 'hide'; ?>">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="title" translate="orders"></span>
                            <span class="{'selected': isActive('/manage/orders')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/neworders')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'neworders')}}" class="nav-link nav-toggle">
                            <i class="fa fa-shopping-basket"></i>
                            <span class="title" translate="new_orders"></span>
                            <span class="{'selected': isActive('/manage/neworders')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/allorderslist')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'allorderslist')}}" class="nav-link nav-toggle">
                            <i class="fa fa-cart-arrow-down"></i>
                            <span class="title" translate="allorders"></span>
                            <span class="{'selected': isActive('/manage/allorderslist')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/products')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'products')}}" class="nav-link nav-toggle">
                            <i class="fa fa-archive "></i>
                            <span class="title" translate="products"></span>
                            <span class="{'selected': isActive('/manage/products')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/banklisting')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'banklisting')}}" class="nav-link nav-toggle">
                            <i class="fa fa-university"></i>
                            <span class="title" translate="banklist"></span>
                            <span class="{'selected': isActive('/manage/banklisting')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/inquery')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'inquery')}}" class="nav-link nav-toggle">
                            <i class="fa fa-envelope-o"></i>
                            <span class="title" translate="inquery"></span>
                            <span class="{'selected': isActive('/manage/inquery')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/usermanage')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'usermanage')}}" class="nav-link nav-toggle">
                            <i class="fa fa-users"></i>
                            <span class="title" translate="usermanage"></span>
                            <span class="{'selected': isActive('/manage/usermanage')}"></span>
                        </a>
                    </li>
                    <li ng-class="{'active': isActive('/manage/configsetting')}" ng-click="hidesub()">
                        <a href="{{ URL::to(MANAGE_TEXT.'configsetting')}}" class="nav-link nav-toggle">
                            <i class="fa fa-cogs"></i>
                            <span class="title" translate="setting"></span>
                            <span class="{'selected': isActive('/manage/configsetting')}"></span>
                        </a>
                    </li>
            
            <?php } ?>
            
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->