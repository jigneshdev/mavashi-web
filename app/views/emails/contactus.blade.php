<html>
	<body>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
		<table width="614" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:12px;color:#656565;background: #FDFDFD;border: 1px solid #D6D5D5;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;-webkit-box-shadow: 0px -1px 5px #DDD;-moz-box-shadow: 0px -1px 3px #DDD;box-shadow: 0px -1px 5px #DDD;width: 168px;">
			<tbody>
				<tr>
					<td style="border-radius: 8px 8px 0 0; position: relative; text-align:center;background: #252525; padding: 10px">
						<a href="<?php echo '#'; ?>" target="_blank">
							<img src="<?=ASSETS_IMAGE_FRONT.'monkey.png'?>" alt="UNIDeal" width="100" border="0" style="padding:0px;max-width: 200px">
						</a>
					</td>
				</tr>
				<tr>
					<td style="padding:10px; background: #eee;">
						<table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
							<tbody>
								<tr>
									<td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-weight:600;font-size:15px;padding:16px 0 10px 10px;">Dear Administrator,</td>
								</tr>
							</tbody>
						</table>

						<table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
							<tbody>
								<tr>
									<td style="padding:0 10px 20px 10px;">
										<table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td>
														<div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
															You have new inquiry for UNIDeal App.
														</div>
													</td>
												</tr>
                                                                                                <br/>
                                                                                                <tr>
													<td>
														<div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                                                                    <span style="font-weight: bold">Name :</span> {{ $name }}.
														</div>
													</td>
												</tr><br/>
                                                                                                <tr>
													<td>
														<div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                                                                    <span style="font-weight: bold">Email :</span> {{ $email }}.
														</div>
													</td>
												</tr><br/>
                                                                                                <tr>
													<td>
														<div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                                                                                    <span style="font-weight: bold">Message :</span> {{ $contact_message }}.
														</div>
													</td>
												</tr>
											</tbody>
										</table>        
									</td>
								</tr>
							</tbody>
						</table>					


						<table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c">
							<tbody>
								<tr>
									<td style="padding:0 10px 20px 10px;">
										<table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td>
														<div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
															<?php //echo $html; ?>
														</div>
													</td>
												</tr>
											</tbody>
										</table>        
									</td>
								</tr>
								<tr>
									<td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top:1px solid #6c6c6c;">
										Sincerely,<br /> The UNIDeal Team.
									</td>
								</tr>
							</tbody>
						</table>
						

					</td>
				</tr>
				<tr>
					<td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:12px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #252525; border-top: 3px solid #6c6c6c; color: #fff;">
						<?php //echo $this->config->item('front_footer'); ?>&copy; UNIDeal					
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>