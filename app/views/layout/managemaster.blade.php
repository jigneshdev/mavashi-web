<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" ng-app="UniDealApp" id="UniDealApp">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <!-- <meta charset="utf-8" /> -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title ng-bind="title">Mawashi :: Dashboard</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="cache-control" content="private, max-age=0, no-cache">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="expires" content="0">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->        
        {{ HTML::script('assets/lib/jquery.min.js') }}        
        <!-- BEGIN THEME GLOBAL SCRIPTS -->                
        <!-- END THEME GLOBAL SCRIPTS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        {{ HTML::style('assets/lib/font-awesome/css/font-awesome.min.css',array('rel'=>'stylesheet')) }}    
        {{ HTML::style('assets/lib/simple-line-icons/simple-line-icons.min.css',array('rel'=>'stylesheet')) }}          
        {{ HTML::style('assets/lib/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/uniform/css/uniform.default.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/bootstrap-toastr/toastr.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/icomoon/icon-moon.css',array('rel'=>'stylesheet')) }}
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- ADDITIONAL STYLES -->
        {{ HTML::style('assets/lib/datatables/datatables.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('assets/lib/datatables/plugins/bootstrap/datatables.bootstrap.css',array('rel'=>'stylesheet')) }}
        
		<!-- END ADDITIONAL STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        {{ HTML::style('assets/css/components.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/plugins.min.css', array('rel' => 'stylesheet')) }}
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        {{ HTML::style('assets/global/plugin/bootstrap-daterangepicker/daterangepicker.min.css', array('rel' => 'stylesheet')) }} 

{{ HTML::style('assets/global/plugin/bootstrap-datepicker/css/bootstrap-datepicker3.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/layout.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/themes/darkblue.min.css', array('rel' => 'stylesheet')) }}
        {{ HTML::style('assets/css/cropper.min.css', array('rel' => 'stylesheet')) }}
		{{ HTML::style('assets/css/custom.css', array('rel' => 'stylesheet')) }}
		
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?= url('/') . "/" ?>favicon.png"/>
        <script>
            var base_url = "<?= url('/') . '/' ?>";
        </script>
        <base href="<?= url('/') . '/' ?>">
    </head>
    <!-- END HEAD -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white"> 
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{ URL::to(MANAGE_TEXT.'dashboard')}}">
                    <h3 style="color:white" translate="mawashi"></h3>
                        <!-- <img src="<?= ASSETS_IMAGE ?>logo.png" alt="logo" class="logo-default  visible-md visible-lg" />
                        <img src="<?= ASSETS_IMAGE ?>logo-big.png" alt="logo" class="logo-default visible-xs visible-sm" />--> </a>
                    <div class="menu-toggler sidebar-toggler" style="display: none;"> </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">                       
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="<?= ASSETS_IMAGE . 'avatar3.jpg' ?>" />
                                <span class="username username-hide-on-mobile">  </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="{{ URL::to(MANAGE_TEXT.'settings')}}" translate="admin_settings">
                                        <i class="fa fa-cogs"></i> 
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0)" onclick="window.location = '{{URL::to(MANAGE_TEXT.'login/logout')}}'" translate="log_out">
                                        <i class="fa fa-key"></i>  </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            @include('general.sidebar')            
            <!-- END CONTENT BODY -->            
            <div class="page-content-wrapper">
                <div class="page-spinner-bar">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" ng-view></div>    
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT BODY -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                <div class="page-footer-inner"> Mawashi &copy; copyright 2016
                </div>
                <div class="scroll-to-top">
                    <i class="fa fa-arrow-up"></i>
                </div>
            </div>
        </div>
        <!-- END FOOTER -->
    </body>
    <!--[if lt IE 9]>
           <script src="assets/global/plugins/respond.min.js"></script>
           <script src="assets/global/plugins/excanvas.min.js"></script> 
           <![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    {{ HTML::script('assets/lib/bootstrap/js/bootstrap.min.js') }}
    {{ HTML::script('assets/lib/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
    
    {{ HTML::script('assets/lib/uniform/jquery.uniform.min.js') }}
    
    {{ HTML::script('assets/lib/jquery.cokie.min.js') }}
    {{ HTML::script('assets/lib/bootstrap-toastr/toastr.min.js') }} 
    
    <!-- END CORE PLUGINS -->

    <!-- ANGULAR JS PLUGIN START -->
   
    {{ HTML::script('assets/lib/angular.min.js') }}    
    {{ HTML::script('assets/lib/angular-route.js') }}    

    {{ HTML::script('assets/lib/angular-sanitize.min.js') }}
    {{ HTML::script("assets/js/ui-bootstrap-tpls-1.3.2.js") }}
    {{ HTML::script('assets/js/angular-translate.min.js') }}
    {{ HTML::script('assets/js/customangular.js') }}
    
    <!-- ANGULAR JS PLUGIN END -->

    <!-- BEGIN USEFUL MODULE -->
    {{ HTML::script('assets/lib/datatables/datatables.min.js') }}
    {{ HTML::script('assets/lib/datatables/plugins/bootstrap/datatables.bootstrap.js') }}
    {{ HTML::script('assets/lib/jquery-validation/js/jquery.validate.min.js') }}
    {{ HTML::script('assets/lib/jquery-validation/js/additional-methods.min.js') }}
    {{ HTML::script('assets/js/moment.min.js') }}
     {{ HTML::script('assets/global/plugin/bootstrap-daterangepicker/daterangepicker.js') }}
     {{ HTML::script('assets/global/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
 
<!--    <script src=''></script>-->
    {{ HTML::script('assets/js/cropper.min.js') }}
    	
    <!-- END USEFUL MODULE -->

    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    {{ HTML::script('assets/js/app.min.js') }}
    {{ HTML::script('assets/js/layout.min.js') }}
    
   
	<!-- END THEME LAYOUT SCRIPTS -->

    <!-- BEGIN CUSTOM JS FILE FOR MODULE -->
    
    <!-- END CUSTOM JS FILE FOR MODULE -->
</html>
