// JavaScript Document
app = angular.module('UniDealApp', ['ngRoute', 'ui.bootstrap','pascalprecht.translate']);

app.constant('MYMESSAGE', {
   ERROR_NUMBER_INPUT_VALUE: "Value should be between 0 to 99"
   });

/*Check if user logout it redirect the login page.*/
function checklogin(param) {    
    if (param.hasOwnProperty('logout')) {
        window.location = 'manage/login';
    } else {
        return true;
    }
}

/*#########################
 * menu controller
 *#########################*/

app.controller('MyCtrl', function($scope, $location) {

    $scope.isActive = function(route) {

        return route === $location.path();
    }
    $scope.showsub = false;
    $scope.arrowopen = "arrow";
    $scope.submenu = function() {
        $scope.showsub = !$scope.showsub;
        $scope.mainmenu = "nav-item open";
        if (!$scope.showsub) {
            $scope.arrowopen = "arrow";
        }
        else {
            $scope.arrowopen = "arrow open";
        }
    }
    $scope.activemainmenu = function() {
        $scope.mainmenu = "active";
    }
    $scope.hidesub = function() {
        $scope.mainmenu = "";
    }
});

/*#########################
 * Routes controller
 *#########################*/

app.config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }]);
//for changing title
app.run(['$location', '$rootScope', function($location, $rootScope) {
        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
			//$rootScope.title = current.$route.title;
            document.body.scrollTop = document.documentElement.scrollTop = 0;

        });
    }]);
//end
app.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.
                when('/manage/dashboard', {
                    templateUrl: base_url + 'app/views/manage/dashboard.html',
                    controller: 'HomeController',
                    title: 'Mavashi :: Dashboard'
                }).
                when('/manage/settings', {
                    templateUrl: base_url + 'app/views/manage/settings.html',
                    controller: 'AdminProfileController',
                    title: 'Mavashi :: Admin Profile '
                }).
                when('/manage/orders', {
                    templateUrl: base_url + 'app/views/manage/orders.html',
                    controller: 'OrdersController',
                    title: 'Mavashi :: Orders'
                }).
                when('/manage/neworders', {
                    templateUrl: base_url + 'app/views/manage/neworders.html',
                    controller: 'OrdersController',
                    title: 'Mavashi :: New Orders'
                }).
                when('/manage/products', {
                    templateUrl: base_url + 'app/views/manage/product.html',
                    controller: 'ProductController',
                    title: 'Mavashi:: Manage Products'
                }).
                when('/manage/addnewproduct', {
                    templateUrl: base_url + 'app/views/manage/addproduct.html',
                    controller: 'AddproductController',
                    title: 'Mavashi:: Add New Products'
                }).
                when('/manage/order_detail/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_order.html',
                    controller: 'OrdersController',
                    title: 'Mavashi:: Order detail'
                }).
                when('/manage/edit_order/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_order.html',
                    controller: 'OrdersController',
                    title: 'Mavashi:: Order detail'
                }).
                when('/manage/product_detail/:id', {
                    templateUrl: base_url + 'app/views/manage/product_detail.html',
                    controller: 'ProductController',
                    title: 'Mavashi:: Product detail'
                }).
                when('/manage/edit_product/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_product.html',
                    controller: 'EditproductController',
                    title: 'Mavashi:: Edit Product'
                }).
                when('/manage/configsetting', {
                    templateUrl: base_url + 'app/views/manage/configsetting.html',
                    controller: 'SettingsController',
                    title: 'Mavashi:: Settings'
                }).
                when('/manage/banklisting', {
                    templateUrl: base_url + 'app/views/manage/banklist.html',
                    controller: 'BankController',
                    title: 'Mavashi:: Bank listing'
                }).
                when('/manage/edit_bank/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_bank.html',
                    controller: 'BankController',
                    title: 'Mavashi:: Edit Bank'
                }).
                otherwise({
                    redirectTo: '/manage/dashboard'
                });
        $locationProvider.html5Mode(true);
    }]);

//for loader
/*CONFIG*/
app.run(function($rootScope, $location, $route, $timeout) {

    $rootScope.layout = {};
    $rootScope.layout.loading = false;

    $rootScope.$on('$routeChangeStart', function() {

        $timeout(function() {
            $rootScope.layout.loading = true;
        });
    });
    $rootScope.$on('$routeChangeSuccess', function() {
        $timeout(function() {
            $rootScope.layout.loading = false;
        }, 200);
    });
    $rootScope.$on('$routeChangeError', function() {
        $rootScope.layout.loading = false;

    });
});
app.config(function($httpProvider) {
    $httpProvider.responseInterceptors.push('myHttpInterceptor');

    var spinnerFunction = function spinnerFunction(data, headersGetter) {
        $(".page-spinner-bar").show();
        return data;
    };

    $httpProvider.defaults.transformRequest.push(spinnerFunction);
});

app.config(function ($translateProvider) {
  $translateProvider.translations('en', {
    mawashi:'Mawashi',
    user:'User',
    admin:'Admin',
    dashboard:'Dashboard',
    orders:'Complete Orders',
    new_orders:'Today Orders',
    products:'Products',
    my_details: 'MY DETAILS',
    name:'NAME : ',
    email:'EMAIL : ',
    save:'SAVE',
    change_password:'CHANGE PASSWORD',
    old_password:'OLD PASSWORD',
    new_password:'NEW PASSWORD',
    change_language:'CHANGE LANGUAGE',
    language:'Language : ',
    admin_settings:'Admin Settings',
    log_out:'Log Out',
    total_products:'Total Products',
    total_orders:'Total Orders',
    //new_orders:'New Orders',
    total_revenue:'Total Sales',
    recent_activities:'RECENT ACTIVITIES',
    coming_soon:'Coming soon..',
    order_id:'Order Id',
    product_name:'Product Name',
    sheep_type:'Sheep type',
    sheep_size:'Sheep Size',
    gross_total:'Gross Total',
    delivery_address:'Delivery Address',
    action:'Action',
    reset:'Reset',
    add_new_product:'Add new product',
    id:'Id',
    product_pic:'Product pic',
    price:'Price',
    uploaded_on:'Uploaded on',
    product_photo:'Product photo',
    option:'Option',
    add:'Add',
    add_more_options:'Add more options',
    clear:'Clear',
    remove:'Remove',
    add_more_checkbox:'Add more checkbox',
    add_product:'Add product',
    edit_product:'Edit Products',
    update_product:'Update Products',
    order_details:'Order details',
    amount:'Amount',
    paid:'Paid',
    unpaid:'Unpaid',
    no:'No',
    lable:'Lable',
    product_size:'Product size',
    product_image:'Product image',
    quantity:'Quantity',
    payment_status:'Payment status',
    sheep_size_detail:'Sheep size detail',
    sheep_type_detail:'Sheep type detail',
    general_setting:'General setting',
    pending_orders:'Pending orders',
    orders_by_week:'Orders by week',
    orders_by_month:'Orders by month',
    orders_by_year:'Orders by year',
    update_order:'Update order',
      banklist: 'Banks'
  });
  $translateProvider.translations('ar', {
    mawashi:'مواشي',
    user:'المستعمل',
    admin:'مشرف',
    dashboard:'لوحة القيادة',
    orders:'أوامر كاملة',
    new_orders:'أوامر اليوم',
    products:'منتجات',
    my_details: 'تفاصيلي',
    name:' : اسم',
    email:' : البريد الإلكتروني',
    save:'حفظ',
    change_password:'تغيير كلمة السر',
    old_password:'كلمة المرور القديمة',
    new_password:'كلمة السر الجديدة',
    change_language:'غير اللغة',
    language:'اللغة',
    admin_settings:'إعدادات المشرف',
    log_out:'الخروج',
    total_products:'إجمالي المنتجات',
    total_orders:'إجمالي الطلبات',
    //new_orders:'طلبات جديدة',
    total_revenue:'إجمالي الإيرادات',
    recent_activities:'أنشطة حالية',
    coming_soon:'قريبا..',
    order_id:'رقم التعريف الخاص بالطلب',
    product_name:'اسم المنتج',
    sheep_type:'نوع الأغنام',
    sheep_size:'حجم الأغنام',
    gross_total:'المجموع الكلي',
    delivery_address:'عنوان التسليم',
    action:'عمل',
    reset:'إعادة تعيين',
    add_new_product:'إضافة منتج جديد',
    id:'هوية شخصية',
    product_pic:'صورة المنتج',
    price:'السعر',
    uploaded_on:'تم التحميل على',
    product_photo:'صورة المنتج',
    option:'اختيار',
    add:'إضافة',
    add_more_options:'إضافة المزيد من الخيارات',
    clear:'واضح',
    remove:'إزالة',
    add_more_checkbox:'أضف المزيد من مربع الاختيار',
    add_product:'إضافة منتج',
    edit_product:'تحرير المنتجات',
    update_product:'تحديث المنتجات',
    order_details:'تفاصيل الطلب',
    amount:'كمية',
    paid:'دفع',
    unpaid:'غير مدفوع',
    no:'لا',
    lable:'علامة مميزة',
    product_size:'حجم المنتج',
    product_image:'صورة المنتج',
    quantity:'كمية',
    payment_status:'حالة السداد',
    sheep_size_detail:'الأغنام حجم التفاصيل',
    sheep_type_detail:'نوع الأغنام التفاصيل',
    general_setting:'الإعداد العام',
    pending_orders:'في انتظار أوامر',
    orders_by_week:'أوامر حسب الأسبوع',
    orders_by_month:'أوامر حسب الشهر',
    orders_by_year:'أوامر حسب السنة',
    update_order:'أجل التحديث',
      banklist: 'البنوك'
  });
  $translateProvider.preferredLanguage('en');
});

app.factory('myHttpInterceptor', function($q, $window) {
    return function(promise) {
        return promise.then(function(response) {
            $(".page-spinner-bar").hide();
            checklogin(response.data);
            return response;
        }, function(response) {
            $(".page-spinner-bar").hide();
            return $q.reject(response);
        });
    };
});

// Home controllers.
app.controller('HomeController', ['$http', '$scope', function($http, $scope) {

    $http.get('dashboard/lateststats', {cache: true}).
       success(function (data, status, headers, config) {
                if(data.success){
                    $scope.allstats = data.data;
                }
       }).
       error(function (data, status, headers, config) {

       });

    }]);

// For admin profile.
app.controller('AdminProfileController', ['$http', '$scope','$routeParams','$translate', function($http, $scope,$routeParams,$translate) {
        $scope.changeLanguage = function (key) {
            $translate.use(key);
        };

        $http.get('adminprofile/adminprofile', {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.admindetails = data.details;
            }
        }).
        error(function (data, status, headers, config) {

        });
}]);

app.controller('OrdersController', ['$http', '$scope','$routeParams', function($http, $scope,$routeParams) {
        $http.get('orders/orderdetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.orderdetails = data.details;
                 $scope.order_options  = $scope.orderdetails.order_options;
                    console.log($scope.order_options);
                   // $scope.tt = [{"lable":"نوع التغليف ","option":"المصارين","price":"50"},{"lable":"الراس ","option":"غير مشلوخ ","price":"50"},{"lable":"إزالة الشحم	","option":"إزالة الشحم","price":"50"}];
                    //console.log($scope.tt); 
                    
//                    var values =  $scope.tt;
//
//                            angular.forEach(values,function(value,key){
//                                angular.forEach(value,function(v1,k1){//this is nested angular.forEach loop
//                                    console.log(k1+":"+v1);
//                                });
//                            });
           }
        }).
        error(function (data, status, headers, config) {

        });
}]);
app.controller('ProductController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){
    $http.get('products/productetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.productetails = data.details;
            }
        }).
        error(function (data, status, headers, config) {

        });
}]);
//AddproductController

app.controller('AddproductController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){
    
    $http.get('products/sheeptypes', {cache: true}).
    success(function (data, status, headers, config) {
    if(data.success == true) {
             $scope.sheepdetails = data.details;
        }
    }).
    error(function (data, status, headers, config) {

    });

    $http.get('products/sheepsizes', {cache: true}).
    success(function (data, status, headers, config) {
    if(data.success == true) {
             $scope.sheepsizedetails = data.details;
        }
    }).
    error(function (data, status, headers, config) {

    });

    $scope.add_new_product = [];
    
    // Add new options
    $scope.addNewOptions = function(){
        $scope.add_new_product.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDiv = function(index){
        $scope.add_new_product.splice(index, 1);
        console.log($scope.add_new_product);
         $('#remove_div_' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv = function(index){
        //$(document).find('#adddivprice_'+ index).html(' ');
      //  $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" placeholder="Option price">');
        //$('#adddivoptions_'+ index).append('<br><input type="text" class="form-control" name="lable_optiion[]" placeholder="Enter values for drop down">  <a href=');
    }
    
    $scope.addpriceandoptions = function(index){
        $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" id placeholder="Option price">');
        $('#adddivoptions_'+index).append('<br><input type="text" class="form-control" name="lable_option[]" placeholder="Enter values for drop down"><input type="hidden" value="'+index+'" name="option_value_index[]">');
    }
    
    //Add more checkbox
    $scope.add_new_product_chk = [];
    
    // Add new options
    $scope.addNewOptionschk = function(){
        $scope.add_new_product_chk.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDivchk = function(index){
        $scope.add_new_product_chk.splice(index, 1);
        console.log($scope.add_new_product_chk);
         $('#remove_div_chk' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv_chk = function(index){
        //$(document).find('#adddivprice_'+ index).html(' ');
      //  $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" placeholder="Option price">');
        //$('#adddivoptions_'+ index).append('<br><input type="text" class="form-control" name="lable_optiion[]" placeholder="Enter values for drop down">  <a href=');
    }
    
    $scope.addpriceandoptions_chk = function(index){
        $('#adddivprice_chk' + index).append('<br><input type="text" class="form-control" name="lable_price_chk[]" id placeholder="Option price">');
        $('#adddivoptions_chk'+index).append('<br><input type="text" class="form-control" name="lable_option_chk[]" placeholder="Enter values for drop down"><input type="hidden" value="'+index+'" name="option_value_index_chk[]">');
    }
}]);

//Edit product
app.controller('EditproductController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){
    
    $http.get('products/productetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.productetails = data.details;
                 console.log($scope.productetails);
                 console.log($scope.productetails.sheep_size);
            }
        }).
        error(function (data, status, headers, config) {

        });

        $scope.isChecked = function(product) {
          var items = $scope.productetails.sheep_size;
          for (var i=0; i < items.length; i++) {
            if (product == items[i])
              return true;
          }
          return false;
        };
        
    $http.get('products/sheeptypes', {cache: true}).
    success(function (data, status, headers, config) {
    if(data.success == true) {
             $scope.sheepdetails = data.details;
        }
    }).
    error(function (data, status, headers, config) {

    });

    $http.get('products/sheepsizes', {cache: true}).
    success(function (data, status, headers, config) {
    if(data.success == true) {
             $scope.sheepsizedetails = data.details;
        }
    }).
    error(function (data, status, headers, config) {

    });
    $scope.add_new_product = [];
    
    // Add new options
    $scope.addNewOptions = function(){
        $scope.add_new_product.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDiv = function(index){
        $scope.add_new_product.splice(index, 1);
        console.log($scope.add_new_product);
         $('#remove_div_' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv = function(index){
        //$(document).find('#adddivprice_'+ index).html(' ');
      //  $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" placeholder="Option price">');
        //$('#adddivoptions_'+ index).append('<br><input type="text" class="form-control" name="lable_optiion[]" placeholder="Enter values for drop down">  <a href=');
    }
    
    $scope.addpriceandoptions = function(index){
        $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" id placeholder="Option price">');
        $('#adddivoptions_'+index).append('<br><input type="text" class="form-control" name="lable_option[]" placeholder="Enter values for drop down"><input type="hidden" value="'+index+'" name="option_value_index[]">');
    }
    
    //Add more checkbox
    $scope.add_new_product_chk = [];
    
    // Add new options
    $scope.addNewOptionschk = function(){
        $scope.add_new_product_chk.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDivchk = function(index){
        $scope.add_new_product_chk.splice(index, 1);
        console.log($scope.add_new_product_chk);
         $('#remove_div_chk' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv_chk = function(index){
        //$(document).find('#adddivprice_'+ index).html(' ');
      //  $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" placeholder="Option price">');
        //$('#adddivoptions_'+ index).append('<br><input type="text" class="form-control" name="lable_optiion[]" placeholder="Enter values for drop down">  <a href=');
    }
    
    $scope.addpriceandoptions_chk = function(index){
        $('#adddivprice_chk' + index).append('<br><input type="text" class="form-control" name="lable_price_chk[]" id placeholder="Option price">');
        $('#adddivoptions_chk'+index).append('<br><input type="text" class="form-control" name="lable_option_chk[]" placeholder="Enter values for drop down"><input type="hidden" value="'+index+'" name="option_value_index_chk[]">');
    }
}]);

app.controller('SettingsController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){

}]);
app.controller('BankController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){

        $http.get('bank/bankdetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.bankdetails = data.data;
            }
        }).
        error(function (data, status, headers, config) {

        });
    
}]);