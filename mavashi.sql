/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : mavashi

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-05-10 08:18:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for contact_us
-- ----------------------------
DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE `contact_us` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `contact_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contact_us
-- ----------------------------
INSERT INTO `contact_us` VALUES ('1', 'Bhargav', 'test@gmail.com', 'asfsdafasfsdafasd', '2016-07-06 08:20:04');
INSERT INTO `contact_us` VALUES ('2', 'Bhargav', 'test@gmail.com', 'asdfsfdasfasfasd', '2016-07-06 08:20:15');
INSERT INTO `contact_us` VALUES ('3', 'Bhargav', 'test@gmail.com', 'asdfasdfasdfasdf', '2016-07-06 08:20:35');
INSERT INTO `contact_us` VALUES ('4', 'Bhargav', 'test@gmail.com', 'sadfsadfsafasd', '2016-07-06 08:25:46');
INSERT INTO `contact_us` VALUES ('5', 'Bhargav', 'test@gmail.com', 'asfasdfadsfasfasd', '2016-07-06 08:27:48');
INSERT INTO `contact_us` VALUES ('6', 'Bhargav', 'test@gmail.com', 'asdfasdfsafasdfasdf', '2016-07-06 08:28:30');
INSERT INTO `contact_us` VALUES ('7', 'Bhargav', 'test@gmail.com', 'ssdfgdfgfsdgsdfgsd', '2016-07-06 08:30:09');
INSERT INTO `contact_us` VALUES ('8', 'bhargav', 'test@gmail.com', 'asdfasdfasdfasd', '2016-07-06 08:31:38');
INSERT INTO `contact_us` VALUES ('9', 'Bhargav', 'test@gmail.com', 'sadfasdfasfsafasd', '2016-07-06 08:32:16');
INSERT INTO `contact_us` VALUES ('10', 'Bhargav', 'test@gmail.com', 'sdfgdfgsdfgfdsgsd', '2016-07-06 08:32:40');
INSERT INTO `contact_us` VALUES ('11', 'Bhargav', 'test@gmail.com', 'test message', '2016-07-07 00:17:02');

-- ----------------------------
-- Table structure for front_users
-- ----------------------------
DROP TABLE IF EXISTS `front_users`;
CREATE TABLE `front_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `gender` tinyint(1) NOT NULL COMMENT '1 => Male 2=> Female',
  `phone_number` varchar(20) NOT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `questioner_ratings` varchar(10) DEFAULT NULL,
  `agent_ratings` varchar(10) DEFAULT NULL,
  `os_type` tinyint(4) DEFAULT NULL COMMENT '1: ios 2: android',
  `user_type` tinyint(4) DEFAULT NULL COMMENT '1: Questioner, 2: Agent, 3: both',
  `from_social_network` tinyint(4) DEFAULT '0' COMMENT '1:from facebook, 2: from google , 3: normal',
  `googleplus_id` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `total_earnings` float(10,2) NOT NULL,
  `referral_code` varchar(20) NOT NULL,
  `default_card` varchar(40) NOT NULL,
  `my_referral_code` varchar(20) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `reset_password_token` varchar(255) NOT NULL,
  `latitude` varchar(10) NOT NULL,
  `longitude` float(10,2) NOT NULL,
  `verification_token` varchar(255) DEFAULT NULL,
  `bio_files` text,
  `bio` text,
  `jobstatus_notification` tinyint(4) DEFAULT NULL COMMENT '1:  yes, 0: no',
  `transaction_notification` tinyint(4) DEFAULT NULL COMMENT '1: yes, 0: no',
  `newjob_notification` tinyint(4) DEFAULT NULL COMMENT '1: yes 0: no',
  `expertise_id` int(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `language` varchar(4) DEFAULT NULL COMMENT 'en: english, ch: chinese',
  `last_login_as` tinyint(4) DEFAULT NULL COMMENT '1: questioner, 2: agent',
  `status` tinyint(4) DEFAULT '1' COMMENT '1:acive,0:deactivated,2:unverified',
  `utc_seconds_diff` varchar(255) DEFAULT NULL,
  `user_delete` tinyint(4) DEFAULT '1' COMMENT '1:deleted, 0:active',
  `delete_date` datetime DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of front_users
-- ----------------------------
INSERT INTO `front_users` VALUES ('1', 'Bhavdip Agent', '1', '98984398', 'bhavdip@creokestudios.com', '148291972045BgS.jpeg', '', '$2y$10$rotjwbcPazysdwxWWB2Enu0vgwxXQvqwqb6vzcj06GeU0ZcHK0Lka', '0', '0', '2', '2', '3', '', '', '0.00', '', '', 'hgjJmCYA', null, '', '', '0.00', 'ykWlCj30qPyvbmGucgyUK5s1j', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2016-12-28 10:08:41', null, null);
INSERT INTO `front_users` VALUES ('2', 'Vasupujy Patel', '1', '', 'jignesh@creolestudios.com', 'https://lh6.googleusercontent.com/-PZlJR1__v_8/AAAAAAAAAAI/AAAAAAAAAGo/5EJhtojuwbc/photo.jpg', '', null, '0', '0', '2', '2', '2', '104834963399171329331', null, '0.00', '', '', '1sSvs', null, '', '', '0.00', null, null, null, '0', '0', '0', '1', '1.0', 'en', null, '1', null, '0', null, '2016-12-28 10:17:10', null, '2016-12-28 10:17:10');
INSERT INTO `front_users` VALUES ('3', 'MartinAmundsen Amundsen', '0', '', '', 'https://graph.facebook.com/362458107432279/picture?type=large', '', null, '0', '0', '2', '2', '1', null, '362458107432279', '0.00', '', 'CARD-46B60629CT0366047LBXX6KA', 'stsqc', null, '', '', '0.00', null, null, null, '1', '1', '1', null, '1.0', 'en', null, '1', null, '0', null, '2016-12-28 10:17:56', null, '2016-12-28 10:17:56');
INSERT INTO `front_users` VALUES ('4', 'Johanty Rodge', '1', '9099004888', 'jignesh@creolestudios.com', '1482920305qDigC.jpg', '', '$2y$10$oM4em3WROlCHIeFJBLmM.OjeQPWo4KD1xgj5/pBLjx.Er1pr/N3hS', '0', '0', '1', '1', '3', '', '', '0.00', '2588721254', 'CARD-2A192639X52893728LB2LS6Y', 'PFKk0JIk', null, '', '', '0.00', '', null, null, '1', '1', '1', null, '1.1', 'en', null, '1', null, '0', null, '2016-12-28 10:18:25', null, '2017-01-06 07:15:38');
INSERT INTO `front_users` VALUES ('5', 'Kishan Vaghela', '0', '', 'kishan@creolestudios.com', 'https://lh3.googleusercontent.com/-exoWYZEm4sk/AAAAAAAAAAI/AAAAAAAAAOM/xhB2ypmmXZY/photo.jpg', '', null, '0', '0', '2', '1', '2', '108521196677289924809', null, '0.00', '', '', 'O8DrA', null, '', '', '0.00', null, null, null, '1', '1', '1', null, '1.0', 'en', null, '1', null, '0', null, '2016-12-28 10:18:48', null, '2016-12-28 10:18:48');
INSERT INTO `front_users` VALUES ('6', 'Ketty perry', '1', '9099004888', 'hardika@creolestudios.com', '14829203613dMV8.jpg', '', '$2y$10$ctlRHa321Wlm/MoX0hGl7.xyydqOZ53Qi/d8REkgUHt2ZPh2io0ky', '0', '0', '1', '2', '3', '', '', '0.00', '2588721254', 'CARD-4T3462619D688573CLBW7ZUQ', 'VXyRTpmt', null, '', '', '0.00', '', null, null, '1', '1', '1', null, '1.1', 'en', null, '1', null, '0', null, '2016-12-28 10:19:21', null, null);
INSERT INTO `front_users` VALUES ('7', 'Vasup Patel', '0', '', 'patelvasup9@gmail.com', '', '', null, '0', '0', '2', '1', '2', '106939280822935774249', null, '0.00', '', '', 'N2bfe', null, '', '', '0.00', null, null, null, '1', '1', '0', null, '1.0', 'en', null, '1', null, '0', null, '2016-12-28 10:20:49', null, '2016-12-28 10:20:49');
INSERT INTO `front_users` VALUES ('8', 'James Ponds', '1', '9099004888', 'mariaalex@creolestudios.com', '1482920447T4xSS.jpg', '', '$2y$10$iyG47/jFSeRy040AxeZgou3avlpVVhTzTPcY9IsAln9qJZw90jfse', '0', '0', '1', '1', '3', '', '', '0.00', '2588721254', '', '93C9wvrY', null, '', '', '0.00', 'LbDJSLssWlVgbr8Lf2X8Yt28L', null, null, '1', '1', '1', null, '1.1', 'en', null, '1', null, '0', null, '2016-12-28 10:20:47', null, null);
INSERT INTO `front_users` VALUES ('9', 'chan', '1', '23352665', 'chan@yopmail.com', '1482920563iVbxr.jpeg', '', '$2y$10$ESj5FfVPiIukyfWOVofxEupM3jKsScLA7AjmCjBmwtO8d7ta4Io6a', '0', '0', '2', '1', '3', '', '', '0.00', '12345678', '', 'ldoCIxwv', null, '', '', '0.00', '', null, null, '1', '1', '1', null, '1.0', 'en', null, '1', null, '0', null, '2016-12-28 10:22:43', null, '2017-01-17 06:14:18');
INSERT INTO `front_users` VALUES ('10', 'Micheal George', '1', '22366652', 'micheal@mailnator.com', '1482921958TOaSQ.jpeg', '', '$2y$10$QabuHsIPQMB3W436Y2pqBeKgp7CV0Xn7D4twBQ6PlIBRuspfEVu/K', '0', '0', '2', '2', '3', '', '', '0.00', '', '', 'a2Pdl8LB', null, '', '', '0.00', '0FVoEa7daWKi0WmGy8TDWHamS', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2016-12-28 10:45:58', null, null);
INSERT INTO `front_users` VALUES ('11', 'Micheal George', '1', '88800855', 'micheal@mailinator.com', '', '', '$2y$10$tY0q08mmhSvLXRJ0kQ.a7ukNGBDpnuoULHuKDUBickRU9c/6Lpzcq', '0', '0', '2', '2', '3', '', '', '0.00', '', '', 'amG5POap', null, '', '', '0.00', '98Odm279cNut0RIV696uN9EbZ', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2016-12-28 10:47:49', null, null);
INSERT INTO `front_users` VALUES ('12', 'Alex Ferguson', '1', '14780965', 'alexcreole@mailinator.cim', '', '', '$2y$10$6lUQMgo4HXOlHvIkaWtPlubBnJZ7zV4Btaxnxh.4R9ntk6ml2g6iu', '0', '0', '2', '2', '3', '', '', '0.00', '', '', 'TTaKSwm0', null, '', '', '0.00', 'UWQIZjSHfSAIDmsURd7yRySaX', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2016-12-28 10:51:27', null, null);
INSERT INTO `front_users` VALUES ('13', 'Alex Ferguson', '1', '85686836', 'alexcreole@mailinator.com', '', '', '$2y$10$B02eUyTSap/VlJx20eQvZeP57QRxCS3FAOlfeaQV.vKQHYzJufTtO', '0', '0', '2', '2', '3', '', '', '0.00', '', '', 'sXqjjBC8', null, '', '', '0.00', '', null, null, '0', '0', '0', null, '1.0', 'en', null, '1', null, '0', null, '2016-12-28 10:53:04', null, '2016-12-28 10:54:23');
INSERT INTO `front_users` VALUES ('14', 'Sandy Daniel', '2', '12345678', 'sandydaniel@mailinator.com', '', '', '$2y$10$XtbCcGRxcQ5XL0E2jCxBh.ECigMPtoxlCD9bTBczSv2cslLuZNtMC', '0', '0', '1', '1', '3', '', '', '0.00', '123456', 'CARD-6D931049CS3298317LBXZ5QA', '8LsTuu0c', null, '', '', '0.00', '', null, null, '1', '1', '1', null, '1.5', 'en', null, '1', null, '0', null, '2016-12-28 11:29:47', null, '2017-01-09 05:52:51');
INSERT INTO `front_users` VALUES ('15', 'jin pong', '1', '96325874', 'jin@maillinator.com', '', '', '$2y$10$c2TPsNLNWp5Dfrek2ILGiuRl9i9CNG4SdIIVtqq76wg2UITA7Lks.', '0', '0', '2', '2', '3', '', '', '0.00', '12345678', '', 'WqXzyW7C', null, '', '', '0.00', 'IdWRH7osMr2qIA4zH2ayOAdEF', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2016-12-30 04:57:46', null, null);
INSERT INTO `front_users` VALUES ('16', 'din smith', '1', '99939636', 'smith@yopmail.com', '', '', '$2y$10$KyApC00gJmiYSVymbD5zsumRk7XpmEpWH0PHDKQcx4TzvGlFnfjom', '0', '0', '2', '2', '3', '', '', '0.00', '12345678', '', '7Clwyeg1', null, '', '', '0.00', 'MKBa3dY9X0f1r5jXnTBG69f3j', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2016-12-30 05:54:40', null, null);
INSERT INTO `front_users` VALUES ('17', 'James', '1', '96855552', 'james@yopmail.com', '', '', '$2y$10$nOQIB82nvCPdhD40OorQ5e2USsWuBw/7FUWSLnuuejLEeScpaWgPS', '0', '0', '2', '1', '3', '', '', '0.00', '12345678', '', 'VxCQ3dfc', null, '', '', '0.00', '', null, null, '1', '1', '1', null, '1.0', 'en', null, '1', null, '0', null, '2017-01-02 06:32:14', null, '2017-01-04 07:22:05');
INSERT INTO `front_users` VALUES ('18', 'nick', '1', '96857412', 'nick@yopmail.com', '', '', '$2y$10$cObzEU0UjgcD4wjduMGoWu8vZ.MejPKHIoxFSikExOt9MHdsiq/ZS', '0', '0', '2', '2', '3', '', '', '0.00', '12345678', '', '0nN8ybhh', null, '', '', '0.00', 'JEOz4c1QxMZGewMRIiEtzSPSb', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2017-01-02 06:40:31', null, null);
INSERT INTO `front_users` VALUES ('19', 'VP Patel', '0', '', '', 'https://graph.facebook.com/901227649978766/picture?type=large', '', null, '0', '0', '2', '2', '1', null, '901227649978766', '0.00', '', '', '1sxbQ', null, '', '', '0.00', null, null, null, '1', '1', '1', null, '1.0', 'en', null, '1', null, '0', null, '2017-01-02 13:41:52', null, '2017-01-04 15:35:53');
INSERT INTO `front_users` VALUES ('20', 'Zhang Gaurya', '1', '96385244', 'zhang@yopmail.com', '', '', '$2y$10$YGHJiRWbnkjmU3W8SS9Uiu.N5z0KRZo7Z1MB.mSBRGvug6FuMq592', '0', '0', '2', '2', '3', '', '', '0.00', '12345678', '', 'am4AJyXd', null, '', '', '0.00', '4AzlNvjSBfmAmmPQs7G3htl5W', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2017-01-04 06:06:29', null, '2017-01-04 15:35:53');
INSERT INTO `front_users` VALUES ('21', 'Ding Ling', '1', '85236974', 'ding@yopmail.com', '', '', '$2y$10$IcYj.XJs6jNLgQJ49W7G9ufiNC36XVj4VjaZUFZP8PmBSH1o5vwM2', '0', '0', '2', '2', '3', '', '', '0.00', '12345678', '', '3LOXwGmr', null, '', '', '0.00', 'wjgLLQzRIi0Q2KgLRfGIebQ3D', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2017-01-04 06:09:07', null, '2017-01-04 15:35:53');
INSERT INTO `front_users` VALUES ('22', 'fu liang', '1', '96385247', 'fu@yopmail.com.com', '', '', '$2y$10$RYYP1xHOgIez5PyccpgmS.w3/qz0ciq8FGxn0RXM3lPlEjpfabWnW', '0', '0', '2', '1', '3', '', '', '0.00', '12345678', '', 'fjoIhoIM', null, '', '', '0.00', 'EUUOJOTB42UwQKDJPAaW7LljY', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2017-01-04 06:25:43', null, '2017-01-04 15:35:53');
INSERT INTO `front_users` VALUES ('23', 'xue chen', '1', '96385247', 'xue@yopmail.com', '', '', '$2y$10$nufF36R2iWPL/wLzAQydvOqX.N0Wy18Vq9vtr1uumnk/koCi/oeVW', '0', '0', '2', '1', '3', '', '', '0.00', '12345678', '', '2to6RYJW', null, '', '', '0.00', 'HaBxD9XwnHuSVklsI4yIA8HHq', null, null, '1', '1', '1', null, '1.0', 'en', null, '2', null, '0', null, '2017-01-04 06:26:53', null, '2017-01-04 10:15:53');
INSERT INTO `front_users` VALUES ('24', 'Nidhi Patel', '1', '', '', '', '', null, '0', '0', '1', '2', '1', null, '1212235532189965', '0.00', '', 'CARD-4T3462619D688573CLBW7ZAA', '3d8xv', null, '', '', '0.00', null, null, null, '1', '1', '1', null, '1.0', 'en', null, '1', null, '0', null, '2017-01-05 10:32:33', null, '2017-01-05 10:32:33');

-- ----------------------------
-- Table structure for front_user_token
-- ----------------------------
DROP TABLE IF EXISTS `front_user_token`;
CREATE TABLE `front_user_token` (
  `token_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `os_type` tinyint(4) NOT NULL COMMENT '1: ios, 2:android',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of front_user_token
-- ----------------------------
INSERT INTO `front_user_token` VALUES ('1', '2', 'fHnMx-UXOew:APA91bEZ2zvrg4uOr7tmS1iiXdQ_CnB_sYi3ZRyqtAfHR3huItGLAqtjAvOvb3vWNAp2IyeCkVVIfx91MHo8yyHua6x4jczcQLFlypdBWMPNViR1_MMnriejKQ66mzezdK1OtvNprO5u', '2', '2016-12-28 04:17:11');
INSERT INTO `front_user_token` VALUES ('3', '13', 'ctVcGVS2Ekg:APA91bG4rEx75t8E6_OxDIshbiWf-HaEuMQ4PUXxHGVj3RG7fhbCa7iHn3WPeaAh4GZeMWMeyMdBBh4-8-N4CYnus0WbOjpxjO_DguQfc-SXYOh6F9_Eo-_JC717m2ytl4ZEB1AyWFKJ', '2', '2016-12-28 04:54:23');
INSERT INTO `front_user_token` VALUES ('4', '2', 'eiW4-BS4O6s:APA91bEgluXO1V_9AuWDsGJdkNtd27pcZTvI4yXVQxkkMqFK4e5hpIKDbKZPHPZ0eopRjYreQ6UMOz19QYxbJkapPnxAs15RPvjFIw_Q8X8MQSe085zPRXR48ohIbjrHPcudlaTi6mxM', '2', '2016-12-28 23:26:45');
INSERT INTO `front_user_token` VALUES ('6', '2', 'dzFLZFKi2Do:APA91bGSAH-nRXSe5QUO4iL3pOwnz_HctPetOeLfAwlHjIKXEWmLqdqKvSccxoJHh2d64__9r93qXxWZ26E-pyiX3k0jeSsAOXGfAIsq89kyOEGMmM9HBENBTnfk3-b8bu2gzaGSFXtN', '2', '2016-12-29 06:23:51');
INSERT INTO `front_user_token` VALUES ('7', '2', 'fNYk1wH16sI:APA91bETVUWI2ROtbIluzwyjUuzi24HphBHSaNZF4c-64Y2W34nBaLhE45c6sARLTilNDbizlXkiVhYwpjWjBa4cztVRjGdFEqxJprS_5aZZZufA26nHmA1O0HRXF_J4feC0EZtQStNc', '2', '2016-12-30 00:41:46');
INSERT INTO `front_user_token` VALUES ('8', '9', 'd_MgMQAnRa8:APA91bHrh7hQVRu_LFnnEhwvnJmcHQmK912HQIYYswUaYK-j9wfN7lfv-loPSEMwaT2k83t9sbq6wp1Mj_xCvD2W7agovBMK1SgrGwfUOWHLBUyRebdVZ3-k6kjyE1R1nbWzetKizRyN', '2', '2017-01-01 23:41:04');
INSERT INTO `front_user_token` VALUES ('9', '9', 'dP9qrF64pQA:APA91bHysxfrcofiRqCA4YRuOQVHfTgBgFWoQn_0TWdwlfIqs-8WAztG9rUBr5IFuc5iTB3bE7PfYI6WmZirPxosa4_c3Z1kDazNQLmZPwFcANh-t4-VEkXsIpnBEhj5OUqeh3qeMzrx', '2', '2017-01-02 00:11:15');
INSERT INTO `front_user_token` VALUES ('10', '4', '12315641654123', '1', '2017-01-05 04:27:05');

-- ----------------------------
-- Table structure for manage_users
-- ----------------------------
DROP TABLE IF EXISTS `manage_users`;
CREATE TABLE `manage_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `forgot_token` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `admin_type` tinyint(4) DEFAULT NULL COMMENT '1 => Super Admin 2 => Section Admin',
  `status` tinyint(4) DEFAULT '1' COMMENT '1:active,0:inactive',
  `is_delete` tinyint(4) DEFAULT '1' COMMENT '1:no,0:yes',
  `last_activity` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of manage_users
-- ----------------------------
INSERT INTO `manage_users` VALUES ('2', 'Jigs Bellfort', '9032922641493667317How_you_doing.jpg', 'abdu@gmail.com', '$2y$10$uKfPkk2SjK/iSEUuyy2jXeGUhJpiKa/UQ22D73zG2Cq4hlkL7IxHW', null, null, null, 'yDXjIT8t0DwmkJqowkyLQbsD9cEDHGSbgiGOL54uA7Y4HevxijeDGw8O8ebZ', 'GQOEQ6YtmKBtGLnnB0ZA0XQ4F', '2', '1', '1', null, null, '2016-10-11 13:07:11');

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `from_user_id` int(11) DEFAULT '0',
  `user_type` tinyint(4) DEFAULT NULL COMMENT '1=> requester, 2=> agent',
  `job_status` int(11) DEFAULT '0',
  `is_awared` tinyint(4) DEFAULT '0' COMMENT '[0 =false 1= true]',
  `notification_type` tinyint(11) NOT NULL COMMENT '1 => job; 2=>message',
  `read_flag` tinyint(4) DEFAULT '0' COMMENT '0:no,1:yes',
  `ch_message` text,
  `message` text CHARACTER SET latin1,
  `created_on` datetime NOT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=372 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notifications
-- ----------------------------
INSERT INTO `notifications` VALUES ('1', '1', '5', '2', '1', '2', '0', '2', '0', '用户已申请作业1', 'Vasupujy Patel has applied for the Job #1', '2016-12-28 10:22:33', null);
INSERT INTO `notifications` VALUES ('2', '1', '2', '5', '2', '1', '0', '1', '0', '您对工作＃1的提案已被拒绝。', 'Your proposal on job #1 has been declined.', '2016-12-28 10:22:47', null);
INSERT INTO `notifications` VALUES ('3', '2', '2', '4', '1', '2', '0', '2', '0', '用户已申请作业2', 'Johanty Rodge has applied for the Job #2', '2016-12-28 10:23:15', null);
INSERT INTO `notifications` VALUES ('4', '2', '2', '3', '1', '2', '0', '2', '0', '用户已申请作业2', 'MartinAmundsen Amundsen has applied for the Job #2', '2016-12-28 10:26:13', null);
INSERT INTO `notifications` VALUES ('5', '2', '3', '2', '2', '1', '1', '1', '1', '您对工作＃2的提案已获批准。', 'Your proposal on job #2 has been approved.', '2016-12-28 10:28:47', '2016-12-27 23:14:34');
INSERT INTO `notifications` VALUES ('6', '2', '2', '3', '1', '1', '0', '1', '0', '您的工作＃2已被代理人MartinAmundsen Amundsen在800 HKD拒绝。', 'Your job #2  has been declined by agent MartinAmundsen Amundsen at 800 HKD.', '2016-12-28 10:29:45', null);
INSERT INTO `notifications` VALUES ('7', '2', '2', '3', '1', '2', '0', '2', '0', '用户已申请作业2', 'MartinAmundsen Amundsen has applied for the Job #2', '2016-12-28 10:30:55', null);
INSERT INTO `notifications` VALUES ('8', '2', '3', '2', '2', '1', '1', '1', '1', '您对工作＃2的提案已获批准。', 'Your proposal on job #2 has been approved.', '2016-12-28 10:31:39', '2016-12-27 23:14:31');
INSERT INTO `notifications` VALUES ('9', '2', '2', '3', '1', '1', '1', '1', '0', '您的工作＃2已被代理人MartinAmundsen Amundsen在500 HKD接受。', 'Your job #2  has been accepted by agent MartinAmundsen Amundsen at 500 HKD.', '2016-12-28 10:32:15', null);
INSERT INTO `notifications` VALUES ('10', '3', '4', '3', '1', '2', '0', '2', '0', '用户已申请作业3', 'MartinAmundsen Amundsen has applied for the Job #3', '2016-12-28 10:33:00', null);
INSERT INTO `notifications` VALUES ('11', '1', '5', '3', '1', '2', '0', '2', '0', '用户已申请作业1', 'MartinAmundsen Amundsen has applied for the Job #1', '2016-12-28 10:38:05', null);
INSERT INTO `notifications` VALUES ('12', '1', '5', '9', '1', '2', '0', '2', '0', '用户已申请作业1', 'chan has applied for the Job #1', '2016-12-28 10:38:38', null);
INSERT INTO `notifications` VALUES ('13', '3', '6', '4', '2', '1', '1', '1', '0', '您对工作＃3的提案已获批准。', 'Your proposal on job #3 has been approved.', '2016-12-28 10:48:39', null);
INSERT INTO `notifications` VALUES ('14', '3', '4', '6', '1', '1', '1', '1', '0', '您的工作＃3已被代理人Ketty perry在40 HKD接受。', 'Your job #3  has been accepted by agent Ketty perry at 40 HKD.', '2016-12-28 10:49:16', null);
INSERT INTO `notifications` VALUES ('15', '3', '8', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃3', 'Ketty perry  has changed the Job status from In Process to Pause for Job #3', '2016-12-28 10:49:34', null);
INSERT INTO `notifications` VALUES ('16', '3', '8', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃3', 'Ketty perry  has changed the Job status from Paused to In Process for Job #3', '2016-12-28 10:49:40', null);
INSERT INTO `notifications` VALUES ('17', '3', '8', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃3', 'Ketty perry  has changed the Job status from In Process to Pause for Job #3', '2016-12-28 10:49:45', null);
INSERT INTO `notifications` VALUES ('18', '3', '8', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃3', 'Ketty perry  has changed the Job status from Paused to In Process for Job #3', '2016-12-28 10:49:49', null);
INSERT INTO `notifications` VALUES ('19', '3', '8', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃3', 'Ketty perry  has changed the Job status from In Process to Pause for Job #3', '2016-12-28 10:49:55', null);
INSERT INTO `notifications` VALUES ('20', '3', '8', '6', '1', '6', '0', '2', '0', '您的工作＃3已被管理员提出异议。', 'Your job #3  has been disputed by Admin.', '2016-12-28 10:49:55', null);
INSERT INTO `notifications` VALUES ('21', '3', '8', '6', '1', '2', '0', '2', '0', '用户已申请作业3', 'Ketty perry has applied for the Job #3', '2016-12-28 10:55:07', null);
INSERT INTO `notifications` VALUES ('22', '3', '6', '4', '2', '1', '1', '1', '0', '您对工作＃3的提案已获批准。', 'Your proposal on job #3 has been approved.', '2016-12-28 10:56:13', null);
INSERT INTO `notifications` VALUES ('23', '3', '4', '6', '1', '1', '1', '1', '0', '您的工作＃3已被代理人Ketty perry在40 HKD接受。', 'Your job #3  has been accepted by agent Ketty perry at 40 HKD.', '2016-12-28 10:56:24', null);
INSERT INTO `notifications` VALUES ('24', '3', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃3', 'Ketty perry  has changed the Job status from In Process to Pause for Job #3', '2016-12-28 11:01:13', null);
INSERT INTO `notifications` VALUES ('25', '3', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃3', 'Ketty perry  has changed the Job status from Paused to In Process for Job #3', '2016-12-28 11:01:18', null);
INSERT INTO `notifications` VALUES ('26', '3', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃3', 'Ketty perry  has changed the Job status from In Process to Pause for Job #3', '2016-12-28 11:01:23', null);
INSERT INTO `notifications` VALUES ('27', '3', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃3', 'Ketty perry  has changed the Job status from Paused to In Process for Job #3', '2016-12-28 11:01:27', null);
INSERT INTO `notifications` VALUES ('28', '3', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃3', 'Ketty perry  has changed the Job status from In Process to Pause for Job #3', '2016-12-28 11:01:31', null);
INSERT INTO `notifications` VALUES ('29', '3', '4', '6', '1', '6', '0', '2', '0', '您的工作＃3已被管理员提出异议。', 'Your job #3  has been disputed by Admin.', '2016-12-28 11:01:31', null);
INSERT INTO `notifications` VALUES ('30', '1', '5', '13', '1', '2', '0', '2', '0', '用户已申请作业1', 'Alex Ferguson has applied for the Job #1', '2016-12-28 11:09:19', null);
INSERT INTO `notifications` VALUES ('31', '8', '14', '4', '1', '2', '0', '2', '0', '用户已申请作业8', 'Johanty Rodge has applied for the Job #8', '2016-12-28 12:10:14', null);
INSERT INTO `notifications` VALUES ('32', '8', '14', '3', '1', '2', '0', '2', '0', '用户已申请作业8', 'MartinAmundsen Amundsen has applied for the Job #8', '2016-12-28 12:12:17', null);
INSERT INTO `notifications` VALUES ('33', '7', '14', '3', '1', '2', '0', '2', '0', '用户已申请作业7', 'MartinAmundsen Amundsen has applied for the Job #7', '2016-12-28 12:13:47', null);
INSERT INTO `notifications` VALUES ('34', '8', '14', '4', '1', '2', '0', '2', '0', '用户已申请作业8', 'Johanty Rodge has applied for the Job #8', '2016-12-28 12:35:50', null);
INSERT INTO `notifications` VALUES ('35', '1', '9', '5', '2', '1', '1', '1', '1', '您对工作＃1的提案已获批准。', 'Your proposal on job #1 has been approved.', '2016-12-29 05:17:32', '2017-01-12 00:37:13');
INSERT INTO `notifications` VALUES ('36', '8', '14', '5', '1', '2', '0', '2', '0', '用户已申请作业8', 'Kishan Vaghela has applied for the Job #8', '2016-12-29 05:19:38', null);
INSERT INTO `notifications` VALUES ('37', '1', '5', '9', '1', '1', '1', '1', '0', '您的工作＃1已被代理人chan在22 HKD接受。', 'Your job #1  has been accepted by agent chan at 22 HKD.', '2016-12-29 05:24:49', null);
INSERT INTO `notifications` VALUES ('38', '9', '3', '2', '1', '2', '0', '2', '1', '用户已申请作业9', 'Vasupujy Patel has applied for the Job #9', '2016-12-29 05:28:06', '2017-01-02 18:09:54');
INSERT INTO `notifications` VALUES ('39', '9', '2', '3', '2', '1', '1', '1', '0', '您对工作＃9的提案已获批准。', 'Your proposal on job #9 has been approved.', '2016-12-29 05:29:12', null);
INSERT INTO `notifications` VALUES ('40', '9', '3', '2', '1', '1', '0', '1', '1', '您的工作＃9已被代理人Vasupujy Patel在900 HKD拒绝。', 'Your job #9  has been declined by agent Vasupujy Patel at 900 HKD.', '2016-12-29 05:33:04', '2016-12-28 18:03:10');
INSERT INTO `notifications` VALUES ('41', '12', '2', '3', '1', '2', '0', '2', '0', '用户已申请作业12', 'MartinAmundsen Amundsen has applied for the Job #12', '2016-12-29 06:15:33', null);
INSERT INTO `notifications` VALUES ('42', '14', '2', '9', '1', '2', '0', '2', '1', '用户已申请作业14', 'chan has applied for the Job #14', '2016-12-30 07:09:47', '2017-01-03 16:50:45');
INSERT INTO `notifications` VALUES ('43', '1', '5', '9', '1', '5', '0', '2', '0', 'chan 已将作业状态从进程更改为暂停作业＃1', 'chan  has changed the Job status from In Process to Pause for Job #1', '2017-01-02 10:44:52', null);
INSERT INTO `notifications` VALUES ('44', '1', '5', '9', '1', '3', '0', '2', '0', 'chan 已将作业状态从“已暂停”更改为“正在处理”作业＃1', 'chan  has changed the Job status from Paused to In Process for Job #1', '2017-01-02 10:45:06', null);
INSERT INTO `notifications` VALUES ('45', '1', '5', '9', '1', '5', '0', '2', '0', 'chan 已将作业状态从进程更改为暂停作业＃1', 'chan  has changed the Job status from In Process to Pause for Job #1', '2017-01-02 10:45:21', null);
INSERT INTO `notifications` VALUES ('46', '1', '5', '9', '1', '3', '0', '2', '0', 'chan 已将作业状态从“已暂停”更改为“正在处理”作业＃1', 'chan  has changed the Job status from Paused to In Process for Job #1', '2017-01-02 10:45:31', null);
INSERT INTO `notifications` VALUES ('47', '22', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业22', 'Ketty perry has applied for the Job #22', '2017-01-06 05:19:14', null);
INSERT INTO `notifications` VALUES ('48', '22', '6', '4', '2', '1', '1', '1', '0', '您对工作＃22的提案已获批准。', 'Your proposal on job #22 has been approved.', '2017-01-06 05:20:53', null);
INSERT INTO `notifications` VALUES ('49', '22', '4', '6', '1', '1', '1', '1', '0', '您的工作＃22已被代理人Ketty perry在110 HKD接受。', 'Your job #22  has been accepted by agent Ketty perry at 110 HKD.', '2017-01-06 13:13:23', null);
INSERT INTO `notifications` VALUES ('50', '24', '2', '14', '2', '1', '0', '3', '0', '您的专业领域有新工作（#24）。', 'You have new job #24 in your expertise area.', '2017-01-06 13:29:51', null);
INSERT INTO `notifications` VALUES ('51', '24', '14', '3', '1', '2', '0', '2', '0', '用户已申请作业24', 'MartinAmundsen Amundsen has applied for the Job #24', '2017-01-06 13:30:44', null);
INSERT INTO `notifications` VALUES ('52', '24', '3', '14', '2', '1', '1', '1', '0', '您对工作＃24的提案已获批准。', 'Your proposal on job #24 has been approved.', '2017-01-06 13:31:31', null);
INSERT INTO `notifications` VALUES ('53', '24', '14', '3', '1', '1', '1', '1', '0', '您的工作＃24已被代理人MartinAmundsen Amundsen在50 HKD接受。', 'Your job #24  has been accepted by agent MartinAmundsen Amundsen at 50 HKD.', '2017-01-06 13:45:29', null);
INSERT INTO `notifications` VALUES ('54', '24', '14', '3', '1', '1', '1', '1', '0', '您的工作＃24已被代理人MartinAmundsen Amundsen在50 HKD接受。', 'Your job #24  has been accepted by agent MartinAmundsen Amundsen at 50 HKD.', '2017-01-06 13:47:04', null);
INSERT INTO `notifications` VALUES ('55', '25', '14', '3', '1', '2', '0', '2', '0', '用户已申请作业25', 'MartinAmundsen Amundsen has applied for the Job #25', '2017-01-06 13:48:39', null);
INSERT INTO `notifications` VALUES ('56', '25', '3', '14', '2', '1', '1', '1', '0', '您对工作＃25的提案已获批准。', 'Your proposal on job #25 has been approved.', '2017-01-06 13:48:56', null);
INSERT INTO `notifications` VALUES ('57', '25', '14', '3', '1', '1', '1', '1', '0', '您的工作＃25已被代理人MartinAmundsen Amundsen在100 HKD接受。', 'Your job #25  has been accepted by agent MartinAmundsen Amundsen at 100 HKD.', '2017-01-06 13:49:32', null);
INSERT INTO `notifications` VALUES ('58', '25', '14', '3', '1', '1', '1', '1', '0', '您的工作＃25已被代理人MartinAmundsen Amundsen在100 HKD接受。', 'Your job #25  has been accepted by agent MartinAmundsen Amundsen at 100 HKD.', '2017-01-06 13:50:57', null);
INSERT INTO `notifications` VALUES ('59', '8', '3', '14', '2', '1', '1', '1', '0', '您对工作＃8的提案已获批准。', 'Your proposal on job #8 has been approved.', '2017-01-06 13:58:07', null);
INSERT INTO `notifications` VALUES ('60', '8', '14', '3', '1', '1', '1', '1', '1', '您的工作＃8已被代理人MartinAmundsen Amundsen在100 HKD接受。', 'Your job #8  has been accepted by agent MartinAmundsen Amundsen at 100 HKD.', '2017-01-06 13:58:29', '2017-01-06 02:29:13');
INSERT INTO `notifications` VALUES ('61', '22', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃22', 'The Job Status has been changed from In Process to Completed for Job #22', '2017-01-10 05:25:54', null);
INSERT INTO `notifications` VALUES ('62', '22', '6', '4', '2', '1', '1', '1', '0', '您对工作＃22的提案已获批准。', 'Your proposal on job #22 has been approved.', '2017-01-10 12:12:24', null);
INSERT INTO `notifications` VALUES ('63', '22', '4', '6', '1', '1', '1', '1', '0', '您的工作＃22已被代理人Ketty perry在50 HKD接受。', 'Your job #22  has been accepted by agent Ketty perry at 50 HKD.', '2017-01-10 12:28:47', null);
INSERT INTO `notifications` VALUES ('64', '22', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃22', 'The Job Status has been changed from In Process to Completed for Job #22', '2017-01-10 12:43:12', null);
INSERT INTO `notifications` VALUES ('65', '22', '4', '6', '1', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃22', 'The Job Status has been changed from In Process to Completed for Job #22', '2017-01-10 12:43:52', null);
INSERT INTO `notifications` VALUES ('66', '26', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业26', 'Ketty perry has applied for the Job #26', '2017-01-10 12:50:57', null);
INSERT INTO `notifications` VALUES ('67', '3', '4', '6', '1', '1', '1', '1', '0', '您的工作＃3已被代理人Ketty perry在40 HKD接受。', 'Your job #3  has been accepted by agent Ketty perry at 40 HKD.', '2017-01-10 12:52:22', null);
INSERT INTO `notifications` VALUES ('68', '26', '6', '4', '2', '1', '1', '1', '0', '您对工作＃26的提案已获批准。', 'Your proposal on job #26 has been approved.', '2017-01-10 12:54:46', null);
INSERT INTO `notifications` VALUES ('69', '3', '4', '6', '1', '1', '1', '1', '0', '您的工作＃3已被代理人Ketty perry在40 HKD接受。', 'Your job #3  has been accepted by agent Ketty perry at 40 HKD.', '2017-01-10 12:55:27', null);
INSERT INTO `notifications` VALUES ('70', '27', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业27', 'Ketty perry has applied for the Job #27', '2017-01-10 12:56:58', null);
INSERT INTO `notifications` VALUES ('71', '27', '6', '4', '2', '1', '1', '1', '0', '您对工作＃27的提案已获批准。', 'Your proposal on job #27 has been approved.', '2017-01-10 12:57:24', null);
INSERT INTO `notifications` VALUES ('72', '27', '4', '6', '1', '1', '1', '1', '0', '您的工作＃27已被代理人Ketty perry在40 HKD接受。', 'Your job #27  has been accepted by agent Ketty perry at 40 HKD.', '2017-01-10 12:58:19', null);
INSERT INTO `notifications` VALUES ('73', '27', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃27', 'Ketty perry  has changed the Job status from In Process to Pause for Job #27', '2017-01-10 12:59:08', null);
INSERT INTO `notifications` VALUES ('74', '27', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃27', 'Ketty perry  has changed the Job status from Paused to In Process for Job #27', '2017-01-10 12:59:19', null);
INSERT INTO `notifications` VALUES ('75', '27', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃27', 'Ketty perry  has changed the Job status from In Process to Pause for Job #27', '2017-01-10 12:59:26', null);
INSERT INTO `notifications` VALUES ('76', '27', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃27', 'Ketty perry  has changed the Job status from Paused to In Process for Job #27', '2017-01-10 12:59:30', null);
INSERT INTO `notifications` VALUES ('77', '27', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃27', 'Ketty perry  has changed the Job status from In Process to Pause for Job #27', '2017-01-10 12:59:35', null);
INSERT INTO `notifications` VALUES ('78', '27', '4', '6', '1', '6', '0', '2', '0', '您的工作＃27已被管理员提出异议。', 'Your job #27  has been disputed by Admin.', '2017-01-10 12:59:36', null);
INSERT INTO `notifications` VALUES ('79', '28', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业28', 'Ketty perry has applied for the Job #28', '2017-01-10 13:35:05', null);
INSERT INTO `notifications` VALUES ('80', '28', '6', '4', '2', '1', '1', '1', '0', '您对工作＃28的提案已获批准。', 'Your proposal on job #28 has been approved.', '2017-01-10 13:35:28', null);
INSERT INTO `notifications` VALUES ('81', '28', '4', '6', '1', '1', '1', '1', '0', '您的工作＃28已被代理人Ketty perry在40 HKD接受。', 'Your job #28  has been accepted by agent Ketty perry at 40 HKD.', '2017-01-10 13:35:55', null);
INSERT INTO `notifications` VALUES ('82', '28', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃28', 'Ketty perry  has changed the Job status from In Process to Pause for Job #28', '2017-01-10 13:36:13', null);
INSERT INTO `notifications` VALUES ('83', '28', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃28', 'Ketty perry  has changed the Job status from Paused to In Process for Job #28', '2017-01-10 13:36:17', null);
INSERT INTO `notifications` VALUES ('84', '28', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃28', 'Ketty perry  has changed the Job status from In Process to Pause for Job #28', '2017-01-10 13:36:21', null);
INSERT INTO `notifications` VALUES ('85', '28', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃28', 'Ketty perry  has changed the Job status from Paused to In Process for Job #28', '2017-01-10 13:36:26', null);
INSERT INTO `notifications` VALUES ('86', '28', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃28', 'Ketty perry  has changed the Job status from In Process to Pause for Job #28', '2017-01-10 13:36:30', null);
INSERT INTO `notifications` VALUES ('87', '28', '4', '6', '1', '6', '0', '2', '0', '您的工作＃28已被管理员提出异议。', 'Your job #28  has been disputed by Admin.', '2017-01-10 13:36:31', null);
INSERT INTO `notifications` VALUES ('88', '29', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业29', 'Ketty perry has applied for the Job #29', '2017-01-10 13:40:55', null);
INSERT INTO `notifications` VALUES ('89', '29', '4', '6', '1', '1', '1', '1', '0', '您的工作＃29已被代理人Ketty perry在100 HKD接受。', 'Your job #29  has been accepted by agent Ketty perry at 100 HKD.', '2017-01-10 13:41:32', null);
INSERT INTO `notifications` VALUES ('90', '29', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃29', 'Ketty perry  has changed the Job status from In Process to Pause for Job #29', '2017-01-10 13:42:00', null);
INSERT INTO `notifications` VALUES ('91', '29', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃29', 'Ketty perry  has changed the Job status from Paused to In Process for Job #29', '2017-01-10 13:42:07', null);
INSERT INTO `notifications` VALUES ('92', '29', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃29', 'Ketty perry  has changed the Job status from In Process to Pause for Job #29', '2017-01-10 13:42:11', null);
INSERT INTO `notifications` VALUES ('93', '29', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃29', 'Ketty perry  has changed the Job status from Paused to In Process for Job #29', '2017-01-10 13:42:15', null);
INSERT INTO `notifications` VALUES ('94', '29', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃29', 'Ketty perry  has changed the Job status from In Process to Pause for Job #29', '2017-01-10 13:42:18', null);
INSERT INTO `notifications` VALUES ('95', '29', '4', '6', '1', '6', '0', '2', '0', '您的工作＃29已被管理员提出异议。', 'Your job #29  has been disputed by Admin.', '2017-01-10 13:42:19', null);
INSERT INTO `notifications` VALUES ('96', '30', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业30', 'Ketty perry has applied for the Job #30', '2017-01-10 13:48:02', null);
INSERT INTO `notifications` VALUES ('97', '30', '4', '6', '1', '1', '1', '1', '0', '您的工作＃30已被代理人Ketty perry在150 HKD接受。', 'Your job #30  has been accepted by agent Ketty perry at 150 HKD.', '2017-01-10 13:48:38', null);
INSERT INTO `notifications` VALUES ('98', '30', '4', '6', '1', '1', '1', '1', '0', '您的工作＃30已被代理人Ketty perry在150 HKD接受。', 'Your job #30  has been accepted by agent Ketty perry at 150 HKD.', '2017-01-10 13:49:11', null);
INSERT INTO `notifications` VALUES ('99', '30', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃30', 'Ketty perry  has changed the Job status from In Process to Pause for Job #30', '2017-01-10 13:49:48', null);
INSERT INTO `notifications` VALUES ('100', '30', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃30', 'Ketty perry  has changed the Job status from Paused to In Process for Job #30', '2017-01-10 13:49:52', null);
INSERT INTO `notifications` VALUES ('101', '30', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃30', 'Ketty perry  has changed the Job status from In Process to Pause for Job #30', '2017-01-10 13:49:56', null);
INSERT INTO `notifications` VALUES ('102', '30', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃30', 'Ketty perry  has changed the Job status from Paused to In Process for Job #30', '2017-01-10 13:49:59', null);
INSERT INTO `notifications` VALUES ('103', '30', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃30', 'Ketty perry  has changed the Job status from In Process to Pause for Job #30', '2017-01-10 13:50:03', null);
INSERT INTO `notifications` VALUES ('104', '30', '4', '6', '1', '6', '0', '2', '0', '您的工作＃30已被管理员提出异议。', 'Your job #30  has been disputed by Admin.', '2017-01-10 13:50:04', null);
INSERT INTO `notifications` VALUES ('105', '31', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业31', 'Ketty perry has applied for the Job #31', '2017-01-10 13:57:14', null);
INSERT INTO `notifications` VALUES ('106', '31', '6', '4', '2', '1', '1', '1', '0', '您对工作＃31的提案已获批准。', 'Your proposal on job #31 has been approved.', '2017-01-10 13:57:42', null);
INSERT INTO `notifications` VALUES ('107', '32', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业32', 'Ketty perry has applied for the Job #32', '2017-01-10 14:00:39', null);
INSERT INTO `notifications` VALUES ('108', '31', '6', '4', '2', '1', '1', '1', '0', '您对工作＃31的提案已获批准。', 'Your proposal on job #31 has been approved.', '2017-01-10 14:00:49', null);
INSERT INTO `notifications` VALUES ('109', '32', '4', '6', '1', '1', '1', '1', '0', '您的工作＃32已被代理人Ketty perry在150 HKD接受。', 'Your job #32  has been accepted by agent Ketty perry at 150 HKD.', '2017-01-10 14:01:13', null);
INSERT INTO `notifications` VALUES ('110', '32', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃32', 'Ketty perry  has changed the Job status from In Process to Pause for Job #32', '2017-01-10 14:02:06', null);
INSERT INTO `notifications` VALUES ('111', '32', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃32', 'Ketty perry  has changed the Job status from Paused to In Process for Job #32', '2017-01-10 14:02:10', null);
INSERT INTO `notifications` VALUES ('112', '32', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃32', 'Ketty perry  has changed the Job status from In Process to Pause for Job #32', '2017-01-10 14:02:14', null);
INSERT INTO `notifications` VALUES ('113', '32', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃32', 'Ketty perry  has changed the Job status from Paused to In Process for Job #32', '2017-01-10 14:02:18', null);
INSERT INTO `notifications` VALUES ('114', '32', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃32', 'Ketty perry  has changed the Job status from In Process to Pause for Job #32', '2017-01-10 14:02:21', null);
INSERT INTO `notifications` VALUES ('115', '32', '4', '6', '1', '6', '0', '2', '0', '您的工作＃32已被管理员提出异议。', 'Your job #32  has been disputed by Admin.', '2017-01-10 14:02:22', null);
INSERT INTO `notifications` VALUES ('116', '33', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业33', 'Ketty perry has applied for the Job #33', '2017-01-11 05:07:21', null);
INSERT INTO `notifications` VALUES ('117', '33', '6', '4', '2', '1', '1', '1', '0', '您对工作＃33的提案已获批准。', 'Your proposal on job #33 has been approved.', '2017-01-11 05:07:59', null);
INSERT INTO `notifications` VALUES ('118', '33', '4', '6', '1', '1', '1', '1', '0', '您的工作＃33已被代理人Ketty perry在40 HKD接受。', 'Your job #33  has been accepted by agent Ketty perry at 40 HKD.', '2017-01-11 05:09:21', null);
INSERT INTO `notifications` VALUES ('119', '33', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃33', 'Ketty perry  has changed the Job status from In Process to Pause for Job #33', '2017-01-11 05:13:55', null);
INSERT INTO `notifications` VALUES ('120', '33', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃33', 'Ketty perry  has changed the Job status from Paused to In Process for Job #33', '2017-01-11 05:14:01', null);
INSERT INTO `notifications` VALUES ('121', '33', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃33', 'Ketty perry  has changed the Job status from In Process to Pause for Job #33', '2017-01-11 05:14:07', null);
INSERT INTO `notifications` VALUES ('122', '33', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃33', 'Ketty perry  has changed the Job status from Paused to In Process for Job #33', '2017-01-11 05:14:12', null);
INSERT INTO `notifications` VALUES ('123', '33', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃33', 'Ketty perry  has changed the Job status from In Process to Pause for Job #33', '2017-01-11 05:14:18', null);
INSERT INTO `notifications` VALUES ('124', '33', '4', '6', '1', '6', '0', '2', '0', '您的工作＃33已被管理员提出异议。', 'Your job #33  has been disputed by Admin.', '2017-01-11 05:14:18', null);
INSERT INTO `notifications` VALUES ('125', '34', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业34', 'Ketty perry has applied for the Job #34', '2017-01-11 05:21:45', null);
INSERT INTO `notifications` VALUES ('126', '34', '6', '4', '2', '1', '1', '1', '0', '您对工作＃34的提案已获批准。', 'Your proposal on job #34 has been approved.', '2017-01-11 05:21:59', null);
INSERT INTO `notifications` VALUES ('127', '34', '4', '6', '1', '1', '1', '1', '0', '您的工作＃34已被代理人Ketty perry在40 HKD接受。', 'Your job #34  has been accepted by agent Ketty perry at 40 HKD.', '2017-01-11 05:23:05', null);
INSERT INTO `notifications` VALUES ('128', '34', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃34', 'Ketty perry  has changed the Job status from In Process to Pause for Job #34', '2017-01-11 05:25:02', null);
INSERT INTO `notifications` VALUES ('129', '34', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃34', 'Ketty perry  has changed the Job status from Paused to In Process for Job #34', '2017-01-11 05:25:09', null);
INSERT INTO `notifications` VALUES ('130', '34', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃34', 'Ketty perry  has changed the Job status from In Process to Pause for Job #34', '2017-01-11 05:25:14', null);
INSERT INTO `notifications` VALUES ('131', '34', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃34', 'Ketty perry  has changed the Job status from Paused to In Process for Job #34', '2017-01-11 05:25:19', null);
INSERT INTO `notifications` VALUES ('132', '34', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃34', 'Ketty perry  has changed the Job status from In Process to Pause for Job #34', '2017-01-11 05:25:24', null);
INSERT INTO `notifications` VALUES ('133', '34', '4', '6', '1', '6', '0', '2', '0', '您的工作＃34已被管理员提出异议。', 'Your job #34  has been disputed by Admin.', '2017-01-11 05:25:25', null);
INSERT INTO `notifications` VALUES ('134', '35', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业35', 'Ketty perry has applied for the Job #35', '2017-01-11 05:28:56', null);
INSERT INTO `notifications` VALUES ('135', '35', '6', '4', '2', '1', '1', '1', '0', '您对工作＃35的提案已获批准。', 'Your proposal on job #35 has been approved.', '2017-01-11 05:29:14', null);
INSERT INTO `notifications` VALUES ('136', '35', '4', '6', '1', '1', '1', '1', '0', '您的工作＃35已被代理人Ketty perry在30 HKD接受。', 'Your job #35  has been accepted by agent Ketty perry at 30 HKD.', '2017-01-11 05:29:47', null);
INSERT INTO `notifications` VALUES ('137', '35', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃35', 'Ketty perry  has changed the Job status from In Process to Pause for Job #35', '2017-01-11 05:30:04', null);
INSERT INTO `notifications` VALUES ('138', '35', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃35', 'Ketty perry  has changed the Job status from Paused to In Process for Job #35', '2017-01-11 05:30:10', null);
INSERT INTO `notifications` VALUES ('139', '35', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃35', 'Ketty perry  has changed the Job status from In Process to Pause for Job #35', '2017-01-11 05:30:14', null);
INSERT INTO `notifications` VALUES ('140', '35', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃35', 'Ketty perry  has changed the Job status from Paused to In Process for Job #35', '2017-01-11 05:30:18', null);
INSERT INTO `notifications` VALUES ('141', '35', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃35', 'Ketty perry  has changed the Job status from In Process to Pause for Job #35', '2017-01-11 05:30:22', null);
INSERT INTO `notifications` VALUES ('142', '35', '4', '6', '1', '6', '0', '2', '0', '您的工作＃35已被管理员提出异议。', 'Your job #35  has been disputed by Admin.', '2017-01-11 05:30:23', null);
INSERT INTO `notifications` VALUES ('143', '37', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业37', 'Ketty perry has applied for the Job #37', '2017-01-11 05:36:38', null);
INSERT INTO `notifications` VALUES ('144', '37', '6', '4', '2', '1', '1', '1', '0', '您对工作＃37的提案已获批准。', 'Your proposal on job #37 has been approved.', '2017-01-11 05:36:57', null);
INSERT INTO `notifications` VALUES ('145', '37', '4', '6', '1', '1', '1', '1', '0', '您的工作＃37已被代理人Ketty perry在60 HKD接受。', 'Your job #37  has been accepted by agent Ketty perry at 60 HKD.', '2017-01-11 05:37:28', null);
INSERT INTO `notifications` VALUES ('146', '37', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃37', 'Ketty perry  has changed the Job status from In Process to Pause for Job #37', '2017-01-11 05:37:44', null);
INSERT INTO `notifications` VALUES ('147', '37', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃37', 'Ketty perry  has changed the Job status from Paused to In Process for Job #37', '2017-01-11 05:37:49', null);
INSERT INTO `notifications` VALUES ('148', '37', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃37', 'Ketty perry  has changed the Job status from In Process to Pause for Job #37', '2017-01-11 05:37:53', null);
INSERT INTO `notifications` VALUES ('149', '37', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃37', 'Ketty perry  has changed the Job status from Paused to In Process for Job #37', '2017-01-11 05:37:59', null);
INSERT INTO `notifications` VALUES ('150', '37', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃37', 'Ketty perry  has changed the Job status from In Process to Pause for Job #37', '2017-01-11 05:38:03', null);
INSERT INTO `notifications` VALUES ('151', '37', '4', '6', '1', '6', '0', '2', '0', '您的工作＃37已被管理员提出异议。', 'Your job #37  has been disputed by Admin.', '2017-01-11 05:38:04', null);
INSERT INTO `notifications` VALUES ('152', '38', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业38', 'Ketty perry has applied for the Job #38', '2017-01-11 05:42:37', null);
INSERT INTO `notifications` VALUES ('153', '38', '6', '4', '2', '1', '1', '1', '0', '您对工作＃38的提案已获批准。', 'Your proposal on job #38 has been approved.', '2017-01-11 05:43:34', null);
INSERT INTO `notifications` VALUES ('154', '38', '4', '6', '1', '1', '1', '1', '0', '您的工作＃38已被代理人Ketty perry在10 HKD接受。', 'Your job #38  has been accepted by agent Ketty perry at 10 HKD.', '2017-01-11 05:44:09', null);
INSERT INTO `notifications` VALUES ('155', '38', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃38', 'Ketty perry  has changed the Job status from In Process to Pause for Job #38', '2017-01-11 05:44:24', null);
INSERT INTO `notifications` VALUES ('156', '38', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃38', 'Ketty perry  has changed the Job status from Paused to In Process for Job #38', '2017-01-11 05:44:33', null);
INSERT INTO `notifications` VALUES ('157', '38', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃38', 'Ketty perry  has changed the Job status from In Process to Pause for Job #38', '2017-01-11 05:44:38', null);
INSERT INTO `notifications` VALUES ('158', '38', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃38', 'Ketty perry  has changed the Job status from Paused to In Process for Job #38', '2017-01-11 05:44:42', null);
INSERT INTO `notifications` VALUES ('159', '38', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃38', 'Ketty perry  has changed the Job status from In Process to Pause for Job #38', '2017-01-11 05:44:47', null);
INSERT INTO `notifications` VALUES ('160', '38', '4', '6', '1', '6', '0', '2', '0', '您的工作＃38已被管理员提出异议。', 'Your job #38  has been disputed by Admin.', '2017-01-11 05:44:48', null);
INSERT INTO `notifications` VALUES ('161', '39', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业39', 'Ketty perry has applied for the Job #39', '2017-01-11 05:59:06', null);
INSERT INTO `notifications` VALUES ('162', '39', '6', '4', '2', '1', '1', '1', '0', '您对工作＃39的提案已获批准。', 'Your proposal on job #39 has been approved.', '2017-01-11 05:59:20', null);
INSERT INTO `notifications` VALUES ('163', '39', '4', '6', '1', '1', '1', '1', '0', '您的工作＃39已被代理人Ketty perry在60 HKD接受。', 'Your job #39  has been accepted by agent Ketty perry at 60 HKD.', '2017-01-11 05:59:56', null);
INSERT INTO `notifications` VALUES ('164', '39', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃39', 'Ketty perry  has changed the Job status from In Process to Pause for Job #39', '2017-01-11 06:01:08', null);
INSERT INTO `notifications` VALUES ('165', '39', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃39', 'Ketty perry  has changed the Job status from Paused to In Process for Job #39', '2017-01-11 06:01:16', null);
INSERT INTO `notifications` VALUES ('166', '39', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃39', 'Ketty perry  has changed the Job status from In Process to Pause for Job #39', '2017-01-11 06:01:20', null);
INSERT INTO `notifications` VALUES ('167', '39', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃39', 'Ketty perry  has changed the Job status from Paused to In Process for Job #39', '2017-01-11 06:01:23', null);
INSERT INTO `notifications` VALUES ('168', '39', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃39', 'Ketty perry  has changed the Job status from In Process to Pause for Job #39', '2017-01-11 06:01:27', null);
INSERT INTO `notifications` VALUES ('169', '39', '4', '6', '1', '6', '0', '2', '0', '您的工作＃39已被管理员提出异议。', 'Your job #39  has been disputed by Admin.', '2017-01-11 06:01:28', null);
INSERT INTO `notifications` VALUES ('170', '40', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业40', 'Ketty perry has applied for the Job #40', '2017-01-11 06:19:29', null);
INSERT INTO `notifications` VALUES ('171', '40', '6', '4', '2', '1', '1', '1', '0', '您对工作＃40的提案已获批准。', 'Your proposal on job #40 has been approved.', '2017-01-11 06:19:41', null);
INSERT INTO `notifications` VALUES ('172', '40', '4', '6', '1', '1', '1', '1', '0', '您的工作＃40已被代理人Ketty perry在60 HKD接受。', 'Your job #40  has been accepted by agent Ketty perry at 60 HKD.', '2017-01-11 06:20:05', null);
INSERT INTO `notifications` VALUES ('173', '40', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃40', 'Ketty perry  has changed the Job status from In Process to Pause for Job #40', '2017-01-11 06:20:48', null);
INSERT INTO `notifications` VALUES ('174', '40', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃40', 'Ketty perry  has changed the Job status from Paused to In Process for Job #40', '2017-01-11 06:20:56', null);
INSERT INTO `notifications` VALUES ('175', '40', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃40', 'Ketty perry  has changed the Job status from In Process to Pause for Job #40', '2017-01-11 06:21:00', null);
INSERT INTO `notifications` VALUES ('176', '40', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃40', 'Ketty perry  has changed the Job status from Paused to In Process for Job #40', '2017-01-11 06:21:03', null);
INSERT INTO `notifications` VALUES ('177', '40', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃40', 'Ketty perry  has changed the Job status from In Process to Pause for Job #40', '2017-01-11 06:21:06', null);
INSERT INTO `notifications` VALUES ('178', '40', '4', '6', '1', '6', '0', '2', '0', '您的工作＃40已被管理员提出异议。', 'Your job #40  has been disputed by Admin.', '2017-01-11 06:21:07', null);
INSERT INTO `notifications` VALUES ('179', '40', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业40', 'Ketty perry has applied for the Job #40', '2017-01-11 06:31:33', null);
INSERT INTO `notifications` VALUES ('180', '41', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业41', 'Ketty perry has applied for the Job #41', '2017-01-11 06:31:51', null);
INSERT INTO `notifications` VALUES ('181', '41', '6', '4', '2', '1', '1', '1', '0', '您对工作＃41的提案已获批准。', 'Your proposal on job #41 has been approved.', '2017-01-11 06:32:19', null);
INSERT INTO `notifications` VALUES ('182', '41', '4', '6', '1', '1', '1', '1', '0', '您的工作＃41已被代理人Ketty perry在100 HKD接受。', 'Your job #41  has been accepted by agent Ketty perry at 100 HKD.', '2017-01-11 06:33:00', null);
INSERT INTO `notifications` VALUES ('183', '41', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃41', 'Ketty perry  has changed the Job status from In Process to Pause for Job #41', '2017-01-11 06:34:00', null);
INSERT INTO `notifications` VALUES ('184', '41', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃41', 'Ketty perry  has changed the Job status from Paused to In Process for Job #41', '2017-01-11 06:34:10', null);
INSERT INTO `notifications` VALUES ('185', '41', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃41', 'Ketty perry  has changed the Job status from In Process to Pause for Job #41', '2017-01-11 06:34:17', null);
INSERT INTO `notifications` VALUES ('186', '41', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃41', 'Ketty perry  has changed the Job status from Paused to In Process for Job #41', '2017-01-11 06:34:21', null);
INSERT INTO `notifications` VALUES ('187', '41', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃41', 'Ketty perry  has changed the Job status from In Process to Pause for Job #41', '2017-01-11 06:34:25', null);
INSERT INTO `notifications` VALUES ('188', '41', '4', '6', '1', '6', '0', '2', '0', '您的工作＃41已被管理员提出异议。', 'Your job #41  has been disputed by Admin.', '2017-01-11 06:34:26', null);
INSERT INTO `notifications` VALUES ('189', '42', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业42', 'Ketty perry has applied for the Job #42', '2017-01-11 06:39:28', null);
INSERT INTO `notifications` VALUES ('190', '42', '6', '4', '2', '1', '1', '1', '0', '您对工作＃42的提案已获批准。', 'Your proposal on job #42 has been approved.', '2017-01-11 06:39:41', null);
INSERT INTO `notifications` VALUES ('191', '42', '4', '6', '1', '1', '1', '1', '0', '您的工作＃42已被代理人Ketty perry在120 HKD接受。', 'Your job #42  has been accepted by agent Ketty perry at 120 HKD.', '2017-01-11 06:40:11', null);
INSERT INTO `notifications` VALUES ('192', '42', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃42', 'Ketty perry  has changed the Job status from In Process to Pause for Job #42', '2017-01-11 06:40:23', null);
INSERT INTO `notifications` VALUES ('193', '42', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃42', 'Ketty perry  has changed the Job status from Paused to In Process for Job #42', '2017-01-11 06:40:36', null);
INSERT INTO `notifications` VALUES ('194', '42', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃42', 'Ketty perry  has changed the Job status from In Process to Pause for Job #42', '2017-01-11 06:40:40', null);
INSERT INTO `notifications` VALUES ('195', '42', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃42', 'Ketty perry  has changed the Job status from Paused to In Process for Job #42', '2017-01-11 06:40:43', null);
INSERT INTO `notifications` VALUES ('196', '42', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃42', 'Ketty perry  has changed the Job status from In Process to Pause for Job #42', '2017-01-11 06:40:47', null);
INSERT INTO `notifications` VALUES ('197', '42', '4', '6', '1', '6', '0', '2', '0', '您的工作＃42已被管理员提出异议。', 'Your job #42  has been disputed by Admin.', '2017-01-11 06:40:48', null);
INSERT INTO `notifications` VALUES ('198', '43', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业43', 'Ketty perry has applied for the Job #43', '2017-01-11 07:26:18', null);
INSERT INTO `notifications` VALUES ('199', '43', '6', '4', '2', '1', '1', '1', '0', '您对工作＃43的提案已获批准。', 'Your proposal on job #43 has been approved.', '2017-01-11 07:26:30', null);
INSERT INTO `notifications` VALUES ('200', '43', '4', '6', '1', '1', '1', '1', '0', '您的工作＃43已被代理人Ketty perry在130 HKD接受。', 'Your job #43  has been accepted by agent Ketty perry at 130 HKD.', '2017-01-11 07:27:05', null);
INSERT INTO `notifications` VALUES ('201', '43', '4', '6', '1', '1', '1', '1', '0', '您的工作＃43已被代理人Ketty perry在130 HKD接受。', 'Your job #43  has been accepted by agent Ketty perry at 130 HKD.', '2017-01-11 07:28:48', null);
INSERT INTO `notifications` VALUES ('202', '43', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃43', 'Ketty perry  has changed the Job status from In Process to Pause for Job #43', '2017-01-11 07:28:58', null);
INSERT INTO `notifications` VALUES ('203', '43', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃43', 'Ketty perry  has changed the Job status from Paused to In Process for Job #43', '2017-01-11 07:29:04', null);
INSERT INTO `notifications` VALUES ('204', '43', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃43', 'Ketty perry  has changed the Job status from In Process to Pause for Job #43', '2017-01-11 07:29:15', null);
INSERT INTO `notifications` VALUES ('205', '43', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃43', 'Ketty perry  has changed the Job status from Paused to In Process for Job #43', '2017-01-11 07:29:19', null);
INSERT INTO `notifications` VALUES ('206', '43', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃43', 'Ketty perry  has changed the Job status from In Process to Pause for Job #43', '2017-01-11 07:29:24', null);
INSERT INTO `notifications` VALUES ('207', '43', '4', '6', '1', '6', '0', '2', '0', '您的工作＃43已被管理员提出异议。', 'Your job #43  has been disputed by Admin.', '2017-01-11 07:29:25', null);
INSERT INTO `notifications` VALUES ('208', '43', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃43', 'Ketty perry  has changed the Job status from In Process to Pause for Job #43', '2017-01-11 07:32:19', null);
INSERT INTO `notifications` VALUES ('209', '43', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃43', 'Ketty perry  has changed the Job status from Paused to In Process for Job #43', '2017-01-11 07:32:25', null);
INSERT INTO `notifications` VALUES ('210', '43', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃43', 'Ketty perry  has changed the Job status from In Process to Pause for Job #43', '2017-01-11 07:35:17', null);
INSERT INTO `notifications` VALUES ('211', '43', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃43', 'Ketty perry  has changed the Job status from Paused to In Process for Job #43', '2017-01-11 07:35:21', null);
INSERT INTO `notifications` VALUES ('212', '43', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃43', 'Ketty perry  has changed the Job status from In Process to Pause for Job #43', '2017-01-11 07:35:26', null);
INSERT INTO `notifications` VALUES ('213', '43', '4', '6', '1', '6', '0', '2', '0', '您的工作＃43已被管理员提出异议。', 'Your job #43  has been disputed by Admin.', '2017-01-11 07:35:26', null);
INSERT INTO `notifications` VALUES ('214', '44', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业44', 'Ketty perry has applied for the Job #44', '2017-01-11 07:42:54', null);
INSERT INTO `notifications` VALUES ('215', '44', '6', '4', '2', '1', '1', '1', '0', '您对工作＃44的提案已获批准。', 'Your proposal on job #44 has been approved.', '2017-01-11 07:43:12', null);
INSERT INTO `notifications` VALUES ('216', '43', '4', '6', '1', '1', '1', '1', '0', '您的工作＃43已被代理人Ketty perry在100 HKD接受。', 'Your job #43  has been accepted by agent Ketty perry at 100 HKD.', '2017-01-11 07:43:35', null);
INSERT INTO `notifications` VALUES ('217', '44', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃44', 'Ketty perry  has changed the Job status from In Process to Pause for Job #44', '2017-01-11 07:44:03', null);
INSERT INTO `notifications` VALUES ('218', '44', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃44', 'Ketty perry  has changed the Job status from In Process to Pause for Job #44', '2017-01-11 07:44:11', null);
INSERT INTO `notifications` VALUES ('219', '44', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃44', 'Ketty perry  has changed the Job status from In Process to Pause for Job #44', '2017-01-11 07:44:17', null);
INSERT INTO `notifications` VALUES ('220', '44', '4', '6', '1', '6', '0', '2', '0', '您的工作＃44已被管理员提出异议。', 'Your job #44  has been disputed by Admin.', '2017-01-11 07:44:21', null);
INSERT INTO `notifications` VALUES ('221', '45', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业45', 'Ketty perry has applied for the Job #45', '2017-01-11 07:48:17', null);
INSERT INTO `notifications` VALUES ('222', '45', '6', '4', '2', '1', '1', '1', '0', '您对工作＃45的提案已获批准。', 'Your proposal on job #45 has been approved.', '2017-01-11 07:48:27', null);
INSERT INTO `notifications` VALUES ('223', '45', '4', '6', '1', '1', '1', '1', '0', '您的工作＃45已被代理人Ketty perry在50 HKD接受。', 'Your job #45  has been accepted by agent Ketty perry at 50 HKD.', '2017-01-11 07:49:00', null);
INSERT INTO `notifications` VALUES ('224', '45', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃45', 'Ketty perry  has changed the Job status from In Process to Pause for Job #45', '2017-01-11 07:49:24', null);
INSERT INTO `notifications` VALUES ('225', '45', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃45', 'Ketty perry  has changed the Job status from Paused to In Process for Job #45', '2017-01-11 07:49:32', null);
INSERT INTO `notifications` VALUES ('226', '45', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃45', 'Ketty perry  has changed the Job status from In Process to Pause for Job #45', '2017-01-11 07:49:35', null);
INSERT INTO `notifications` VALUES ('227', '45', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃45', 'Ketty perry  has changed the Job status from Paused to In Process for Job #45', '2017-01-11 07:49:38', null);
INSERT INTO `notifications` VALUES ('228', '45', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃45', 'Ketty perry  has changed the Job status from In Process to Pause for Job #45', '2017-01-11 07:49:42', null);
INSERT INTO `notifications` VALUES ('229', '45', '4', '6', '1', '6', '0', '2', '0', '您的工作＃45已被管理员提出异议。', 'Your job #45  has been disputed by Admin.', '2017-01-11 07:49:43', null);
INSERT INTO `notifications` VALUES ('230', '46', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业46', 'Ketty perry has applied for the Job #46', '2017-01-11 07:52:55', null);
INSERT INTO `notifications` VALUES ('231', '46', '6', '4', '2', '1', '1', '1', '0', '您对工作＃46的提案已获批准。', 'Your proposal on job #46 has been approved.', '2017-01-11 07:53:22', null);
INSERT INTO `notifications` VALUES ('232', '46', '4', '6', '1', '1', '1', '1', '0', '您的工作＃46已被代理人Ketty perry在200 HKD接受。', 'Your job #46  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-11 07:53:43', null);
INSERT INTO `notifications` VALUES ('233', '46', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃46', 'Ketty perry  has changed the Job status from In Process to Pause for Job #46', '2017-01-11 07:53:48', null);
INSERT INTO `notifications` VALUES ('234', '46', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃46', 'Ketty perry  has changed the Job status from Paused to In Process for Job #46', '2017-01-11 07:53:52', null);
INSERT INTO `notifications` VALUES ('235', '46', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃46', 'Ketty perry  has changed the Job status from In Process to Pause for Job #46', '2017-01-11 07:53:58', null);
INSERT INTO `notifications` VALUES ('236', '46', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃46', 'Ketty perry  has changed the Job status from Paused to In Process for Job #46', '2017-01-11 07:54:02', null);
INSERT INTO `notifications` VALUES ('237', '46', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃46', 'Ketty perry  has changed the Job status from In Process to Pause for Job #46', '2017-01-11 07:54:08', null);
INSERT INTO `notifications` VALUES ('238', '46', '4', '6', '1', '6', '0', '2', '0', '您的工作＃46已被管理员提出异议。', 'Your job #46  has been disputed by Admin.', '2017-01-11 07:54:09', null);
INSERT INTO `notifications` VALUES ('239', '47', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业47', 'Ketty perry has applied for the Job #47', '2017-01-11 08:48:36', null);
INSERT INTO `notifications` VALUES ('240', '47', '6', '4', '2', '1', '1', '1', '0', '您对工作＃47的提案已获批准。', 'Your proposal on job #47 has been approved.', '2017-01-11 08:48:46', null);
INSERT INTO `notifications` VALUES ('241', '47', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃47', 'Ketty perry  has changed the Job status from In Process to Pause for Job #47', '2017-01-11 08:49:13', null);
INSERT INTO `notifications` VALUES ('242', '47', '4', '6', '1', '1', '1', '1', '0', '您的工作＃47已被代理人Ketty perry在200 HKD接受。', 'Your job #47  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-11 08:49:21', null);
INSERT INTO `notifications` VALUES ('243', '47', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃47', 'Ketty perry  has changed the Job status from In Process to Pause for Job #47', '2017-01-11 08:49:24', null);
INSERT INTO `notifications` VALUES ('244', '47', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃47', 'Ketty perry  has changed the Job status from Paused to In Process for Job #47', '2017-01-11 08:49:32', null);
INSERT INTO `notifications` VALUES ('245', '47', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃47', 'Ketty perry  has changed the Job status from In Process to Pause for Job #47', '2017-01-11 08:49:38', null);
INSERT INTO `notifications` VALUES ('246', '47', '4', '6', '1', '6', '0', '2', '0', '您的工作＃47已被管理员提出异议。', 'Your job #47  has been disputed by Admin.', '2017-01-11 08:49:39', null);
INSERT INTO `notifications` VALUES ('247', '48', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业48', 'Ketty perry has applied for the Job #48', '2017-01-11 08:58:24', null);
INSERT INTO `notifications` VALUES ('248', '48', '6', '4', '2', '1', '1', '1', '0', '您对工作＃48的提案已获批准。', 'Your proposal on job #48 has been approved.', '2017-01-11 08:58:31', null);
INSERT INTO `notifications` VALUES ('249', '48', '4', '6', '1', '1', '1', '1', '0', '您的工作＃48已被代理人Ketty perry在200 HKD接受。', 'Your job #48  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-11 08:58:59', null);
INSERT INTO `notifications` VALUES ('250', '48', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃48', 'Ketty perry  has changed the Job status from In Process to Pause for Job #48', '2017-01-11 09:01:16', null);
INSERT INTO `notifications` VALUES ('251', '48', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃48', 'Ketty perry  has changed the Job status from Paused to In Process for Job #48', '2017-01-11 09:01:20', null);
INSERT INTO `notifications` VALUES ('252', '48', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃48', 'Ketty perry  has changed the Job status from In Process to Pause for Job #48', '2017-01-11 09:01:26', null);
INSERT INTO `notifications` VALUES ('253', '48', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃48', 'Ketty perry  has changed the Job status from Paused to In Process for Job #48', '2017-01-11 09:01:30', null);
INSERT INTO `notifications` VALUES ('254', '48', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃48', 'Ketty perry  has changed the Job status from In Process to Pause for Job #48', '2017-01-11 09:01:42', null);
INSERT INTO `notifications` VALUES ('255', '48', '4', '6', '1', '6', '0', '2', '0', '您的工作＃48已被管理员提出异议。', 'Your job #48  has been disputed by Admin.', '2017-01-11 09:01:43', null);
INSERT INTO `notifications` VALUES ('256', '49', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业49', 'Ketty perry has applied for the Job #49', '2017-01-11 09:07:52', null);
INSERT INTO `notifications` VALUES ('257', '49', '6', '4', '2', '1', '1', '1', '0', '您对工作＃49的提案已获批准。', 'Your proposal on job #49 has been approved.', '2017-01-11 09:07:59', null);
INSERT INTO `notifications` VALUES ('258', '49', '4', '6', '1', '1', '1', '1', '0', '您的工作＃49已被代理人Ketty perry在200 HKD接受。', 'Your job #49  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-11 09:08:24', null);
INSERT INTO `notifications` VALUES ('259', '49', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃49', 'Ketty perry  has changed the Job status from In Process to Pause for Job #49', '2017-01-11 09:08:29', null);
INSERT INTO `notifications` VALUES ('260', '49', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃49', 'Ketty perry  has changed the Job status from Paused to In Process for Job #49', '2017-01-11 09:08:33', null);
INSERT INTO `notifications` VALUES ('261', '49', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃49', 'Ketty perry  has changed the Job status from In Process to Pause for Job #49', '2017-01-11 09:08:37', null);
INSERT INTO `notifications` VALUES ('262', '49', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃49', 'Ketty perry  has changed the Job status from Paused to In Process for Job #49', '2017-01-11 09:08:41', null);
INSERT INTO `notifications` VALUES ('263', '49', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃49', 'Ketty perry  has changed the Job status from In Process to Pause for Job #49', '2017-01-11 09:08:45', null);
INSERT INTO `notifications` VALUES ('264', '49', '4', '6', '1', '6', '0', '2', '0', '您的工作＃49已被管理员提出异议。', 'Your job #49  has been disputed by Admin.', '2017-01-11 09:08:46', null);
INSERT INTO `notifications` VALUES ('265', '50', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业50', 'Ketty perry has applied for the Job #50', '2017-01-11 09:54:07', null);
INSERT INTO `notifications` VALUES ('266', '50', '6', '4', '2', '1', '1', '1', '0', '您对工作＃50的提案已获批准。', 'Your proposal on job #50 has been approved.', '2017-01-11 09:55:01', null);
INSERT INTO `notifications` VALUES ('267', '50', '4', '6', '1', '1', '1', '1', '0', '您的工作＃50已被代理人Ketty perry在200 HKD接受。', 'Your job #50  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-11 09:55:24', null);
INSERT INTO `notifications` VALUES ('268', '50', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃50', 'Ketty perry  has changed the Job status from In Process to Pause for Job #50', '2017-01-11 09:55:30', null);
INSERT INTO `notifications` VALUES ('269', '50', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃50', 'Ketty perry  has changed the Job status from Paused to In Process for Job #50', '2017-01-11 09:55:36', null);
INSERT INTO `notifications` VALUES ('270', '50', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃50', 'Ketty perry  has changed the Job status from In Process to Pause for Job #50', '2017-01-11 09:55:39', null);
INSERT INTO `notifications` VALUES ('271', '50', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃50', 'Ketty perry  has changed the Job status from Paused to In Process for Job #50', '2017-01-11 09:55:47', null);
INSERT INTO `notifications` VALUES ('272', '50', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃50', 'Ketty perry  has changed the Job status from In Process to Pause for Job #50', '2017-01-11 09:55:52', null);
INSERT INTO `notifications` VALUES ('273', '50', '4', '6', '1', '6', '0', '2', '0', '您的工作＃50已被管理员提出异议。', 'Your job #50  has been disputed by Admin.', '2017-01-11 09:55:53', null);
INSERT INTO `notifications` VALUES ('274', '51', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业51', 'Ketty perry has applied for the Job #51', '2017-01-11 09:58:49', null);
INSERT INTO `notifications` VALUES ('275', '51', '6', '4', '2', '1', '1', '1', '0', '您对工作＃51的提案已获批准。', 'Your proposal on job #51 has been approved.', '2017-01-11 09:58:57', null);
INSERT INTO `notifications` VALUES ('276', '51', '4', '6', '1', '1', '1', '1', '0', '您的工作＃51已被代理人Ketty perry在200 HKD接受。', 'Your job #51  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-11 09:59:23', null);
INSERT INTO `notifications` VALUES ('277', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 09:59:30', null);
INSERT INTO `notifications` VALUES ('278', '51', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃51', 'Ketty perry  has changed the Job status from Paused to In Process for Job #51', '2017-01-11 09:59:34', null);
INSERT INTO `notifications` VALUES ('279', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 09:59:38', null);
INSERT INTO `notifications` VALUES ('280', '51', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃51', 'Ketty perry  has changed the Job status from Paused to In Process for Job #51', '2017-01-11 09:59:42', null);
INSERT INTO `notifications` VALUES ('281', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 09:59:46', null);
INSERT INTO `notifications` VALUES ('282', '51', '4', '6', '1', '6', '0', '2', '0', '您的工作＃51已被管理员提出异议。', 'Your job #51  has been disputed by Admin.', '2017-01-11 09:59:47', null);
INSERT INTO `notifications` VALUES ('283', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 10:02:32', null);
INSERT INTO `notifications` VALUES ('284', '51', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃51', 'Ketty perry  has changed the Job status from Paused to In Process for Job #51', '2017-01-11 10:02:40', null);
INSERT INTO `notifications` VALUES ('285', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 10:02:44', null);
INSERT INTO `notifications` VALUES ('286', '51', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃51', 'Ketty perry  has changed the Job status from Paused to In Process for Job #51', '2017-01-11 10:02:47', null);
INSERT INTO `notifications` VALUES ('287', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 10:02:50', null);
INSERT INTO `notifications` VALUES ('288', '51', '4', '6', '1', '6', '0', '2', '0', '您的工作＃51已被管理员提出异议。', 'Your job #51  has been disputed by Admin.', '2017-01-11 10:02:51', null);
INSERT INTO `notifications` VALUES ('289', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 10:04:40', null);
INSERT INTO `notifications` VALUES ('290', '51', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃51', 'Ketty perry  has changed the Job status from Paused to In Process for Job #51', '2017-01-11 10:04:45', null);
INSERT INTO `notifications` VALUES ('291', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 10:04:48', null);
INSERT INTO `notifications` VALUES ('292', '51', '4', '6', '1', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃51', 'Ketty perry  has changed the Job status from Paused to In Process for Job #51', '2017-01-11 10:04:53', null);
INSERT INTO `notifications` VALUES ('293', '51', '4', '6', '1', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃51', 'Ketty perry  has changed the Job status from In Process to Pause for Job #51', '2017-01-11 10:04:56', null);
INSERT INTO `notifications` VALUES ('294', '51', '4', '6', '1', '6', '0', '2', '0', '您的工作＃51已被管理员提出异议。', 'Your job #51  has been disputed by Admin.', '2017-01-11 10:04:57', null);
INSERT INTO `notifications` VALUES ('295', '52', '6', '4', '1', '2', '0', '2', '0', '用户已申请作业52', 'Johanty Rodge has applied for the Job #52', '2017-01-11 10:16:24', null);
INSERT INTO `notifications` VALUES ('296', '52', '6', '6', '2', '1', '1', '1', '0', '您对工作＃52的提案已获批准。', 'Your proposal on job #52 has been approved.', '2017-01-11 10:16:41', null);
INSERT INTO `notifications` VALUES ('297', '52', '4', '6', '2', '1', '1', '1', '0', '您对工作＃52的提案已获批准。', 'Your proposal on job #52 has been approved.', '2017-01-11 10:16:47', null);
INSERT INTO `notifications` VALUES ('298', '52', '4', '4', '1', '1', '1', '1', '0', '您的工作＃52已被代理人Johanty Rodge在200 HKD接受。', 'Your job #52  has been accepted by agent Johanty Rodge at 200 HKD.', '2017-01-11 10:17:25', null);
INSERT INTO `notifications` VALUES ('299', '52', '6', '6', '2', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃52', 'Ketty perry  has changed the Job status from In Process to Pause for Job #52', '2017-01-11 10:17:39', null);
INSERT INTO `notifications` VALUES ('300', '52', '6', '6', '2', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃52', 'Ketty perry  has changed the Job status from Paused to In Process for Job #52', '2017-01-11 10:17:43', null);
INSERT INTO `notifications` VALUES ('301', '52', '6', '6', '2', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃52', 'Ketty perry  has changed the Job status from In Process to Pause for Job #52', '2017-01-11 10:17:48', null);
INSERT INTO `notifications` VALUES ('302', '52', '6', '6', '2', '3', '0', '2', '0', 'Ketty perry 已将作业状态从“已暂停”更改为“正在处理”作业＃52', 'Ketty perry  has changed the Job status from Paused to In Process for Job #52', '2017-01-11 10:17:53', null);
INSERT INTO `notifications` VALUES ('303', '52', '6', '6', '2', '5', '0', '2', '0', 'Ketty perry 已将作业状态从进程更改为暂停作业＃52', 'Ketty perry  has changed the Job status from In Process to Pause for Job #52', '2017-01-11 10:18:23', null);
INSERT INTO `notifications` VALUES ('304', '52', '6', '6', '2', '6', '0', '2', '0', '您的工作＃52已被管理员提出异议。', 'Your job #52  has been disputed by Admin.', '2017-01-11 10:18:23', null);
INSERT INTO `notifications` VALUES ('305', '53', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业53', 'Ketty perry has applied for the Job #53', '2017-01-13 11:21:05', null);
INSERT INTO `notifications` VALUES ('306', '53', '6', '4', '2', '1', '1', '1', '0', '您对工作＃53的提案已获批准。', 'Your proposal on job #53 has been approved.', '2017-01-13 11:22:28', null);
INSERT INTO `notifications` VALUES ('307', '53', '4', '6', '1', '1', '1', '1', '0', '您的工作＃53已被代理人Ketty perry在50 HKD接受。', 'Your job #53  has been accepted by agent Ketty perry at 50 HKD.', '2017-01-13 11:23:45', null);
INSERT INTO `notifications` VALUES ('308', '54', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业54', 'Ketty perry has applied for the Job #54', '2017-01-16 07:29:57', null);
INSERT INTO `notifications` VALUES ('309', '54', '6', '4', '2', '1', '1', '1', '0', '您对工作＃54的提案已获批准。', 'Your proposal on job #54 has been approved.', '2017-01-16 07:33:09', null);
INSERT INTO `notifications` VALUES ('310', '54', '4', '6', '1', '1', '1', '1', '0', '您的工作＃54已被代理人Ketty perry在12.33 HKD接受。', 'Your job #54  has been accepted by agent Ketty perry at 12.33 HKD.', '2017-01-16 07:33:55', null);
INSERT INTO `notifications` VALUES ('311', '54', '4', '6', '1', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃54', 'The Job Status has been changed from In Process to Completed for Job #54', '2017-01-16 07:35:21', null);
INSERT INTO `notifications` VALUES ('312', '56', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业56', 'Ketty perry has applied for the Job #56', '2017-01-19 10:23:45', null);
INSERT INTO `notifications` VALUES ('313', '56', '4', '6', '1', '1', '1', '1', '0', '您的工作＃56已被代理人Ketty perry在200 HKD接受。', 'Your job #56  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 10:26:30', null);
INSERT INTO `notifications` VALUES ('314', '56', '4', '6', '1', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃56', 'The Job Status has been changed from In Process to Completed for Job #56', '2017-01-19 10:30:22', null);
INSERT INTO `notifications` VALUES ('315', '57', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业57', 'Ketty perry has applied for the Job #57', '2017-01-19 10:31:21', null);
INSERT INTO `notifications` VALUES ('316', '57', '6', '4', '2', '1', '1', '1', '0', '您对工作＃57的提案已获批准。', 'Your proposal on job #57 has been approved.', '2017-01-19 10:32:34', null);
INSERT INTO `notifications` VALUES ('317', '57', '4', '6', '1', '1', '1', '1', '0', '您的工作＃57已被代理人Ketty perry在200 HKD接受。', 'Your job #57  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 10:33:54', null);
INSERT INTO `notifications` VALUES ('318', '57', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃57', 'The Job Status has been changed from In Process to Completed for Job #57', '2017-01-19 10:34:49', null);
INSERT INTO `notifications` VALUES ('319', '58', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业58', 'Ketty perry has applied for the Job #58', '2017-01-19 10:42:49', null);
INSERT INTO `notifications` VALUES ('320', '58', '6', '4', '2', '1', '1', '1', '0', '您对工作＃58的提案已获批准。', 'Your proposal on job #58 has been approved.', '2017-01-19 10:42:58', null);
INSERT INTO `notifications` VALUES ('321', '58', '4', '6', '1', '1', '1', '1', '0', '您的工作＃58已被代理人Ketty perry在200 HKD接受。', 'Your job #58  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 10:43:32', null);
INSERT INTO `notifications` VALUES ('322', '58', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃58', 'The Job Status has been changed from In Process to Completed for Job #58', '2017-01-19 10:43:46', null);
INSERT INTO `notifications` VALUES ('323', '58', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃58', 'The Job Status has been changed from In Process to Completed for Job #58', '2017-01-19 10:45:18', null);
INSERT INTO `notifications` VALUES ('324', '58', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃58', 'The Job Status has been changed from In Process to Completed for Job #58', '2017-01-19 10:51:08', null);
INSERT INTO `notifications` VALUES ('325', '58', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃58', 'The Job Status has been changed from In Process to Completed for Job #58', '2017-01-19 10:56:26', null);
INSERT INTO `notifications` VALUES ('326', '58', '4', '6', '1', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃58', 'The Job Status has been changed from In Process to Completed for Job #58', '2017-01-19 10:59:39', null);
INSERT INTO `notifications` VALUES ('327', '59', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业59', 'Ketty perry has applied for the Job #59', '2017-01-19 11:01:01', null);
INSERT INTO `notifications` VALUES ('328', '59', '6', '4', '2', '1', '1', '1', '0', '您对工作＃59的提案已获批准。', 'Your proposal on job #59 has been approved.', '2017-01-19 11:01:15', null);
INSERT INTO `notifications` VALUES ('329', '59', '4', '6', '1', '1', '1', '1', '0', '您的工作＃59已被代理人Ketty perry在200 HKD接受。', 'Your job #59  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 11:01:41', null);
INSERT INTO `notifications` VALUES ('330', '59', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃59', 'The Job Status has been changed from In Process to Completed for Job #59', '2017-01-19 11:03:36', null);
INSERT INTO `notifications` VALUES ('331', '59', '4', '6', '1', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃59', 'The Job Status has been changed from In Process to Completed for Job #59', '2017-01-19 11:05:35', null);
INSERT INTO `notifications` VALUES ('332', '59', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业59', 'Ketty perry has applied for the Job #59', '2017-01-19 11:06:25', null);
INSERT INTO `notifications` VALUES ('333', '60', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业60', 'Ketty perry has applied for the Job #60', '2017-01-19 11:07:10', null);
INSERT INTO `notifications` VALUES ('334', '60', '6', '4', '2', '1', '1', '1', '0', '您对工作＃60的提案已获批准。', 'Your proposal on job #60 has been approved.', '2017-01-19 11:07:16', null);
INSERT INTO `notifications` VALUES ('335', '60', '4', '6', '1', '1', '1', '1', '0', '您的工作＃60已被代理人Ketty perry在150 HKD接受。', 'Your job #60  has been accepted by agent Ketty perry at 150 HKD.', '2017-01-19 11:07:44', null);
INSERT INTO `notifications` VALUES ('336', '60', '4', '6', '1', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃60', 'The Job Status has been changed from In Process to Completed for Job #60', '2017-01-19 11:08:52', null);
INSERT INTO `notifications` VALUES ('337', '61', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业61', 'Ketty perry has applied for the Job #61', '2017-01-19 11:11:51', null);
INSERT INTO `notifications` VALUES ('338', '61', '6', '4', '2', '1', '1', '1', '0', '您对工作＃61的提案已获批准。', 'Your proposal on job #61 has been approved.', '2017-01-19 11:11:58', null);
INSERT INTO `notifications` VALUES ('339', '61', '4', '6', '1', '1', '1', '1', '0', '您的工作＃61已被代理人Ketty perry在150 HKD接受。', 'Your job #61  has been accepted by agent Ketty perry at 150 HKD.', '2017-01-19 11:12:32', null);
INSERT INTO `notifications` VALUES ('340', '61', '6', '4', '2', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃61', 'The Job Status has been changed from In Process to Completed for Job #61', '2017-01-19 11:13:03', null);
INSERT INTO `notifications` VALUES ('341', '61', '4', '6', '1', '4', '0', '2', '0', '已将作业状态从进程更改为暂停作业＃61', 'The Job Status has been changed from In Process to Completed for Job #61', '2017-01-19 11:13:40', null);
INSERT INTO `notifications` VALUES ('342', '62', '1', '2', '1', '2', '0', '2', '0', '用户已申请作业62', 'Vasupujy Patel has applied for the Job #62', '2017-01-19 13:02:16', null);
INSERT INTO `notifications` VALUES ('343', '62', '1', '6', '1', '2', '0', '2', '0', '用户已申请作业62', 'Ketty perry has applied for the Job #62', '2017-01-19 13:03:16', null);
INSERT INTO `notifications` VALUES ('344', '62', '6', '4', '2', '1', '1', '1', '0', '您对工作＃62的提案已获批准。', 'Your proposal on job #62 has been approved.', '2017-01-19 13:03:37', null);
INSERT INTO `notifications` VALUES ('345', '62', '4', '6', '1', '1', '1', '1', '0', '您的工作＃62已被代理人Ketty perry在200 HKD接受。', 'Your job #62  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 13:11:24', null);
INSERT INTO `notifications` VALUES ('346', '62', '4', '6', '1', '1', '1', '1', '0', '您的工作＃62已被代理人Ketty perry在200 HKD接受。', 'Your job #62  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 13:17:01', null);
INSERT INTO `notifications` VALUES ('347', '64', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业64', 'Ketty perry has applied for the Job #64', '2017-01-19 13:19:45', null);
INSERT INTO `notifications` VALUES ('348', '64', '6', '4', '2', '1', '1', '1', '0', '您对工作＃64的提案已获批准。', 'Your proposal on job #64 has been approved.', '2017-01-19 13:19:53', null);
INSERT INTO `notifications` VALUES ('349', '64', '4', '6', '1', '1', '1', '1', '0', '您的工作＃64已被代理人Ketty perry在200 HKD接受。', 'Your job #64  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 13:21:23', null);
INSERT INTO `notifications` VALUES ('350', '64', '4', '6', '1', '1', '1', '1', '0', '您的工作＃64已被代理人Ketty perry在200 HKD接受。', 'Your job #64  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 13:22:05', null);
INSERT INTO `notifications` VALUES ('351', '64', '4', '6', '1', '1', '1', '1', '0', '您的工作＃64已被代理人Ketty perry在200 HKD接受。', 'Your job #64  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-19 13:29:43', null);
INSERT INTO `notifications` VALUES ('352', '65', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业65', 'Ketty perry has applied for the Job #65', '2017-01-20 07:13:56', null);
INSERT INTO `notifications` VALUES ('353', '65', '6', '4', '2', '1', '1', '1', '0', '您对工作＃65的提案已获批准。', 'Your proposal on job #65 has been approved.', '2017-01-20 07:14:07', null);
INSERT INTO `notifications` VALUES ('354', '65', '4', '6', '1', '1', '1', '1', '0', '您的工作＃65已被代理人Ketty perry在200 HKD接受。', 'Your job #65  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-20 07:20:30', null);
INSERT INTO `notifications` VALUES ('355', '66', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业66', 'Ketty perry has applied for the Job #66', '2017-01-20 07:33:51', null);
INSERT INTO `notifications` VALUES ('356', '66', '6', '4', '2', '1', '1', '1', '0', '您对工作＃66的提案已获批准。', 'Your proposal on job #66 has been approved.', '2017-01-20 07:34:04', null);
INSERT INTO `notifications` VALUES ('357', '66', '4', '6', '1', '1', '1', '1', '0', '您的工作＃66已被代理人Ketty perry在200 HKD接受。', 'Your job #66  has been accepted by agent Ketty perry at 200 HKD.', '2017-01-20 07:34:24', null);
INSERT INTO `notifications` VALUES ('358', '66', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业66', 'Ketty perry has applied for the Job #66', '2017-01-20 07:58:41', null);
INSERT INTO `notifications` VALUES ('359', '67', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业67', 'Ketty perry has applied for the Job #67', '2017-01-20 07:58:48', null);
INSERT INTO `notifications` VALUES ('360', '67', '6', '4', '2', '1', '1', '1', '0', '您对工作＃67的提案已获批准。', 'Your proposal on job #67 has been approved.', '2017-01-20 07:58:57', null);
INSERT INTO `notifications` VALUES ('361', '67', '4', '6', '1', '1', '1', '1', '0', '您的工作＃67已被代理人Ketty perry在300 HKD接受。', 'Your job #67  has been accepted by agent Ketty perry at 300 HKD.', '2017-01-20 07:59:26', null);
INSERT INTO `notifications` VALUES ('362', '68', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业68', 'Ketty perry has applied for the Job #68', '2017-01-20 08:01:13', null);
INSERT INTO `notifications` VALUES ('363', '68', '6', '4', '2', '1', '1', '1', '0', '您对工作＃68的提案已获批准。', 'Your proposal on job #68 has been approved.', '2017-01-20 08:01:19', null);
INSERT INTO `notifications` VALUES ('364', '69', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业69', 'Ketty perry has applied for the Job #69', '2017-01-20 08:53:20', null);
INSERT INTO `notifications` VALUES ('365', '69', '6', '4', '2', '1', '1', '1', '0', '您对工作＃69的提案已获批准。', 'Your proposal on job #69 has been approved.', '2017-01-20 08:53:28', null);
INSERT INTO `notifications` VALUES ('366', '70', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业70', 'Ketty perry has applied for the Job #70', '2017-01-20 09:08:51', null);
INSERT INTO `notifications` VALUES ('367', '70', '6', '4', '2', '1', '1', '1', '0', '您对工作＃70的提案已获批准。', 'Your proposal on job #70 has been approved.', '2017-01-20 09:08:57', null);
INSERT INTO `notifications` VALUES ('368', '70', '4', '6', '1', '1', '1', '1', '0', '您的工作＃70已被代理人Ketty perry在300 HKD接受。', 'Your job #70  has been accepted by agent Ketty perry at 300 HKD.', '2017-01-20 09:41:37', null);
INSERT INTO `notifications` VALUES ('369', '71', '4', '6', '1', '2', '0', '2', '0', '用户已申请作业71', 'Ketty perry has applied for the Job #71', '2017-01-20 09:54:16', null);
INSERT INTO `notifications` VALUES ('370', '71', '6', '4', '2', '1', '1', '1', '0', '您对工作＃71的提案已获批准。', 'Your proposal on job #71 has been approved.', '2017-01-20 09:54:27', null);
INSERT INTO `notifications` VALUES ('371', '71', '4', '6', '1', '1', '1', '1', '0', '您的工作＃71已被代理人Ketty perry在100 HKD接受。', 'Your job #71  has been accepted by agent Ketty perry at 100 HKD.', '2017-01-20 09:54:49', null);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(255) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=new order,1=completed order',
  `created_on` date DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('1', '1', '7', '1950', '1', '2017-05-06', '2017-05-09 13:15:12');

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_lable_id` int(11) NOT NULL,
  `order_option_id` int(11) NOT NULL,
  `created_on` date NOT NULL,
  PRIMARY KEY (`order_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_detail
-- ----------------------------
INSERT INTO `order_detail` VALUES ('1', '1', '13', '5', '2017-05-06');
INSERT INTO `order_detail` VALUES ('2', '1', '14', '8', '2017-05-06');
INSERT INTO `order_detail` VALUES ('3', '1', '15', '10', '2017-05-06');
INSERT INTO `order_detail` VALUES ('4', '1', '16', '12', '2017-05-06');
INSERT INTO `order_detail` VALUES ('5', '1', '17', '14', '2017-05-06');
INSERT INTO `order_detail` VALUES ('6', '1', '18', '16', '2017-05-06');
INSERT INTO `order_detail` VALUES ('7', '1', '19', '19', '2017-05-06');
INSERT INTO `order_detail` VALUES ('8', '1', '20', '21', '2017-05-06');
INSERT INTO `order_detail` VALUES ('9', '1', '21', '22', '2017-05-06');
INSERT INTO `order_detail` VALUES ('10', '1', '22', '24', '2017-05-06');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `sheep_type_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0: inactive, 1: active',
  `created_on` datetime DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('12', null, '\"نعيمي الذبح والتوصيل مجانا\"', '136591494365234ram_hogget__medium.jpg', '20', '1', '2017-05-09 09:27:14', '2017-05-10 07:58:28');

-- ----------------------------
-- Table structure for products_sizes
-- ----------------------------
DROP TABLE IF EXISTS `products_sizes`;
CREATE TABLE `products_sizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `product_size_id` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products_sizes
-- ----------------------------

-- ----------------------------
-- Table structure for product_lable
-- ----------------------------
DROP TABLE IF EXISTS `product_lable`;
CREATE TABLE `product_lable` (
  `product_lable_id` int(11) NOT NULL AUTO_INCREMENT,
  `lable` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0=not checkbox,1=chekbox',
  PRIMARY KEY (`product_lable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_lable
-- ----------------------------
INSERT INTO `product_lable` VALUES ('1', 'option1', '1', '0');
INSERT INTO `product_lable` VALUES ('2', 'option2', '1', '0');
INSERT INTO `product_lable` VALUES ('3', 'option1', '2', '0');
INSERT INTO `product_lable` VALUES ('4', 'option2', '2', '0');
INSERT INTO `product_lable` VALUES ('5', 'option1', '3', '0');
INSERT INTO `product_lable` VALUES ('6', 'option2', '3', '0');
INSERT INTO `product_lable` VALUES ('7', 'option1', '4', '0');
INSERT INTO `product_lable` VALUES ('8', 'option2', '4', '0');
INSERT INTO `product_lable` VALUES ('9', 'option1', '5', '0');
INSERT INTO `product_lable` VALUES ('10', 'option2', '5', '0');
INSERT INTO `product_lable` VALUES ('11', 'option1', '6', '0');
INSERT INTO `product_lable` VALUES ('12', 'option2', '6', '0');
INSERT INTO `product_lable` VALUES ('13', 'Size', '7', '0');
INSERT INTO `product_lable` VALUES ('14', 'Type of shredder', '7', '0');
INSERT INTO `product_lable` VALUES ('15', 'Type of packing', '7', '0');
INSERT INTO `product_lable` VALUES ('16', 'Head', '7', '0');
INSERT INTO `product_lable` VALUES ('17', 'Liver', '7', '0');
INSERT INTO `product_lable` VALUES ('18', 'Minced', '7', '0');
INSERT INTO `product_lable` VALUES ('19', 'Dry meat', '7', '0');
INSERT INTO `product_lable` VALUES ('20', 'Remove grease', '7', '0');
INSERT INTO `product_lable` VALUES ('21', 'Full Cleaning', '7', '0');
INSERT INTO `product_lable` VALUES ('22', 'Quantity', '7', '0');
INSERT INTO `product_lable` VALUES ('23', 'option1', '8', '0');
INSERT INTO `product_lable` VALUES ('24', 'option2', '8', '0');
INSERT INTO `product_lable` VALUES ('25', 'option3', '8', '1');
INSERT INTO `product_lable` VALUES ('26', 'option4', '8', '1');
INSERT INTO `product_lable` VALUES ('27', 'option1', '9', '0');
INSERT INTO `product_lable` VALUES ('28', 'option2', '9', '0');
INSERT INTO `product_lable` VALUES ('29', 'option3', '9', '1');
INSERT INTO `product_lable` VALUES ('30', 'option4', '9', '1');
INSERT INTO `product_lable` VALUES ('31', 'option1', '10', '0');
INSERT INTO `product_lable` VALUES ('32', 'option2', '10', '0');
INSERT INTO `product_lable` VALUES ('33', 'option3', '10', '1');
INSERT INTO `product_lable` VALUES ('34', 'option4', '10', '1');
INSERT INTO `product_lable` VALUES ('35', '??? ???????	 ', '11', '0');
INSERT INTO `product_lable` VALUES ('36', '??? ???????	', '11', '0');
INSERT INTO `product_lable` VALUES ('37', '?????	 ', '11', '0');
INSERT INTO `product_lable` VALUES ('38', '????? ?????', '11', '1');
INSERT INTO `product_lable` VALUES ('39', 'Type of shredder', '12', '0');
INSERT INTO `product_lable` VALUES ('40', 'Type of packaging	 	', '12', '0');
INSERT INTO `product_lable` VALUES ('41', 'weight', '12', '0');
INSERT INTO `product_lable` VALUES ('42', 'Full Cleaning', '12', '1');
INSERT INTO `product_lable` VALUES ('43', '', '12', '1');

-- ----------------------------
-- Table structure for product_price_option
-- ----------------------------
DROP TABLE IF EXISTS `product_price_option`;
CREATE TABLE `product_price_option` (
  `price_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `lable_option` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lable_price` varchar(255) NOT NULL,
  `lable_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0=not checkbox,1=chekbox',
  PRIMARY KEY (`price_option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_price_option
-- ----------------------------
INSERT INTO `product_price_option` VALUES ('1', '9kg', '101', '11', '0');
INSERT INTO `product_price_option` VALUES ('2', '19kg', '102', '11', '0');
INSERT INTO `product_price_option` VALUES ('3', '10kg', '103', '11', '0');
INSERT INTO `product_price_option` VALUES ('4', '100kg', '201', '12', '0');
INSERT INTO `product_price_option` VALUES ('5', 'Small', '50', '13', '0');
INSERT INTO `product_price_option` VALUES ('6', 'Medium', '50', '13', '0');
INSERT INTO `product_price_option` VALUES ('7', 'Large', '50', '13', '0');
INSERT INTO `product_price_option` VALUES ('8', 'Half', '50', '14', '0');
INSERT INTO `product_price_option` VALUES ('9', 'Quarter', '100', '14', '0');
INSERT INTO `product_price_option` VALUES ('10', 'Thermal packaging', '50', '15', '0');
INSERT INTO `product_price_option` VALUES ('11', 'Plain packaging', '80', '15', '0');
INSERT INTO `product_price_option` VALUES ('12', 'With Fat', '100', '16', '0');
INSERT INTO `product_price_option` VALUES ('13', 'Fat free', '50', '16', '0');
INSERT INTO `product_price_option` VALUES ('14', 'Need', '150', '17', '0');
INSERT INTO `product_price_option` VALUES ('15', 'No need', '75', '17', '0');
INSERT INTO `product_price_option` VALUES ('16', '5Kg', '150', '18', '0');
INSERT INTO `product_price_option` VALUES ('17', '3Kg', '70', '18', '0');
INSERT INTO `product_price_option` VALUES ('18', '5kg', '200', '19', '0');
INSERT INTO `product_price_option` VALUES ('19', '3kg', '100', '19', '0');
INSERT INTO `product_price_option` VALUES ('20', 'Yes', '200', '20', '0');
INSERT INTO `product_price_option` VALUES ('21', 'No', '100', '20', '0');
INSERT INTO `product_price_option` VALUES ('22', 'Yes', '200', '21', '0');
INSERT INTO `product_price_option` VALUES ('23', 'No', '100', '21', '0');
INSERT INTO `product_price_option` VALUES ('24', '1', '1000', '22', '0');
INSERT INTO `product_price_option` VALUES ('25', '2', '2000', '22', '0');
INSERT INTO `product_price_option` VALUES ('26', '3', '3000', '22', '0');
INSERT INTO `product_price_option` VALUES ('27', '9kg', '100', '23', '0');
INSERT INTO `product_price_option` VALUES ('28', '10kg', '101', '23', '0');
INSERT INTO `product_price_option` VALUES ('29', '11kg', '200', '23', '0');
INSERT INTO `product_price_option` VALUES ('30', '8kg', '600', '24', '0');
INSERT INTO `product_price_option` VALUES ('31', '9kg', '100', '27', '0');
INSERT INTO `product_price_option` VALUES ('32', '10kg', '101', '27', '0');
INSERT INTO `product_price_option` VALUES ('33', '11kg', '200', '27', '0');
INSERT INTO `product_price_option` VALUES ('34', '8kg', '600', '28', '0');
INSERT INTO `product_price_option` VALUES ('35', '9kg', '100', '31', '0');
INSERT INTO `product_price_option` VALUES ('36', '10kg', '101', '31', '0');
INSERT INTO `product_price_option` VALUES ('37', '11kg', '200', '31', '0');
INSERT INTO `product_price_option` VALUES ('38', '8kg', '600', '32', '0');
INSERT INTO `product_price_option` VALUES ('39', '6kg', '50', '33', '1');
INSERT INTO `product_price_option` VALUES ('40', '1kg', '60', '33', '1');
INSERT INTO `product_price_option` VALUES ('41', '50', '70', '34', '1');
INSERT INTO `product_price_option` VALUES ('42', '???? ??', '5', '35', '0');
INSERT INTO `product_price_option` VALUES ('43', '??????', '10', '35', '0');
INSERT INTO `product_price_option` VALUES ('44', '????????? ???????', '15', '36', '0');
INSERT INTO `product_price_option` VALUES ('45', '????', '5', '37', '0');
INSERT INTO `product_price_option` VALUES ('46', '????? ?????', '10', '38', '1');
INSERT INTO `product_price_option` VALUES ('47', 'Strip-Cut', '5', '39', '0');
INSERT INTO `product_price_option` VALUES ('48', 'Cross-Cut', '10', '39', '0');
INSERT INTO `product_price_option` VALUES ('49', 'plastic packing', '5', '40', '0');
INSERT INTO `product_price_option` VALUES ('50', 'Box packing', '10', '40', '0');
INSERT INTO `product_price_option` VALUES ('51', '5 kg', '5', '41', '0');
INSERT INTO `product_price_option` VALUES ('52', '10 kg', '10', '41', '0');
INSERT INTO `product_price_option` VALUES ('53', '15 kg', '15', '41', '0');
INSERT INTO `product_price_option` VALUES ('54', 'Full Cleaning', '10', '42', '1');
INSERT INTO `product_price_option` VALUES ('55', '', '', '43', '1');

-- ----------------------------
-- Table structure for sheep_size
-- ----------------------------
DROP TABLE IF EXISTS `sheep_size`;
CREATE TABLE `sheep_size` (
  `size_id` int(11) NOT NULL AUTO_INCREMENT,
  `size_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0: no 2: inactive',
  `created_on` datetime DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sheep_size
-- ----------------------------
INSERT INTO `sheep_size` VALUES ('1', 'الأغنام الكبيرة', '1', '2017-05-10 02:45:46', '2017-05-10 02:47:40');
INSERT INTO `sheep_size` VALUES ('2', 'الأغنام الصغيرة', '1', '2017-05-10 02:45:46', '2017-05-10 02:47:52');
INSERT INTO `sheep_size` VALUES ('3', 'متوسط', '1', '2017-05-10 02:45:46', '2017-05-10 02:47:53');
INSERT INTO `sheep_size` VALUES ('4', 'شترا كبيرة', '1', '2017-05-10 02:45:46', '2017-05-10 02:47:55');
INSERT INTO `sheep_size` VALUES ('5', 'شترا شترا كبيرة', '1', '2017-05-10 02:45:46', '2017-05-10 02:47:54');

-- ----------------------------
-- Table structure for sheep_types
-- ----------------------------
DROP TABLE IF EXISTS `sheep_types`;
CREATE TABLE `sheep_types` (
  `sheep_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `sheep_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0:no,1:active',
  `created_on` datetime DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sheep_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sheep_types
-- ----------------------------
INSERT INTO `sheep_types` VALUES ('1', 'الأسترالي، شيب', '1', '2017-05-10 02:40:49', '2017-05-10 02:43:01');
INSERT INTO `sheep_types` VALUES ('2', 'الخراف العربي', '1', '2017-05-10 02:40:49', '2017-05-10 02:44:42');
INSERT INTO `sheep_types` VALUES ('3', 'خرفان الهندي', '1', '2017-05-10 02:40:49', '2017-05-10 02:44:43');

-- ----------------------------
-- Table structure for user_log
-- ----------------------------
DROP TABLE IF EXISTS `user_log`;
CREATE TABLE `user_log` (
  `user_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `gym_id` int(11) DEFAULT NULL,
  `message` text,
  `distance` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `system_version` varchar(255) DEFAULT NULL,
  `system_model` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `app_state` varchar(255) DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`user_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_log
-- ----------------------------
INSERT INTO `user_log` VALUES ('1', '3', '0', '', '', '', '', '', '', '', '', '', '2016-09-29 12:07:06');
INSERT INTO `user_log` VALUES ('2', '3', '2', '', '', '', '', '', '', '', '', '', '2016-09-29 12:07:45');
INSERT INTO `user_log` VALUES ('3', '3', '2', 'sdfsdfsdf', '', '', '', '', '', '', '', '', '2016-09-29 12:07:52');
INSERT INTO `user_log` VALUES ('4', '3', '0', '', '', '', '', '', '', '', '', '', '2016-09-29 12:09:03');
INSERT INTO `user_log` VALUES ('5', '3', '0', '34gdfgdfgdfg', '', '', '', '', '', '', '', '', '2016-09-29 12:09:11');
INSERT INTO `user_log` VALUES ('6', '3', '0', '34gdfgdfgdfg', '45345', '', '', '', '', '', '', '', '2016-09-29 12:09:20');
INSERT INTO `user_log` VALUES ('7', '3', '8', 'Set EXIT Point in Leo GYM\'s geofencing.', '', 'monitorGym:isOnExit:', '', '', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:29');
INSERT INTO `user_log` VALUES ('8', '3', '8', 'Entered in Leo GYM\'s geofencing.', '9', 'locationManager:didEnterRegion:', '23.0662', '72.5321', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:29');
INSERT INTO `user_log` VALUES ('9', '3', '8', 'Entered in Leo GYM\'s geofencing.', '9', 'locationManager:didEnterRegion:', '23.0662', '72.5321', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:29');
INSERT INTO `user_log` VALUES ('10', '3', '8', 'Set EXIT Point in Leo GYM\'s geofencing.', '', 'monitorGym:isOnExit:', '', '', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:29');
INSERT INTO `user_log` VALUES ('11', '3', '8', 'Started your workout.', '', 'startWorkout:', '', '', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:29');
INSERT INTO `user_log` VALUES ('12', '3', '8', 'Set EXIT Point in Leo GYM\'s geofencing.', '', 'monitorGym:isOnExit:', '', '', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:32');
INSERT INTO `user_log` VALUES ('13', '3', '8', 'Set EXIT Point in Leo GYM\'s geofencing.', '', 'monitorGym:isOnExit:', '', '', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:33');
INSERT INTO `user_log` VALUES ('14', '3', '8', 'Entered in Leo GYM\'s geofencing.', '9', 'locationManager:didEnterRegion:', '23.0662', '72.5321', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:34');
INSERT INTO `user_log` VALUES ('15', '3', '8', 'Completed your workout.', '', 'completeWorkout:', '', '', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:53');
INSERT INTO `user_log` VALUES ('16', '3', '8', 'Entered in Leo GYM\'s geofencing.', '9', 'locationManager:didEnterRegion:', '23.0662', '72.5321', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:12:58');
INSERT INTO `user_log` VALUES ('17', '3', '8', 'Entered in Leo GYM\'s geofencing.', '8', 'locationManager:didEnterRegion:', '23.0662', '72.5321', '9.2', 'iPhone 6', '1.12', 'UIApplicationStateActive', '2016-10-05 10:43:11');
