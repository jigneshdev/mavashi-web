// JavaScript Document
app = angular.module('UniDealApp', ['ngRoute', 'ui.bootstrap','pascalprecht.translate']);

app.constant('MYMESSAGE', {
   ERROR_NUMBER_INPUT_VALUE: "Value should be between 0 to 99"
   });

/*Check if user logout it redirect the login page.*/
function checklogin(param) {    
    if (param.hasOwnProperty('logout')) {
        window.location = 'manage/login';
    } else {
        return true;
    }
}

/*#########################
 * menu controller
 *#########################*/

app.controller('MyCtrl', function($scope, $location) {

    $scope.isActive = function(route) {

        return route === $location.path();
    }
    $scope.showsub = false;
    $scope.arrowopen = "arrow";
    $scope.submenu = function() {
        $scope.showsub = !$scope.showsub;
        $scope.mainmenu = "nav-item open";
        if (!$scope.showsub) {
            $scope.arrowopen = "arrow";
        }
        else {
            $scope.arrowopen = "arrow open";
        }
    }
    $scope.activemainmenu = function() {
        $scope.mainmenu = "active";
    }
    $scope.hidesub = function() {
        $scope.mainmenu = "";
    }
});

/*#########################
 * Routes controller
 *#########################*/

app.config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }]);
//for changing title
app.run(['$location', '$rootScope', function($location, $rootScope) {
        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
            //$rootScope.title = current.$route.title;
            document.body.scrollTop = document.documentElement.scrollTop = 0;

        });
    }]);
//end
app.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.
                when('/manage/dashboard', {
                    templateUrl: base_url + 'app/views/manage/dashboard.html',
                    controller: 'HomeController',
                    title: 'Mavashi :: Dashboard'
                }).
                when('/manage/settings', {
                    templateUrl: base_url + 'app/views/manage/settings.html',
                    controller: 'AdminProfileController',
                    title: 'Mavashi :: Admin Profile '
                }).
                when('/manage/orders', {
                    templateUrl: base_url + 'app/views/manage/orders.html',
                    controller: 'OrdersController',
                    title: 'Mavashi :: Orders'
                }).
                when('/manage/neworders', {
                    templateUrl: base_url + 'app/views/manage/neworders.html',
                    controller: 'OrdersController',
                    title: 'Mavashi :: New Orders'
                }).
                when('/manage/products', {
                    templateUrl: base_url + 'app/views/manage/product.html',
                    controller: 'ProductController',
                    title: 'Mavashi:: Manage Products'
                }).
                when('/manage/addnewproduct', {
                    templateUrl: base_url + 'app/views/manage/addproduct.html',
                    controller: 'AddproductController',
                    title: 'Mavashi:: Add New Products'
                }).
                when('/manage/order_detail/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_order.html',
                    controller: 'OrdersController',
                    title: 'Mavashi:: Order detail'
                }).
                when('/manage/edit_order/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_order.html',
                    controller: 'OrdersController',
                    title: 'Mavashi:: Order detail'
                }).
                when('/manage/product_detail/:id', {
                    templateUrl: base_url + 'app/views/manage/product_detail.html',
                    controller: 'ProductController',
                    title: 'Mavashi:: Product detail'
                }).
                when('/manage/edit_product/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_product.html',
                    controller: 'EditproductController',
                    title: 'Mavashi:: Edit Product'
                }).
                when('/manage/configsetting', {
                    templateUrl: base_url + 'app/views/manage/configsetting.html',
                    controller: 'SettingsController',
                    title: 'Mavashi:: Settings'
                }).
                when('/manage/banklisting', {
                    templateUrl: base_url + 'app/views/manage/banklist.html',
                    controller: 'BankController',
                    title: 'Mavashi:: Bank listing'
                }).
                when('/manage/edit_bank/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_bank.html',
                    controller: 'BankController',
                    title: 'Mavashi:: Edit Bank'
                }).
                 when('/manage/inquery', {
                    templateUrl: base_url + 'app/views/manage/inquery.html',
                    controller: 'InqueryController',
                    title: 'Mavashi:: Edit Bank'
                }).
                 when('/manage/allorderslist', {
                    templateUrl: base_url + 'app/views/manage/allorders.html',
                    controller: 'OrdersController',
                    title: 'Mavashi:: All Order Listing'
                }).
                 when('/manage/usermanage', {
                    templateUrl: base_url + 'app/views/manage/manageuser.html',
                    controller: 'UserController',
                    title: 'Mavashi:: User manage'
                }).
                 when('/manage/userdetail/:id', {
                    templateUrl: base_url + 'app/views/manage/edit_user.html',
                    controller: 'UsereditController',
                    title: 'Mavashi:: User manage'
                }).
                when('/manage/addnewuser', {
                    templateUrl: base_url + 'app/views/manage/add_user.html',
                    controller: 'UseresddController',
                    title: 'Mavashi:: Add new user'
                }).
                otherwise({
                    redirectTo: '/manage/dashboard'
                });
        $locationProvider.html5Mode(true);
    }]);

//for loader
/*CONFIG*/
app.run(function($rootScope, $location, $route, $timeout, $http) {

    $rootScope.layout = {};
    $rootScope.layout.loading = false;
     $rootScope.ADMIN_ACCESS = '';
    $rootScope.$on('$routeChangeStart', function() {

        $timeout(function() {
            $rootScope.layout.loading = true;
        });
    });
    $rootScope.$on('$routeChangeSuccess', function() {
        $timeout(function() {
            $rootScope.layout.loading = false;
        }, 200);
    });
    $rootScope.$on('$routeChangeError', function() {
        $rootScope.layout.loading = false;

    });
    
            $http.get('manage/login/useraccess', {cache: true}).
            success(function (data, status, headers, config) {
                     $rootScope.ADMIN_ACCESS = data;
            }).
            error(function (data, status, headers, config) {

            });
    
});
app.config(function($httpProvider) {
    $httpProvider.responseInterceptors.push('myHttpInterceptor');

    var spinnerFunction = function spinnerFunction(data, headersGetter) {
        $(".page-spinner-bar").show();
        return data;
    };

    $httpProvider.defaults.transformRequest.push(spinnerFunction);
});

app.config(function ($translateProvider) {
  $translateProvider.translations('en', {
    mawashi:'Mawashi',
    user:'User',
    admin:'Admin',
    dashboard:'Dashboard',
    orders:'Completed Orders',
    order:'Order',
    new_orders:'Today Orders',
    products:'Products',
    my_details: 'MY DETAILS',
    name: 'NAME : ',
    email: 'EMAIL : ',
    save: 'SAVE',
    change_password:'CHANGE PASSWORD',
    old_password:'OLD PASSWORD',
    new_password:'NEW PASSWORD',
    change_language:'CHANGE LANGUAGE',
    language:'Language : ',
    admin_settings:'Admin Settings',
    log_out:'Log Out',
    total_products:'Total Products',
    total_orders:'Total Orders',
    allorders:'Active Orders',
    total_revenue:'Total Sales',
    'total_profit': 'Total Profit',
    recent_activities:'RECENT ACTIVITIES',
    coming_soon:'Coming soon..',
    order_id:'Order Id',
    product_name:'Product Name',
    sheep_type:'Sheep type',
    sheep_size:'Sheep Size',
    gross_total:'Gross Total',
    delivery_address:'Delivery Address',
    action:'Action',
    reset:'Reset',
    add_new_product:'Add new product',
    id:'Id',
    product_pic:'Product pic',
    price:'Price',
    uploaded_on:'Uploaded on',
    product_photo:'Product photo',
    option:'Option',
    add:'Add',
    add_more_options:'Add more options',
    clear:'Clear',
    remove:'Remove',
    add_more_checkbox:'Add more checkbox',
    add_product:'Add product',
    edit_product:'Edit Products',
    update_product:'Update Products',
    order_details:'Order details',
    amount:'Amount',
    paid:'Paid',
    unpaid:'Unpaid',
    no:'No',
    lable:'Lable',
    product_size:'Product size',
    product_image:'Product image',
    quantity:'Quantity',
    payment_status:'Payment status',
    sheep_size_detail:'Add Sheep Size',
    sheep_type_detail:'Add Sheep Type',
    general_setting:'General setting',
    pending_orders:'Pending orders',
    orders_by_week:'Orders of this week',
    orders_by_month:'Orders of this month',
    orders_by_year:'Orders of this year',
    update_order:'Update order',
    about_us:'About us',
    our_vision:'Our vision',
    content_detail:'Content detail',
    banklist: 'Banks',
    setting:'Setting',
    privacy_policy:'Privacy policy',
    export_to:'Export To',
    export:'Export',
    bank_list:'Bank List',
    add_new_bank:'ADD NEW BANK',
    add_bank:'Add bank',
    bank_logo:'Bank logo',
    bank_name:'Bank name',
    bank_account_number:'Bank account number',
    account_number:'Account Number',
    excel:'Excel',
    inquery: 'Inquiry',
    inquerylist: 'Inquery List',
    inq_name : 'Name',
    inq_email: 'Email',
    inq_message : 'Message',
    inq_date : 'Date',
    inq_num : 'Contact',
    all_order_list: 'All Orders List',
    ship_type: 'Ship Type',
    ship_size: 'Ship Size',
    usermanage: 'Manage Users',
    user_id: 'User Id',
    user_name: 'Name',
    profile_image: 'Profile Pic',
    admin_type: 'Admin Type',
    email_address: 'Email',
    add_new_user: 'Add New User',
    confirm_password: 'Confirm Password',
    add_users_button: 'Add User',
    edit_users_button: 'Edit User',
    city: 'City',
    payment_type: 'Payment Type',
    application_price: 'Final price'
    
});
  $translateProvider.translations('ar', {
    mawashi:'مواشي',
    user:'المستعمل',
    application_price: 'السعر النهائي',
    admin:'مشرف',
    dashboard:'لوحة القيادة',
    order:'طلب',
    orders:'الطلبات المكتملة',
    new_orders:'أوامر اليوم',
    products:'منتجات',
    my_details: 'تفاصيلي',
    name:' : اسم',
    email:' : البريد الإلكتروني',
    save:'حفظ',
    change_password:'تغيير كلمة السر',
    old_password:'كلمة المرور القديمة',
    new_password:'كلمة السر الجديدة',
    change_language:'غير اللغة',
    language:'اللغة',
    admin_settings:'إعدادات المشرف',
    log_out:'الخروج',
    total_products:'إجمالي المنتجات',
    total_orders:'إجمالي الطلبات',
    //new_orders:'طلبات جديدة',
    total_revenue:'إجمالي الإيرادات',
    total_profit : 'إجمالي الإيرادات',
    recent_activities:'أنشطة حالية',
    coming_soon:'قريبا..',
    order_id:'رقم التعريف الخاص بالطلب',
    product_name:'اسم المنتج',
    sheep_type:'نوع الأغنام',
    sheep_size:'حجم الأغنام',
    gross_total:'المجموع الكلي',
    delivery_address:'عنوان التسليم',
    action:'عمل',
    reset:'إعادة تعيين',
    add_new_product:'إضافة منتج جديد',
    id:'هوية شخصية',
    product_pic:'صورة المنتج',
    price:'السعر',
    uploaded_on:'تم التحميل على',
    product_photo:'صورة المنتج',
    option:'اختيار',
    add:'إضافة',
    add_more_options:'إضافة المزيد من الخيارات',
    clear:'واضح',
    remove:'إزالة',
    add_more_checkbox:'أضف المزيد من مربع الاختيار',
    add_product:'إضافة منتج',
    edit_product:'تحرير المنتجات',
    update_product:'تحديث المنتجات',
    order_details:'تفاصيل الطلب',
    amount:'كمية',
    paid:'دفع',
    unpaid:'غير مدفوع',
    no:'لا',
    lable:'علامة مميزة',
    product_size:'حجم المنتج',
    product_image:'صورة المنتج',
    quantity:'كمية',
    payment_status:'حالة السداد',
    sheep_size_detail:'الأغنام حجم التفاصيل',
    sheep_type_detail:'نوع الأغنام التفاصيل',
    general_setting:'الإعداد العام',
    pending_orders:'في انتظار أوامر',
    orders_by_week:'أوامر حسب الأسبوع',
    orders_by_month:'أوامر حسب الشهر',
    orders_by_year:'أوامر حسب السنة',
    update_order:'أجل التحديث',
    about_us:'معلومات عنا',
    our_vision:'رؤيتنا',
    content_detail:'تفاصيل المحتوى',
    banklist: 'البنوك',
    setting:'ضبط',
    privacy_policy:'سياسة الخصوصية',
    export_to:'تصدير الى',
    export:'تصدير',
    bank_list:'قائمة البنوك',
    add_new_bank:'إضافة بنك جديد',
    add_bank:'أد بانك',
    bank_logo:'شعار البنك',
    bank_name:'اسم البنك',
    bank_account_number:'رقم الحساب المصرفي',
    account_number:'رقم حساب',
    excel:'تفوق',
    inquery: 'تحقيق',
    inquerylist : 'قائمة الاستفسارات',
    inq_name : 'اسم',
    inq_email : 'البريد الإلكتروني',
    inq_message : 'رسالة',
    inq_date: 'تاريخ',
    inq_num: 'اتصل',
    allorders: 'جميع الطلبات',
    usermanage: 'ادارة المستخدمين',
    user_id: 'معرف المستخدم',
    user_name: 'اسم',
    profile_image: 'الملف الشخصي الموافقة المسبقة عن علم',
    admin_type: 'نوع المشرف',
    email_address: 'البريد الإلكتروني',
    add_new_user: 'إضافة مستخدم جديد',
    confirm_password: 'تأكيد كلمة المرور',
    add_users_button: 'إضافة مستخدم',
    edit_users_button: 'تحرير العضو',
    city: 'مدينة',
    payment_type: 'نوع الدفع'
  });
  $translateProvider.preferredLanguage('en');
});

app.factory('myHttpInterceptor', function($q, $window) {
    return function(promise) {
        return promise.then(function(response) {
            $(".page-spinner-bar").hide();
            checklogin(response.data);
            return response;
        }, function(response) {
            $(".page-spinner-bar").hide();
            return $q.reject(response);
        });
    };
});

// Home controllers.
app.controller('HomeController', ['$http', '$scope', function($http, $scope) {

    $http.get('dashboard/lateststats', {cache: true}).
       success(function (data, status, headers, config) {
                if(data.success){
                    $scope.allstats = data.data;
                }
       }).
       error(function (data, status, headers, config) {

       });

    }]);

// For admin profile.
app.controller('AdminProfileController', ['$http', '$scope','$routeParams','$translate', function($http, $scope,$routeParams,$translate) {
        $scope.changeLanguage = function (key) {
            $translate.use(key);
        };

        $http.get('adminprofile/adminprofile', {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.admindetails = data.details;
            }
        }).
        error(function (data, status, headers, config) {

        });
}]);

app.controller('OrdersController', ['$http', '$scope','$routeParams', function($http, $scope,$routeParams) {
        
        // To Order details.
        $http.get('orders/orderdetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                   $scope.orderdetails = data.details.order_info;
                   $scope.order_options = data.details.order_list;
           }
        }).
        error(function (data, status, headers, config) {

        });
      
        // To get Orders comments.
        $http.get('orders/allcomments?id=' + $routeParams.id, {cache: true}).
         success(function (data, status, headers, config) {
          //  $scope.commentslist = [];
             if(data.success == true) {
                            $scope.commentslist = [];
                          $scope.commentslist.push(data.data);
                         console.log($scope.commentslist);
                          setTimeout(function(){
                                 $('li').last().addClass('active-li').focus();
                            },2000);
               }
            }).
         error(function (data, status, headers, config) {
        });

         $scope.insertComments = function(){
                var order_id = $('#order_main_id').val();
                var comment_box = $("#comment_box").val();
                console.log(comment_box);

                $http.get('orders/insertcomments?order_id='+order_id + '&comment_box=' + comment_box, {cache: true}).
                 success(function (data, status, headers, config) {
                     if(data.success == true) {
                            console.log($scope.commentslist);
                            if(typeof $scope.commentslist != "undefined" && $scope.commentslist != null && $scope.commentslist.length > 0){
                                  $scope.commentslist[0].push(data.data);
                            }else {
                                    $scope.commentslist = [];
                                    $scope.commentslist.push(data.data);
                            }
                            $("#comment_box").val(' ');
                            setTimeout(function(){
                                 $('li').last().addClass('active-li').focus();
                            },2000);
                           
                            //$("#chatinner").animate({ scrollTop: $('#div1').prop("scrollHeight")}, 1000);
                            //$('#chatinner').scrollTop(0); 
                           

                       }
                    }).
                 error(function (data, status, headers, config) {
                });

         }

}]);
app.controller('ProductController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){
    $http.get('products/productetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.productetails = data.details;
            }
        }).
        error(function (data, status, headers, config) {

        });
}]);
//AddproductController

app.controller('AddproductController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){
    
    $http.get('products/sheeptypes', {cache: true}).
    success(function (data, status, headers, config) {
    if(data.success == true) {
             $scope.sheepdetails = data.details;
        }
    }).
    error(function (data, status, headers, config) {

    });

    $http.get('products/sheepsizes', {cache: true}).
    success(function (data, status, headers, config) {
    if(data.success == true) {
             $scope.sheepsizedetails = data.details;
        }
    }).
    error(function (data, status, headers, config) {

    });

    $scope.add_new_product = [];
    
    // Add new options
    $scope.addNewOptions = function(){
        $scope.add_new_product.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDiv = function(index){
        $scope.add_new_product.splice(index, 1);
        console.log($scope.add_new_product);
         $('#remove_div_' + index).remove();
    }
    
    $scope.removeOptionDiv2 =  function(index){
         console.log(index)
        // $scope.add_new_product.splice(index, 1);
         console.log($scope.add_new_product);
         $('#remove_divsec_' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv = function(index){
        //$(document).find('#adddivprice_'+ index).html(' ');
      //  $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" placeholder="Option price">');
        //$('#adddivoptions_'+ index).append('<br><input type="text" class="form-control" name="lable_optiion[]" placeholder="Enter values for drop down">  <a href=');
    }
    
    $scope.addpriceandoptions = function(index){
        
          $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" id placeholder="Option price">');
          $('#adddivoptions_'+index).append('<br><input type="text" class="form-control" name="lable_option[]" placeholder="Enter values for drop down"><input type="hidden" value="'+index+'" name="option_value_index[]">');
        
    }
    
    //Add more checkbox
    $scope.add_new_product_chk = [];
    
    // Add new options
    $scope.addNewOptionschk = function(){
        $scope.add_new_product_chk.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDivchk = function(index){
        $scope.add_new_product_chk.splice(index, 1);
        console.log($scope.add_new_product_chk);
         $('#remove_div_chk' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv_chk = function(index){
        //$(document).find('#adddivprice_'+ index).html(' ');
      //  $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" placeholder="Option price">');
        //$('#adddivoptions_'+ index).append('<br><input type="text" class="form-control" name="lable_optiion[]" placeholder="Enter values for drop down">  <a href=');
    }
    
    $scope.addpriceandoptions_chk = function(index){
        $('#adddivprice_chk' + index).append('<br><input type="text" class="form-control" name="lable_price_chk[]" id placeholder="Option price">');
        $('#adddivoptions_chk'+index).append('<br><input type="text" class="form-control" name="lable_option_chk[]" placeholder="Enter values for drop down"><input type="hidden" value="'+index+'" name="option_value_index_chk[]">');
    }
}]);

//Edit product
app.controller('EditproductController', ['$http', '$scope', '$routeParams', '$timeout', function ($http, $scope,$routeParams, $timeout){
    
    // To Edit product details.
    $http.get('products/productetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.productetails = data.details;
                 $scope.sheep_sizes = data.details.sheep_size;
            }
        }).
        error(function (data, status, headers, config) {

        });
    
    // To Sheep type.   
    $http.get('products/sheeptypes', {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
             $scope.sheepdetails = data.details;
        }
        }).
        error(function (data, status, headers, config) {
     });

    $http.get('products/sheepsizes', {cache: true}).
        success(function (data, status, headers, config) {
    if(data.success == true) {
             $scope.sheepsizedetails = data.details;
        }
    }).
        error(function (data, status, headers, config) {

    });
    $scope.add_new_product = [];
    
    // Add new options
    $scope.addNewOptions = function(){
        $scope.add_new_product.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDiv = function(index){
        $scope.add_new_product.splice(index, 1);
        console.log($scope.add_new_product);
         $('#remove_div_' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv = function(index){}
    
     $scope.addpriceandoptions = function(index, sectionIndex1){
        
          $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_'+sectionIndex1+'_price[]" id placeholder="Option price">');
          $('#adddivoptions_'+index).append('<br><input type="text" class="form-control" name="lable_'+sectionIndex1+'_option[]" placeholder="Enter values for drop down">');
        
    }
    
    $scope.addpriceandoptions2 = function(index, sectionIndex){
    
        $('#adddivprice2_' + index).append('<br><input type="text" class="form-control" name="lable_'+sectionIndex+'_price2[]" id placeholder="Option price">');
        $('#adddivoptions2_'+index).append('<br><input type="text" class="form-control" name="lable_'+sectionIndex+'_option2[]" placeholder="Enter values for drop down">');
    }
    
    //Add more checkbox
    $scope.add_new_product_chk = [];
    
    // Add new options
    $scope.addNewOptionschk = function(){
        $scope.add_new_product_chk.push(1);
    }
    
    // To remove the divs from the add new products.
    $scope.removeOptionDivchk = function(index){
        $scope.add_new_product_chk.splice(index, 1);
        console.log($scope.add_new_product_chk);
         $('#remove_div_chk' + index).remove();
    }
    
    // To clear the div.
    $scope.cleartheoptiondiv_chk = function(index){
        //$(document).find('#adddivprice_'+ index).html(' ');
      //  $('#adddivprice_' + index).append('<br><input type="text" class="form-control" name="lable_price[]" placeholder="Option price">');
        //$('#adddivoptions_'+ index).append('<br><input type="text" class="form-control" name="lable_optiion[]" placeholder="Enter values for drop down">  <a href=');
    }
    
    $scope.addpriceandoptions_chk = function(index ,sectionIndex2){
        $('#adddivprice_chk' + index).append('<br><input type="text" class="form-control" name="lable_'+sectionIndex2+'_price_chk[]" id placeholder="Option price">');
        $('#adddivoptions_chk'+index).append('<br><input type="text" class="form-control" name="lable_'+sectionIndex2+'_option_chk[]" placeholder="Enter values for drop down">');
    }
    
    //add new on edit page.
    $scope.addpriceandoptions_chk2 = function(index, sectionCheckIndex){
        $('#adddivprice_chk2' + index).append('<br><input type="text" class="form-control" name="lable_'+sectionCheckIndex+'_price_chk2[]" id placeholder="Option price">');
        $('#adddivoptions_chk2'+index).append('<br><input type="text" class="form-control" name="lable_'+sectionCheckIndex+'_option_chk2[]" placeholder="Enter values for drop down">');
    }
    //addpriceandoptions_chk2
    
}]);

app.controller('SettingsController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){
        $http.get('setting/content', {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.textdata = data.data;
            }
        }).
        error(function (data, status, headers, config) {

        });
        
        // To get contact Number.
         $http.get('setting/contact', {cache: true}).
                success(function (data, status, headers, config) {
                if(data.success == true) {
                         $scope.contact_number = data.data;
                    }
                }).
                error(function (data, status, headers, config) {

            });
}]);

app.controller('BankController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){

        $http.get('bank/bankdetails?id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.bankdetails = data.data;
            }
        }).
        error(function (data, status, headers, config) {

        });
    
}]);

app.controller('InqueryController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){
}]);

app.controller('UserController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){

       console.log('sadsad');
}]);

app.controller('UsereditController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){

     console.log($routeParams.id);
    
     $http.get('usermanage/userinfo?user_id=' + $routeParams.id, {cache: true}).
        success(function (data, status, headers, config) {
        if(data.success == true) {
                 $scope.edituserdetails = data.details;
                 console.log($scope.edituserdetails);
            }
    }).
    error(function (data, status, headers, config) {
    });
    
}]);

//UseraddController

app.controller('UseresddController', ['$http', '$scope', '$routeParams', function ($http, $scope,$routeParams){

       console.log($routeParams.id);
//     $http.get('products/productetails?id=' + $routeParams.id, {cache: true}).
//        success(function (data, status, headers, config) {
//        if(data.success == true) {
//                 $scope.productetails = data.details;
//                 console.log($scope.productetails);
//                 console.log($scope.productetails.sheep_size);
//            }
//        }).
//        error(function (data, status, headers, config) {
//
//        });
    
}]);