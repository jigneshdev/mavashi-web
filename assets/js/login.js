var Login = function() {

    var handleLogin = function() {

        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },
            messages: {
                email: {
                    required: "Email address is required."
                },
                password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.login-form')).show();
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //define form data
                var fd = new FormData();
                //append data                
                $.each($('.login-form').serializeArray(), function(i, obj) {
                    fd.append(obj.name, obj.value)
                })

                $.ajax({
                    url: BASEURL + 'login/login',
                    type: "post",
                    processData: false,
                    contentType: false,
                    data: fd,
                    beforeSend: function() {

                    },
                    success: function(res) {
                        if (res.status == '1')// in case genre added successfully
                        {
                            //redirect to dashboard
                            location.href = BASEURL + 'dashboard';

                        } else { // in case error occue
                            toastr.error(res.message, 'Error');
                            return false;
                        }
                    },
                    error: function(e) {
                        //called when there is an error
                        toastr.warning('Oops! Something just went wrong. Please try again later.', 'Warning');
                        return false;
                    },
                    complete: function() {
                        $('.login-form')[0].reset();
                    }
                }, "json");
                return false;

            }
        });

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    var handleForgetPassword = function() {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email_fp: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email_fp: {
                    required: "Email address is required."
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit   

            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function(form) {
                //define form data
                var fd = new FormData();
                //append data                
                $.each($('.forget-form').serializeArray(), function(i, obj) {
                    fd.append(obj.name, obj.value)
                })

                $.ajax({
                    url: BASEURL + 'login/forgotpassword',
                    type: "post",
                    processData: false,
                    contentType: false,
                    data: fd,
                    beforeSend: function() {
                        $("#email_fp").attr('disabled',true);
                        $("#btn_forgot_password").attr('disabled',true);
                        $("#btn_forgot_password").text('Please Wait....');
                    },
                    success: function(res) {
                        if (res.status == '1')// in case genre added successfully
                        {
                            //redirect to dashboard
                            toastr.success(res.message, 'ReMo');
                            return false;

                        } else { // in case error occue
                            toastr.error(res.message, 'ReMo');
                            return false;
                        }
                    },
                    error: function(e) {
                        //called when there is an error
                        toastr.warning('Oops! Something just went wrong. Please try again later.', 'ReMo');
                        return false;
                    },
                    complete: function() {
                        $("#email_fp").attr('disabled',false);
                        $("#btn_forgot_password").attr('disabled',false);
                        $("#btn_forgot_password").text('Submit');
                        $('.forget-form')[0].reset();
                    }
                }, "json");
                return false;
            }
        });

        $('.forget-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function() {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function() {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    }

    return {
        //main function to initiate the module
        init: function() {

            handleLogin();
            handleForgetPassword();

        }

    };

}();

jQuery(document).ready(function() {
    Login.init();
});